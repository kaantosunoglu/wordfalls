﻿using System;
using System.Collections;
using System.Collections.Generic;
using Facebook;
using Facebook.Unity;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;
using GameSparks.Core;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using tQsLib;
using UnityEngine;

public class GameSparkManager : MonoBehaviour {

	public static GameSparkManager instance;
	public bool isGSconnected;

	int tries = 0;

	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {

		GS.GameSparksAvailable += OnGameSparksConnected;

	}

	// Update is called once per frame
	void Update () {

	}

	private void OnGameSparksConnected (bool _isConnected) {
		tries++;
		if (_isConnected) {
			isGSconnected = true;
		} else {
			if (tries == 50) {
				ErrorThrower ();
			} else {
				isGSconnected = false;
				GS.Reconnect ();
			}

		}
	}

	public void FacebookAuthentication (Action<bool> completion, Action<string> trouble) {
		string s = "{\"LANG_SEG\":" + (GameController.instance.player.PlayerLanguage == 1 ? "\"TR\"" : "\"EN\"") + "}";
		GSRequestData segment = new GSRequestData (s);

		new FacebookConnectRequest ()
			.SetAccessToken (AccessToken.CurrentAccessToken.TokenString)
			.SetDoNotLinkToCurrentPlayer (false) // we don't want to create a new account so link to the player that is currently logged in
			.SetSwitchIfPossible (true) //this will switch to the player with this FB account id they already have an account from a separate login
			.SetSegments (segment)
			.Send ((fbauth_response) => {
				if (!fbauth_response.HasErrors) {
				
					EventManager.TriggerEvent (WFConstants.NotificationType.DailyRewardReceived, fbauth_response.ScriptData);
					PlayerPrefs.SetString ("loginas", "fb");
					GameController.instance.player.playerName = fbauth_response.DisplayName;
					completion (true);
					//SaverLoader.instance.SaveData();
				} else {
				//	Debug.LogWarning (fbauth_response.Errors.JSON); //if we have errors, print them out
					trouble (fbauth_response.Errors.JSON);
				}
			});
	}

	public void RegisterAsGuest (string nickName, Action<bool> completion, Action<string> trouble) {
		string deviceId = SystemInfo.deviceUniqueIdentifier;
		PlayerPrefs.SetString("DI", deviceId);
		DateTime today = DateTime.UtcNow;
		today = new DateTime (today.Year, today.Month, today.Day, today.Hour, today.Minute, today.Second);
		int time = (int) (today.Subtract (new DateTime (1970, 1, 1, 0, 0, 0))).TotalSeconds;

		string s = "{\"LANG_SEG\":" + (GameController.instance.player.PlayerLanguage == 1 ? "\"TR\"" : "\"EN\"") + "}";
		GSRequestData segment = new GSRequestData (s);

		new RegistrationRequest ()
			.SetDisplayName (nickName)
			.SetUserName (nickName + deviceId)
			.SetPassword (nickName + deviceId)
			.SetSegments (segment)
			.Send ((response) => {

				//new GameSparks.Api.Requests.DeviceAuthenticationRequest().SetDisplayName("Guest_"+SystemInfo.deviceUniqueIdentifier).Send((response) => {
				if (!response.HasErrors) {

				
					PlayerPrefs.SetString ("nick", nickName);
					PlayerPrefs.SetString ("registerTime", time.ToString ());

					PlayerPrefs.SetString ("loginas", "guest");
					AuthenticationGuestRequest (completion, trouble);
					//SaverLoader.instance.SaveData();
				} else {
					Debug.Log(response.Errors.GetString("USERNAME"));
					if(response.Errors.GetString("USERNAME") == "TAKEN"){
						PlayerPrefs.SetString ("nick",nickName);
						PlayerPrefs.SetString ("loginas", "guest");
						trouble (response.Errors.GetString("USERNAME"));
						AuthenticationGuestRequest(completion, trouble);
					}
					//Debug.Log(response.Errors.JSON);
					
					Debug.Log ("Error Registration...");
				}
			});
	}

	public void AuthenticationGuestRequest (Action<bool> completion, Action<string> trouble) {
		string deviceId = SystemInfo.deviceUniqueIdentifier;
		string nick = PlayerPrefs.GetString ("nick");

		new AuthenticationRequest ().SetUserName (nick + deviceId).SetPassword (nick + deviceId).Send ((response) => {
			if (!response.HasErrors) {
				GameController.instance.player.playerName = response.DisplayName;
				GameController.instance.player.newPlayer = response.NewPlayer.ToString () == "true" ? true : false;
				GameController.instance.player.playerID = response.UserId;
				GetPlayerDetails (completion, trouble);
				//EventManager.TriggerEvent (WFConstants.NotificationType.InterstatialClosed, response.ScriptData);
				
			} else {
				RegisterAsGuest(nick, completion, trouble); 
				trouble (response.Errors.JSON);
				Debug.Log ("Error Authenticating...");
			}
		});
	}

	// ******** EDIT THIS FUNCTION FOR GET DETAIL WITH PLAYER ID

	// get player's details for login
	public void GetPlayerDetails (Action<bool> completion, Action<string> trouble) {
		new AccountDetailsRequest ().Send ((response) => {
			if (!response.HasErrors) {
			
				GameController.instance.player.SetStars (response.Currencies.JSON.ToString ());
				GameController.instance.player.SetSeconds (response.VirtualGoods.JSON.ToString ());
			
				GetVirtualSecondVirtualGoods (completion, trouble);
			} else {
				trouble (response.Errors.JSON);
				Debug.Log ("Error player details...");
			}
		});
	}

	public void GetVirtualSecondVirtualGoods (Action<bool> completion, Action<string> trouble) {
		List<string> tags = new List<string> ();
		tags.Add ("second");
		new ListVirtualGoodsRequest ()
			.SetIncludeDisabled (false)
			.SetTags (tags)
			.Send ((response) => {
				if (!response.HasErrors) {
					EventManager.TriggerEvent (WFConstants.NotificationType.VirtualGoodsReceived, response);
					completion (true);
//					SaverLoader.instance.SaveData();
				} else {
					trouble (response.Errors.JSON);
					Debug.Log ("Error player seconds...");
				}
			});
	}

	// public void GetPlayerSeconds (Action<bool> completion, Action<string> trouble) {
	// 	new GetPropertyRequest ().SetPropertyShortCode ("TOTAL_SECONDS").Send ((response) => {
	// 		if (!response.HasErrors) {
	// 			Debug.Log ("GetPlayerSeconds");
	// 			GameController.instance.player.SetSeconds (response.Property.JSON.ToString ());
	// 			completion (true);
				
	// 		} else {
	// 			trouble (response.Errors.JSON);
	// 			Debug.Log ("Error player seconds...");
	// 		}
	// 	});
	// }

	public void GetPlayerDetailsWithID (string id) {
		new AccountDetailsRequest ().Send ((response) => {
			if (!response.HasErrors) {
				EventManager.TriggerEvent (WFConstants.NotificationType.PlayerProfileReceived, response.JSONString);
				SaverLoader.instance.SaveData();
			} else {
				EventManager.TriggerEvent (WFConstants.NotificationType.PlayerProfileReceivedError, null);
				Debug.Log ("Error player details...");
			}
		});
	}
	// ------------>	

	public void getServerTime (Action<LogEventResponse> completion, Action<string> trouble) {
		new LogEventRequest ().SetEventKey ("serverTime").Send ((response) => {
			if (!response.HasErrors) {
				PlayerPrefs.SetString ("serverTime", ExtentionMethods.checkDateForMiliseconds (response));
				completion (response);
			} else {
				trouble (response.Errors.JSON);
				Debug.Log (response.Errors.JSON.ToString ());
			}
		});
	}

	public void setScore (int score, int time, Action<AroundMeLeaderboardResponse> completion, Action<string> trouble) {
	
		if (score > 0 && GameController.instance.player.oldScore < score) {

			if (ExtentionMethods.CountdownToChallengeEnd () != "stop") {
				if (GameController.instance.player.PlayerLanguage == 1) {
					new LogEventRequest_SUBMIT_LEADERBOARD_tr ().Set_SCORE (score).Set_TIME (time).Send ((response) => {
						if (!response.HasErrors) {
							GetLeaderboard (completion, trouble);
							GetMyRank ();
							SaverLoader.instance.SaveData();
						} else {
							trouble (response.Errors.JSON);
						//	Debug.Log (response.Errors.JSON.ToString ());
						}
					});
				} else {
					new LogEventRequest_SUBMIT_LEADERBOARD_en ().Set_SCORE (score).Set_TIME (time).Send ((response) => {
						if (!response.HasErrors) {
							GetLeaderboard (completion, trouble);
							GetMyRank();
							SaverLoader.instance.SaveData();
						} else {
							trouble (response.Errors.JSON);
						//	Debug.Log (response.Errors.JSON.ToString ());
						}
					});
				}
			} else {
				//give error
			}
		} else {
			GetLeaderboard (completion, trouble);
			//leaderboard al
		}
	}

	public void GetLeaderboard (Action<AroundMeLeaderboardResponse> completion, Action<string> trouble) {

		if (ExtentionMethods.CountdownToChallengeEnd () != "stop") {

			string t = PlayerPrefs.GetString ("serverTime", "");

			new AroundMeLeaderboardRequest ().SetLeaderboardShortCode ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + t + "000")
				.SetEntryCount (20).SetIncludeFirst (3)
				.Send ((response) => {
					if (!response.HasErrors) {
					//	Debug.Log (response.JSONString);
						completion (response);
					} else {
						if(response.Errors.JSON.ToString () == "{\"leaderboardShortCode\":\"INVALID\"}") {
							completion (response);
							
						} else {
							trouble (response.Errors.JSON.ToString ());
						}
					//	Debug.Log ("Error Retrieving Leaderboard Data... " + response.Errors.JSON.ToString ());
					}
				});

		} else {
			ErrorThrower();
		}
	}
	public void GetLeaderboard (Action<AroundMeLeaderboardResponse> completion, Action<string> trouble, string leaderboardTime) {

		if (ExtentionMethods.CountdownToChallengeEnd () != "stop") {

			new AroundMeLeaderboardRequest ().SetLeaderboardShortCode ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + leaderboardTime + "000")
				.SetEntryCount (20).SetIncludeFirst (3)
				.Send ((response) => {
					if (!response.HasErrors) {
					//	Debug.Log (response.JSONString);
						completion (response);
					} else {
					//	Debug.Log ("Error Retrieving Leaderboard Data... " + response.Errors.JSON);
						trouble (response.Errors.JSON);
					}
				});

		} else {
			ErrorThrower();
		}
	}

	public void GetLeaderboardWithoutMe (Action<LeaderboardDataResponse> completion, Action<string> trouble, string leaderboardTime) {

		if (ExtentionMethods.CountdownToChallengeEnd () != "stop") {
			new LeaderboardDataRequest ().SetLeaderboardShortCode ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + leaderboardTime + "000")
				.SetEntryCount (20).SetIncludeFirst (3)
				.Send ((response) => {
					if (!response.HasErrors) {
						Debug.Log (response.JSONString);
						completion (response);
					} else {

						Debug.Log ("Error Retrieving Leaderboard Data... " + response.Errors.JSON);

						trouble (response.Errors.JSON);
					}
				});

		} else {
			ErrorThrower();
		}
	}

	public void GetFacebookFriendsLeaderboard (Action<LeaderboardDataResponse> completion, Action<string> trouble) {
		string t = PlayerPrefs.GetString ("serverTime", "");
		if (ExtentionMethods.CountdownToChallengeEnd () != "stop") {
		
			new SocialLeaderboardDataRequest ().SetLeaderboardShortCode ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + t + "000")
				.SetEntryCount (100).SetIncludeFirst (3).Send ((response) => {
					if (!response.HasErrors) {
					//	Debug.Log (response.JSONString);
						completion (response);
					} else {
						trouble (response.Errors.JSON);
					}
				});
		}
	}

	public void GetFacebookFriendsLeaderboard (Action<LeaderboardDataResponse> completion, Action<string> trouble, string leaderboardTime) {
		if (ExtentionMethods.CountdownToChallengeEnd () != "stop") {
			Debug.Log ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + leaderboardTime + "000");
			new SocialLeaderboardDataRequest ().SetLeaderboardShortCode ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + leaderboardTime + "000")
				.SetEntryCount (100).SetIncludeFirst (3).Send ((response) => {
					if (!response.HasErrors) {
					//	Debug.Log (response.JSONString);
						completion (response);
					} else {
                        Debug.Log(response);
                        Debug.Log("-=-=-=-=-=-=-=-=-=-=-=--=-=-=-==-=-=-=-=-=--=");
                        Debug.Log(response.Errors);
                        Debug.Log("-=-=-=-=-=-=-=-=-=-=-=--=-=-=-==-=-=-=-=-=--=");
                        Debug.Log(response.Errors.JSON);
                        trouble (response.Errors.JSON);
					}
				});
		}
	}

	public void GetMyRank () {
		getServerTime (
			delegate (LogEventResponse timeData) {
				string t = serverTime (timeData);
				if (t == "ERROR") {
					Debug.Log("Get my rank error");
					//sorun olustu
				} else {
					List<string> leaderboardCodes = new List<string> ();
					leaderboardCodes.Add ("SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + t + "000");

					new GetLeaderboardEntriesRequest ().SetLeaderboards (leaderboardCodes).Send ((response) => {
						if (!response.HasErrors) {
							EventManager.TriggerEvent (WFConstants.NotificationType.MyRankReceived, response.JSONString);
						} else {
							EventManager.TriggerEvent (WFConstants.NotificationType.MyRankReceivedError, response);
						}
					});
				}
			}, delegate (string error) {
			});
	}

	public void GetStarLetter (Action<bool> completion, Action<string> trouble) {
	//	Debug.Log (PlayerPrefs.GetString ("serverTime") + " OOOOOOOOOOOO");
		new LogEventRequest_GET_STAR_LETTERS ().Set_DAY (PlayerPrefs.GetString ("serverTime")).Send ((response) => {
			if (!response.HasErrors) {
				//Debug.Log ("-=-=-=-=-=-=-=-=-=-=-=-==- =-= -=- =- =- =- =-=" + response.JSONString);
				GameController.instance.player.SetLetters (response.ScriptData.JSON.ToString ());
				completion (true);

			} else {
				trouble ("ERROR");
			}
		});
	}

	// send taken star letters to server for not repaet
	public void SetStarLetter (List<string> letters) {

		//var json = JsonConvert.SerializeObject (letters);

		letterList ll = new letterList ();
		ll.list = letters;
		IDictionary<string, object> parsedJSON = (IDictionary<string, object>) GSJson.From (JsonConvert.SerializeObject (ll));
		GSData data = new GSData (parsedJSON);
		new LogEventRequest_SET_STAR_LETTERS ().Set_DAY (PlayerPrefs.GetString ("serverTime")).Set_LETTERS (data).Send ((response) => {
			if (!response.HasErrors) {
				EventManager.TriggerEvent (WFConstants.NotificationType.LetterStarReceived, response.ScriptData.GetInt ("STAR"));
				//completion(response.ScriptData.GetInt("STAR"));
			} else {
				//Debug.Log ("ERRORE" + response.Errors.JSON.ToString ());
			}
		});
	}

	// if user wants to play after 90
	// send to server
	public void GetExtraTime () {
		new LogEventRequest_GIVE_EXTRA_SECONDS ().Send ((response) => {
			if (!response.HasErrors) {
				EventManager.TriggerEvent (WFConstants.NotificationType.ExtraTimeReceived, response.ScriptData.GetInt ("new_star"));
				SaverLoader.instance.SaveData();
			} else {
				//Eventmanager para yok
				PopupController.instance.OpenPopup ("notEnoughGoldHeader", "notEnoughGold", "buy",
					delegate (bool b) {
						PopupController.instance.OpenShopPopup ();
					},
					"cancel",
					delegate (bool b) {
						PopupController.instance.ClosePopup ("CommonPopup");
					}, "", null
				);
			}
		});
	}

	/// Buy 1 second with gold
	public void BuyASecond () {
		int s = (GameController.instance.player.seconds - 90) + 1;
		string k = "SECOND_" + s.ToString ();

		new BuyVirtualGoodsRequest ().SetCurrencyShortCode ("STAR").SetShortCode (k).SetQuantity (1)
			.Send ((response) => {
				if (!response.HasErrors) {
					GameController.instance.player.seconds++;
					GameController.instance.player.stars -= (int) response.CurrencyConsumed;
					EventManager.TriggerEvent (WFConstants.NotificationType.BuyASecondReceived, null);
					SaverLoader.instance.SaveData();
				} else {

				}
			});
	}

	// if user change language setting
	// for writing to server
	public void SetLanguage (int lang) {
		
		new LogEventRequest_SET_PLAYERLANG ().Set_LANG (lang).Send ((response) => {
			if (!response.HasErrors) {
				EventManager.TriggerEvent (WFConstants.NotificationType.NewLangReceived, null);
				SaverLoader.instance.SaveData();
			} else {

				Debug.Log ("error while changing language " + response.Errors.JSON);
			}
		});
	}

	// Buy stars from stores
	public void BuyStars (string signature, string purchaseData) {
#if UNITY_IOS 
	new GameSparks.Api.Requests.IOSBuyGoodsRequest()
		.SetCurrencyCode(signature)
        .SetReceipt(purchaseData)
		.SetSandbox(false)
		.Send((response) => {
				if (!response.HasErrors) {
					// trigger a EVENT for update UI and PurchaseProcessingResult update to Complete
					EventManager.TriggerEvent (WFConstants.NotificationType.IAPReceived, null);
					SaverLoader.instance.SaveData();
				} else {

					Debug.Log ("Purchase error: " + response.Errors.JSON);
				}
			});
#elif UNITY_ANDROID
	//	GooglePurchaseData data = new GooglePurchaseData (receipt);
	//	Debug.Log ("data.inAppDataSignature : " + data.inAppDataSignature + " || data.inAppPurchaseData : " + data.inAppPurchaseData);
		new GameSparks.Api.Requests.GooglePlayBuyGoodsRequest ()
			.SetSignature (signature)
			.SetSignedData (purchaseData)
			.Send ((response) => {
				if (!response.HasErrors) {
					// trigger a EVENT for update UI and PurchaseProcessingResult update to Complete
					EventManager.TriggerEvent (WFConstants.NotificationType.IAPReceived, null);
					SaverLoader.instance.SaveData();
				} else {

					Debug.Log ("Purchase error: " + response.Errors.JSON);
				}
			});
#endif
	}

	// Register for push notifications
	public void PushRegisterRequest (string deviceOS, string pushId) {
		new PushRegistrationRequest ()
			.SetDeviceOS (deviceOS)
			.SetPushId (pushId)
			.Send ((response) => {
				string registrationId = response.RegistrationId;
				//GSData scriptData = response.ScriptData; 
			});
	}

	public string serverTime (LogEventResponse timeData) {
		string t;

		t = timeData.ScriptData.GetInt ("Year").ToString ();
		t += ((timeData.ScriptData.GetInt ("Month") + 1) < 10 ? "0" : "") + (timeData.ScriptData.GetInt ("Month") + 1).ToString ();
		t += (timeData.ScriptData.GetInt ("Day") < 10 ? "0" : "") + timeData.ScriptData.GetInt ("Day").ToString ();
		
		if (t == PlayerPrefs.GetString ("boards")) {
			DateTime today = new DateTime ((int) timeData.ScriptData.GetInt ("Year"), (int) timeData.ScriptData.GetInt ("Month") + 1, (int) timeData.ScriptData.GetInt ("Day"));
			int time = (int) (today.Subtract (new DateTime (1970, 1, 1))).TotalSeconds;

			return time.ToString ();
		} else {

			return "ERROR";
		}
	}

	void OnApplicationQuit () {
		GS.Disconnect ();
		GS.ShutDown ();
	}

	private void ErrorThrower () {
		EventManager.TriggerEvent (WFConstants.NotificationType.ServerError, null);
	}

}