﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using Facebook;
using Facebook.Unity;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using tQsLib;
using UnityEngine;

public class LoginManager : MonoBehaviour {

	public static LoginManager instance;

	private Action<bool> completion;
	private Action<string> trouble;

	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	public void RegisterAsFacebook (Action<bool> completion, Action<String> trouble) {
		FacebookManager.instance.LoginFacebookAndGetProfile (
			delegate (object fbResult) {
				var data = fbResult as IResult;
				GameController.instance.player.SetFacebookParams (data.RawResult.ToString ());
				StartCoroutine (AuthenticationWithFacebook (completion, trouble));

			},
			delegate (object str) {
				trouble (str.ToString ());
			});
	}

	public void RefreshFAcebookLogin (Action<bool> completion, Action<String> trouble) {
		this.completion = completion;
		this.trouble = trouble;

		if (!FB.IsInitialized) {
			FB.Init (FBInitilized);
		} else {
			FBInitilized ();
		}
	}

	private void FBInitilized () {

		if (AccessToken.CurrentAccessToken == null || !FB.IsLoggedIn) {
			GameController.instance.isRefreshingAccessToken = true;
			FacebookManager.instance.LogoutFacebook ();
			FacebookManager.instance.LoginFacebookAndGetProfile (
				delegate (object fbResult) {
					var data = fbResult as IResult;

					GameController.instance.player.SetFacebookParams (data.RawResult.ToString ());
					StartCoroutine (AuthenticationWithFacebook (completion, trouble));

				},
				delegate (object str) {
					trouble (str.ToString ());
				});
		} else if (AccessToken.CurrentAccessToken.ExpirationTime < DateTime.Now) {
			FB.Mobile.RefreshCurrentAccessToken (RefreshCallback);
		} else {
			StartCoroutine (AuthenticationWithFacebook (completion, trouble));
		}
	}
	void RefreshCallback (IAccessTokenRefreshResult result) {
		if (FB.IsLoggedIn) {
			StartCoroutine (AuthenticationWithFacebook (completion, trouble));

		}
	}

	public void RegisterGuest (string nickName, Action<bool> completion, Action<String> trouble) {
		StartCoroutine (RegisterAsGuest (nickName, completion, trouble));
	}

	public void AuthenticationGuest (Action<bool> completion, Action<String> trouble) {
		StartCoroutine (AuthenticationAsGuest (completion, trouble));
	}

	private IEnumerator RegisterAsGuest (string nickName, Action<bool> completion, Action<String> trouble) {
		if (GameSparkManager.instance.isGSconnected) {
			GameSparkManager.instance.RegisterAsGuest (nickName, completion, trouble);

		} else {
			yield return new WaitForSeconds (1);
			StartCoroutine (RegisterAsGuest (nickName, completion, trouble));
		}
	}

	private IEnumerator AuthenticationAsGuest (Action<bool> completion, Action<String> trouble) {
		if (GameSparkManager.instance.isGSconnected) {
			GameSparkManager.instance.AuthenticationGuestRequest (completion, trouble);
		} else {
			yield return new WaitForSeconds (1);
			StartCoroutine (AuthenticationAsGuest (completion, trouble));
		}
	}

	private IEnumerator AuthenticationWithFacebook (Action<bool> completion, Action<String> trouble) {

		if (GameSparkManager.instance.isGSconnected) {
			GameSparkManager.instance.FacebookAuthentication (completion, trouble);

		} else {
			yield return new WaitForSeconds (1);
			StartCoroutine (AuthenticationWithFacebook (completion, trouble));
		}
	}

}