﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using tQsLib;
public class ProfileController : MonoBehaviour {

	public Text playerNameText;
	public PlayerAvatarController avatar;

	public GameObject profilePanel;
	public GameObject achievementPanel;
	 

	private Player player;
	// Use this for initialization
	void Start () {
		player = new Player();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	// Get Player data from server when Enable
	void OnEnable() {
		EventManager.StartListening (WFConstants.NotificationType.PlayerProfileReceived, SetPlayerProfileData);
		EventManager.StartListening (WFConstants.NotificationType.PlayerProfileReceivedError, PlayerProfileError);
		profilePanel.SetActive(false);
		achievementPanel.SetActive(false);
		PopupController.instance.OpenPopup("LoadingPopup");
		
		// *-> Add player id when code ready
		GameSparkManager.instance.GetPlayerDetailsWithID("");
	}

	// If no error and data received
	private void SetPlayerProfileData(object obj){
		EventManager.StopListening (WFConstants.NotificationType.PlayerProfileReceived, SetPlayerProfileData);
		EventManager.StopListening (WFConstants.NotificationType.PlayerProfileReceivedError, PlayerProfileError);
		
		Debug.Log("PROFILE   " + obj.ToString());

		//player.SetStars(obj.ToString());
		
		Debug.Log(obj.ToString());

		profilePanel.SetActive(true);
		achievementPanel.SetActive(true);
		PopupController.instance.ClosePopup("LoadingPopup");
	}

	// HUSTON!!!
	private void PlayerProfileError(object obj) {
		PopupController.instance.ClosePopup("LoadingPopup");
		PopupController.instance.ClosePopup("ProfilePopup");
	}

	void OnDisable() {
		EventManager.StopListening (WFConstants.NotificationType.PlayerProfileReceived, SetPlayerProfileData);
		EventManager.StopListening (WFConstants.NotificationType.PlayerProfileReceivedError, PlayerProfileError);
		
	}

}
