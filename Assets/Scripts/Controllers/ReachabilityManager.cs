﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ReachabilityManager : MonoBehaviour {

	public static ReachabilityManager instance;
	public GameObject noConnection;
	private bool _isReachable;
	private bool isReachable = true;

	public bool IsReachable {
		get {
			_isReachable = Application.internetReachability != NetworkReachability.NotReachable;
			return _isReachable;
		}
		set {
		//	Debug.Log(value +" != "+ _isReachable);
			if (value != isReachable) {
			//	Debug.Log("CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED CHANGED ");
				EventManager.TriggerEvent (WFConstants.NotificationType.ReachibilityChanged, null);
				//NotificationCenter.Instance.PostNotification (this, OKConstants.NotificationType.ReachibilityChanged, value);
				isReachable = value;
			}
			//_isReachable = value;

		}
	}
	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	void Start() {
		IsReachable = Application.internetReachability != NetworkReachability.NotReachable;
		StartCoroutine (CheckConnection ());
	}

	IEnumerator CheckConnection () {
		while (true) {
			IsReachable = (Application.internetReachability != NetworkReachability.NotReachable);
			noConnection.SetActive(!IsReachable);
			yield return new WaitForSeconds (1.0f);
		}
	}

}