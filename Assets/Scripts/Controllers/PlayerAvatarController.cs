﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerAvatarController : MonoBehaviour {

	public Image playerAvatar;
	public GameObject button;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void setAvatar(string facebookId, bool isClickable = false){
		AvatarLoader.instance.setAvatar(facebookId, playerAvatar);	
//		button.SetActive(isClickable);	
	}

	public void openPlayerProfile() {
		PopupController.instance.OpenPopup("ProfilePopup");
	}
}
