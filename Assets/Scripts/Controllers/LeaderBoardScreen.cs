﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Api.Responses;
using tQsLib;

public class LeaderBoardScreen : MonoBehaviour {

    //  public LeaderBoardScreen instance;
    public LeaderboardController lbc;

    public GameObject leaderboardSelectPanel;
    public Toggle globalButton;
    public Toggle facebookButton;
    public Toggle dayButton1;
    public Toggle dayButton2;
    public Toggle dayButton3;

    public Text button2Text;
    public Text button3Text;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {

    }

    void Awake() {
        // if (instance == null) {
        //  instance = this;
        //  return;
        // }

    }

    void OnEnable() {
        AppodealManager.instance.showInterstitial();
        globalButton.isOn = true;
        dayButton1.isOn = true;
        leaderboardSelectPanel.SetActive(PlayerPrefs.GetString("loginas") == "fb");
        PopupController.instance.OpenPopup("LoadingPopup");
        //globalButton.isOn = true;
        //facebookButton.isOn = false;

        lbc.ClearData();

        System.DateTime date1 = ExtentionMethods.UnixTimeStampToDateTime(setLeaderboardTime(1)).Date;
        System.DateTime date2 = ExtentionMethods.UnixTimeStampToDateTime(setLeaderboardTime(2)).Date;
        button2Text.text = date1.Day + "/" + date1.Month + "/" + date1.Year;
        button3Text.text = date2.Day + "/" + date2.Month + "/" + date2.Year; ;
        getGlobalDataFromServer(0);
    }

    private long setLeaderboardTime(int day) {
        long a = System.Int32.Parse(PlayerPrefs.GetString("serverTime", ""));

        return (a - (day * 24 * 60 * 60)) * 1000;
    }

    public void getGlobalDataFromServer(int day) {
        if (globalButton.isOn) {
            //globalButton.isOn = true;
            //facebookButton.isOn = true;
            GameSparkManager.instance.GetLeaderboard(
                delegate (AroundMeLeaderboardResponse response) {
                    lbc.SetData(new LeaderBoardPlayers(response), "global");
                    PopupController.instance.ClosePopup("LoadingPopup");
                },
                delegate (string Error) {
                    if (Error == "{\"leaderboardShortCode\":\"NO_ENTRY\"}") {
                        getGlobalDataFromWithoutMeServer(day);
                    } else {
                        lbc.SetData(null, "global");
                        PopupController.instance.ClosePopup("LoadingPopup");
                        Debug.Log(Error);
                    }
                }, (setLeaderboardTime(day) / 1000).ToString()
            );
        }
    }

    public void getGlobalDataFromWithoutMeServer(int day) {
        if (globalButton.isOn) {
            Debug.Log("global baski");
            //globalButton.isOn = true;
            //facebookButton.isOn = true;
            GameSparkManager.instance.GetLeaderboardWithoutMe(
                delegate (LeaderboardDataResponse response) {
                    lbc.SetData(new LeaderBoardPlayers(response), "global");
                    PopupController.instance.ClosePopup("LoadingPopup");
                },
                delegate (string Error) {
                    if (Error == "{\"leaderboardShortCode\":\"NO_ENTRY\"}") {
                    }
                    lbc.SetData(null, "global");
                    PopupController.instance.ClosePopup("LoadingPopup");
                    Debug.Log(Error);
                }, (setLeaderboardTime(day) / 1000).ToString()
            );
        }
    }

    public void getFacebookDataFromServer(int day) {
        Debug.Log("FACEBOOK DATA 1");
        if (facebookButton.isOn) {
            GameSparkManager.instance.GetFacebookFriendsLeaderboard(
                delegate (LeaderboardDataResponse response) {
                    Debug.Log("FACEBOOK DATA GELDI");
                    lbc.SetData(new LeaderBoardPlayers(response), "fb");
                },
                delegate (string Error) {
                    Debug.Log("FACEBOOK DATA SORUNLU GELDI");
                }, (setLeaderboardTime(day) / 1000).ToString()
            );
        }
    }

    public void getDataFromServer() {
        Debug.Log("gogoogogoogogogoogoogoogogoogogogogo  " + globalButton.isOn);
        int d = 0;
        if (dayButton2.isOn) {
            d = 1;
        } else if (dayButton3.isOn) {
            d = 2;
        }

        if (globalButton.isOn) {
            getGlobalDataFromServer(d);
        } else {
            getFacebookDataFromServer(d);
        }
    }

    public void ClosePopup() {
        PopupController.instance.ClosePopup("LeaderBoardScreen");
    }
}
