﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WFConstants {

	public const int boardWidth = 5;
	public const int boardHeight = 5;
	public const int tileWidth = 200;
	public const int tileHeight = 200;
	public const int BRICK_TOUCH_MARGIN = 40;

	public const int GAME_VERSION = 1;


	public const int TR = 1;
	public const int EN = 2;

	public const int ScheduledNotificationReward = 1;
	public static readonly string mailAdress = "info@thequixoticssolutions.com";

	public static readonly List<string> trLetters = new List<string>{
		"A",
		"B",
		"C",
		"Ç",
		"D",
		"E",
		"F",
		"G",
		"Ğ",
		"H",
		"I",
		"İ",
		"J",
		"K",
		"L",
		"M",
		"N",
		"O",
		"Ö",
		"P",
		"R",
		 "S",
		"Ş",
		 "T",
		"U",
		"Ü",
		"V",
		"Y", 
		"Z"
	};
	public static readonly List<string> enLetters = new List<string>{
		"A",
		"B",
		"C",
		"D",
		"E",
		"F",
		"G",
		"H",
		"I",
		"J",
		"K",
		"L",
		"M",
		"N",
		"O",
		"P",
		"Q",
		"R",
		"S",
		"T",
		"U",
		"V",
		"W",
		"X",
		"Y", 
		"Z"
	};
	public static readonly Dictionary<string, int> trLettersScores = new Dictionary<string, int> {
		{ "A", 1 },
		{ "B", 3 },
		{ "C", 4 },
		{ "Ç", 4 },
		{ "D", 3 },
		{ "E", 1 },
		{ "F", 7 },
		{ "G", 5 },
		{ "Ğ", 8 },
		{ "H", 5 },
		{ "I", 2 },
		{ "İ", 1 },
		{ "J", 10 },
		{ "K", 1 },
		{ "L", 1 },
		{ "M", 2 },
		{ "N", 1 },
		{ "O", 2 },
		{ "Ö", 7 },
		{ "P", 5 },
		{ "R", 1 },
		{ "S", 2 },
		{ "Ş", 4 },
		{ "T", 1 },
		{ "U", 2 },
		{ "Ü", 3 },
		{ "V", 7 },
		{ "Y", 3 },
		{ "Z", 4 }
	};

	public static readonly Dictionary<string, int> enLettersScores = new Dictionary<string, int> {
		{ "A", 1 },
		{ "B", 3 },
		{ "C",3 },
		{ "D",2 },
		{ "E",1 },
		{ "F",4 },
		{ "G",2 },
		{ "H",4 },
		{ "I",1 },
		{ "J",8 },
		{ "K",5 },
		{ "L",1 },
		{ "M",3 },
		{ "N",1 },
		{ "O",1 },
		{ "P",3 },
		{ "Q",10 },
		{ "R",1 },
		{ "S",1 },
		{ "T",1 },
		{ "U",1 },
		{ "V",4 },
		{ "W",4 },
		{ "X",8 },
		{ "Y",4 },
		{ "Z",10 }
	};


	public enum NotificationType {
		LetterStarReceived,
		ExtraTimeReceived,
		MyRankReceived,
		MyRankReceivedError,
		NewLangReceived,
		LeaderBoardReceived,
		LeaderBoardReceivedError,
		PlayerProfileReceived,
		PlayerProfileReceivedError,
		RewardedVideoFinish,
		VirtualGoodsReceived,
		BuyVirtualGoodsReceived,
		BuyASecondReceived,
		IAPReceived,
		IAPSuccess,
		IAPEnded,
		DailyRewardReceived,
		GotDailyReward,
		ServerError,
		ReachibilityChanged,
		InterstatialClosed
	
	}
}
