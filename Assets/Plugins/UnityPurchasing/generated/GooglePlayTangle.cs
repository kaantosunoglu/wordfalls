#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("uwmKqbuGjYKhDcMNfIaKioqOi4jM996oDmvAXcyluqGuUcUTHHhsloJ2ZoOmct8BfN+Ywx7mEOg4htiFlqu6yEhCnFTwQFHCatlP8ykq3SbOSw5OoTtkWGJJ+cj2slCK6ze+UhXB3oXDXa+X857wG+ZabQpAwaCbuntZLPeR9NYhOz5CFZfbYrtX/BObsRPlwHxoxdYLRWp+GyBFfrnAIwmKhIu7CYqBiQmKiosFxph4n2PaQyATx3XaI8z/dPLwcOonq/in5gnJ8q/1JqIIYvchmr6v4H/o/2SJBce6nLTrNP4s3dPVV07ojKskhVuZn+vaEA0tjk9gY+3Dq0eEB6D2AO7pSRsPrTC008RYWMm4HkGSunKuvX8VIU0Ay84OZomIiouK");
        private static int[] order = new int[] { 0,8,5,5,10,5,9,11,8,13,10,12,13,13,14 };
        private static int key = 139;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
#endif
