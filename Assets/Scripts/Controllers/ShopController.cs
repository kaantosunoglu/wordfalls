﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopController : MonoBehaviour {

	public Text secondCost;
	public Button addTimeButton;

	public Text gold150;
	public Text gold450;
	public Text gold1500;
	public Text gold3000;
	public Text gold5000;
	public Text gold15000;
	public Text cost150;
	public Text cost450;
	public Text cost1000;
	public Text cost3000;
	public Text cost5000;
	public Text cost15000;

	private int cost;
	private string product;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	void OnEnable () {
		EventManager.StartListening (WFConstants.NotificationType.BuyASecondReceived, BuySecondReceived);
		EventManager.StartListening (WFConstants.NotificationType.IAPSuccess, IAPReceived);
		SetSecondCost ();
		setIAPInformations ();
	}
	/// <summary>
	/// This function is called when the behaviour becomes disabled or inactive.
	/// </summary>
	void OnDisable () {
		EventManager.StopListening (WFConstants.NotificationType.IAPSuccess, IAPReceived);		
		EventManager.StopListening (WFConstants.NotificationType.BuyASecondReceived, BuySecondReceived);
	}

	private void SetSecondCost () {
		int s = (GameController.instance.player.seconds - 90) + 1;
		string k = "SECOND_" + s.ToString ();
		cost = GameController.instance.secondVirtualGoods.secondVirtualGoods[k];
		if (GameController.instance.secondVirtualGoods.secondVirtualGoods.ContainsKey (k)) {
			secondCost.text = cost.ToString ("N0");
			addTimeButton.interactable = true;
		} else {
			addTimeButton.interactable = false;
		}
		PopupController.instance.ClosePopup ("LoadingPopup");

	}

	private void BuySecondReceived (object o) {
		GameAnalyticsManager.instance.TrackResourceEventSource("time", 1, "buy_time", "Shop");
		GameAnalyticsManager.instance.TrackResourceEventSink("star", cost, "buy_time", "Shop");
		SetSecondCost ();
		PopupController.instance.OpenPopup ("", "secondBuyed", "OK",
			delegate (bool b) {
				PopupController.instance.ClosePopup ("CommonPopup");
			},
			"", null, "", null);
	}
	private void IAPReceived (object o) {
		PopupController.instance.ClosePopup ("LoadingPopup");
		switch (product) {
			case "STAR_PACK_150":
				GameController.instance.player.stars += 150;
				break;
			case "STAR_PACK_450":
				GameController.instance.player.stars += 450;
				break;
			case "STAR_PACK_1500":
				GameController.instance.player.stars += 1500;
				break;
			case "STAR_PACK_3000":
				GameController.instance.player.stars += 3000;
				break;
			case "STAR_PACK_5000":
				GameController.instance.player.stars += 5000;
				break;
			case "STAR_PACK_15000":
				GameController.instance.player.stars += 15000;
				break;
		}
		EventManager.TriggerEvent (WFConstants.NotificationType.IAPEnded, null);

	}

	public void BuySecond () {

		if (GameController.instance.player.stars >= cost) {
			//if (1 < 0) {
			PopupController.instance.OpenPopup ("LoadingPopup");
			GameSparkManager.instance.BuyASecond ();
		} else {
			// altin al yavrum mesaji cikmali
			PopupController.instance.OpenPopup ("notEnoughGoldHeader", "notEnoughGold", "buy",
				delegate (bool b) {
					PopupController.instance.OpenShopPopup ();
				},
				"cancel",
				delegate (bool b) {
					PopupController.instance.ClosePopup ("CommonPopup");
				}, "", null
			);
		}

	}

	public void BuyStar (string productId) {
		product = productId;
		PopupController.instance.OpenPopup ("LoadingPopup");
		IAPManager.instance.BuyProductID (productId);
	}

	public void CloseShop () {
		PopupController.instance.ClosePopup (this.gameObject);
	}

	private void setIAPInformations () {

		gold150.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_150").metadata.localizedTitle;
		gold450.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_450").metadata.localizedTitle;
		gold1500.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_1500").metadata.localizedTitle;
		gold3000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_3000").metadata.localizedTitle;
		gold5000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_5000").metadata.localizedTitle;
		gold15000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_15000").metadata.localizedTitle;
		cost150.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_150").metadata.localizedPriceString;
		cost450.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_450").metadata.localizedPriceString;
		cost1000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_1500").metadata.localizedPriceString;
		cost3000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_3000").metadata.localizedPriceString;
		cost5000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_5000").metadata.localizedPriceString;
		cost15000.text = IAPManager.instance.m_StoreController.products.WithID ("STAR_PACK_15000").metadata.localizedPriceString;
	}
}