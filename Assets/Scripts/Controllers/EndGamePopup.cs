﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EndGamePopup : MonoBehaviour {

	public Text counter;
	public Text counter2;
	public GameObject playPanel;
	public GameObject waitPanel;

	public Button extraTimeWithGoldButton;
	public Button extraTimeWithAdsButton;
	public Text goldTimer;
	public Text adTimer;
	public GameObject leaderboardSelectPanel;
	public GameObject leaderBoardPanel;
	public LeaderboardController lbc;

	public Text secondCost;
	public Button addTimeButton;
	public GameObject howToMask;
	public GameObject click1;
	
	public GameObject click2;

	public GameObject textPanel;
	public GameObject howToText;
	public RectTransform rtTextPanel;
//	public RectTransform click;
	public GameObject backButton;
	public GameObject buyGoldButton;
	public Transform howToMaskT;
	public Transform buyGoldT;
	public Transform playButton;
	public GameObject gameSceen;

	public GameObject playButtonsPanel;
	public GameObject fakePlayButton;
	public GameObject fakeBuyGoldTButton;
	public GameObject fakeExtraTimeWithAdButton;
	
	private bool isChallengeOn = true;
	private bool isCountDownActive = false;
	private string countdownTime;
	private float currentTime;
	private bool willReset;
	private int cost;
	private string endType;

	private int addCount;
	private bool withAds = false;
	// Use this for initialization
	void Start () {
		endType = "";
		addCount = 0;
		
	}

	// Update is called once per frame
	void Update () {
		if (ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged) {
			if (isChallengeOn) {
				countdownTime = ExtentionMethods.CountdownToChallengeEnd ();
				if (countdownTime == "stop") {
					playPanel.SetActive (false);
					waitPanel.SetActive (true);
					isChallengeOn = false;
					willReset = true;
				} else {

					counter.text = countdownTime;
				}
			} else {
				countdownTime = ExtentionMethods.CountdownToChallengeStart ();
				if (countdownTime == "stop") {
					GameController.instance.LoadAgain (gameObject);
					isChallengeOn = true;
					playPanel.SetActive (true);
					waitPanel.SetActive (false);
				} else {
					willReset = true;
					counter2.text = countdownTime;
				}
			}
		}

		if (isCountDownActive && GameController.instance.player.seconds > 90)
			UpdateCountDowntUI ();
	}

	void OnEnable () {
		EventManager.StartListening (WFConstants.NotificationType.InterstatialClosed, RestartGameAfterAd);
		click1.SetActive (false);
		click2.SetActive (false);
		bool reachable = ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged;
		if (reachable) {
			GameAnalyticsManager.instance.screenOpen ("EndGameScreen");
			endType = GameController.instance.firstEnd ? "FirstEnd" : endType;
			EventManager.StartListening (WFConstants.NotificationType.BuyASecondReceived, BuySecondReceived);
			addTimeButton.gameObject.SetActive (true);
			SetSecondCost ();
		} else {
			addTimeButton.gameObject.SetActive (false);
		}
		
		howToMask.SetActive (GameController.instance.player.seconds == 90);
		click1.SetActive (GameController.instance.player.seconds == 90);
		fakeBuyGoldTButton.SetActive(GameController.instance.player.seconds == 90);
		fakePlayButton.SetActive(GameController.instance.player.seconds == 90);
		fakeExtraTimeWithAdButton.SetActive(GameController.instance.player.seconds == 90);
		backButton.SetActive (GameController.instance.player.seconds > 90);
		playButtonsPanel.SetActive(GameController.instance.player.seconds > 90);

		leaderboardSelectPanel.SetActive (PlayerPrefs.GetString ("loginas") == "fb");

		isCountDownActive = true;
		if (ExtentionMethods.CountdownToChallengeEnd () == "stop") {
			isChallengeOn = false;
			willReset = true;
		}
		if (GameController.instance.firstEnd) {
			
			extraTimeWithGoldButton.interactable = reachable;
			extraTimeWithAdsButton.interactable = reachable ? AppodealManager.instance.isRewardedReady : false;
			goldTimer.gameObject.SetActive (reachable);
			adTimer.gameObject.SetActive (reachable);
			leaderboardSelectPanel.SetActive (PlayerPrefs.GetString ("loginas") == "fb" && reachable);
			leaderBoardPanel.SetActive (reachable);
			currentTime = 11;
		} else {
			howToMaskT.SetAsLastSibling ();
			buyGoldT.SetAsLastSibling ();
			rtTextPanel.offsetMax = new Vector2 (rtTextPanel.offsetMax.x, 0);
			rtTextPanel.offsetMin = new Vector2 (rtTextPanel.offsetMin.x, 150);
			howToText.GetComponent<TextController> ().text = "step8";
			howToText.GetComponent<TextController> ().SetText ();
			click2.SetActive (GameController.instance.player.seconds == 90);
			click1.SetActive (false);
			extraTimeWithAdsButton.interactable = false;
			extraTimeWithGoldButton.interactable = false;
			goldTimer.gameObject.SetActive (false);
			adTimer.gameObject.SetActive (false);
			showRatePopup ();
		}

	}

	void OnDisable () {
		EventManager.StopListening (WFConstants.NotificationType.BuyASecondReceived, BuySecondReceived);
		EventManager.StopListening (WFConstants.NotificationType.InterstatialClosed, RestartGameAfterAd);
	}

	private void RestartGameAfterAd (object o) {
		addCount--;
		withAds = false;
		RestartGame ();
	}
	public void RestartGame () {

		if (ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged) {
			addCount = GameController.instance.firstEnd ? addCount + 2 : addCount;
			if (addCount > 3 && withAds && AppodealManager.instance.isInterstitialReady) {
				AppodealManager.instance.showInterstitial ();
			} else {
				endType += ":RestartGame";
				GameAnalyticsManager.instance.trackFunnel ("Funnel:" + endType);
				GameController.instance.firstEnd = true;
				isCountDownActive = false;
				GameSparkManager.instance.GetStarLetter (
					delegate (bool response) {
						lbc.ClearData ();
						BoardController.instance.RestartGame (willReset);
						withAds = GameController.instance.firstEnd;
					},
					delegate (string obj) {
						// popup ile bir sorun var denilebilir belki
						GameController.instance.LoadAgain (gameSceen);
						GameController.instance.firstEnd = true;
						this.gameObject.SetActive (false);
					});
			}
		} else if (ReachabilityManager.instance.IsReachable && GameController.instance.isReachablityChanged) {
			GameController.instance.isReachablityChanged = false;
			GameController.instance.LoadAgain (gameSceen);
			GameController.instance.firstEnd = true;
			this.gameObject.SetActive (false);
		} else {
			isCountDownActive = false;
			BoardController.instance.RestartGame (willReset);
		}
	}

	private void UpdateCountDowntUI () {
		if (GameController.instance.firstEnd) {
			if (currentTime > 0) {
				currentTime -= Time.deltaTime;

				goldTimer.text = ((int) currentTime).ToString ();
				adTimer.text = ((int) currentTime).ToString ();
				//timerImage.fillAmount = currentTime / totalTime;
			} else {
				GameController.instance.firstEnd = false;
				goldTimer.text = "0";
				goldTimer.gameObject.SetActive (false);
				adTimer.text = "0";
				adTimer.gameObject.SetActive (false);
				extraTimeWithGoldButton.interactable = false;
				extraTimeWithAdsButton.interactable = false;
			}
		} else {
			//timerImage.gameObject.SetActive(false);
		}
	}

	public void GetExtraTime () {
		addCount++;
		endType += ":GetExtraTimeWithGold";
		GameAnalyticsManager.instance.trackFunnel ("Funnel:" + endType);

		if (GameController.instance.player.stars < 25) {
			PopupController.instance.OpenPopup ("notEnoughGoldHeader", "notEnoughGold", "buy",
				delegate (bool b) {
					PopupController.instance.OpenShopPopup ();
				},
				"cancel",
				delegate (bool b) {
					PopupController.instance.ClosePopup ("CommonPopup");
				}, "", null
			);
		} else {
			GameController.instance.firstEnd = false;
			lbc.ClearData ();
			BoardController.instance.getExtraTime ();
		}
	}

	public void GetExtraTimeWithAd () {
		addCount--;
		endType += ":GetExtraTimeWithAd";
		GameAnalyticsManager.instance.trackFunnel ("Funnel:" + endType);
		UnityThreadHelper.Dispatcher.Dispatch (() => {
			EventManager.StartListening (WFConstants.NotificationType.RewardedVideoFinish, PlayWithAd);
		});
		AppodealManager.instance.showRewardedVideo ();
		withAds = false;
	}

	private void PlayWithAd (object o) {
		UnityThreadHelper.Dispatcher.Dispatch (() => {
			EventManager.StopListening (WFConstants.NotificationType.RewardedVideoFinish, PlayWithAd);
		});
		GameController.instance.firstEnd = false;
		withAds = false;
		lbc.ClearData ();
		BoardController.instance.setExtraTime (null);
	}

	private void SetSecondCost () {
		if (!GameController.instance.isReachablityChanged && ReachabilityManager.instance.IsReachable) {
			int s = (GameController.instance.player.seconds - 90) + 1;
			string k = "SECOND_" + s.ToString ();
			cost = GameController.instance.secondVirtualGoods.secondVirtualGoods[k];
			if (GameController.instance.secondVirtualGoods.secondVirtualGoods.ContainsKey (k)) {
				secondCost.text = cost.ToString ("N0");
				addTimeButton.interactable = true;
			} else {
				addTimeButton.interactable = false;
			}
			PopupController.instance.ClosePopup ("LoadingPopup");
		} else {
			addTimeButton.gameObject.SetActive (false);
		}
	}
	private void BuySecondReceived (object o) {
		GameAnalyticsManager.instance.TrackResourceEventSource ("time", 1, "buy_time", "EndGame");
		GameAnalyticsManager.instance.TrackResourceEventSink ("star", cost, "buy_time", "EndGame");
		SetSecondCost ();
		PopupController.instance.OpenPopup ("", "secondBuyed", "OK",
			delegate (bool b) {
				PopupController.instance.ClosePopup ("CommonPopup");
				if (GameController.instance.player.seconds == 91) {
					howToMaskT.SetAsLastSibling ();
					playButton.SetAsLastSibling ();
					click1.transform.SetAsLastSibling();
					rtTextPanel.offsetMax = new Vector2 (rtTextPanel.offsetMax.x, 248);
					rtTextPanel.offsetMin = new Vector2 (rtTextPanel.offsetMin.x, 319);
					howToText.GetComponent<TextController> ().text = "step9";
					howToText.GetComponent<TextController> ().SetText ();
					click2.SetActive (false);
					click1.SetActive (true);
					click1.transform.position = new Vector2(-2,click1.transform.position.y);
					
				}
			},
			"", null, "", null);
	}
	public void BuySecond () {
		click2.SetActive (false);
		click1.SetActive (false);
		endType += ":BuySecond";
		GameAnalyticsManager.instance.trackFunnel ("Funnel:" + endType);
		PopupController.instance.OpenPopup ("LoadingPopup");
		if (GameController.instance.player.stars >= cost) {
			GameSparkManager.instance.BuyASecond ();

		} else {
			// altin al yavrum mesaji cikmali
			PopupController.instance.OpenPopup ("notEnoughGoldHeader", "notEnoughGold", "buy",
				delegate (bool b) {
					PopupController.instance.OpenShopPopup ();
				}, "cancel",
				delegate (bool b) {
					PopupController.instance.ClosePopup ("CommonPopup");
				}, "", null);
		}

	}

	private void showRatePopup () {
		int rate = PlayerPrefs.GetInt ("rate", 1);
		if (rate == 999) {
			return;
		} else if (rate == 0 || rate % 10 != 0) {
			rate++;
			PlayerPrefs.SetInt ("rate", rate);

		} else {
			PopupController.instance.OpenPopup ("", "rateMe", "rate",
				delegate (bool b) {
					GameController.instance.OpenMarket ();
					PlayerPrefs.SetInt ("rate", 0);
					PopupController.instance.ClosePopup ("CommonPopup");
				},
				"cancel",
				delegate (bool b) {
					PopupController.instance.ClosePopup ("CommonPopup");
					PlayerPrefs.SetInt ("rate", 999);
				}, "later",
				delegate (bool b) {
					PopupController.instance.ClosePopup ("CommonPopup");
					PlayerPrefs.SetInt ("rate", 0);
				});

		}

	}
}