﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using tQsLib;


public class LetterTile : MonoBehaviour {

	public Text letterText;
	public Text pointText;
	public Image backGround;
	public Tile settings { get; set; }
	public Sprite letterBackImage;
	public Sprite selectedImage;
	public Sprite goldBackImage;
	public Sprite goldSelectedImage;
	public Sprite multipleerBackImage;
	public Sprite multipleerSelectedImage;

	public bool isPlaying = false;

	private bool isSelected = false;
	private bool isJoker;
	public bool isStar;
	public bool isMultiplier;
	public int point;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void setText(string letter){
		if (isJoker) {
			letterText.text = " ";
		} else {
			letterText.text = letter;
		}
	}

	private void setPoint(int p){
		pointText.text = p.ToString ();
		//pointText.text = settings.code;
	}

	private void setPosition(int posX, int posY){
		if (!isPlaying) {	
			//transform.GetComponent<RectTransform> ().localPosition = new Vector2 (posX * WFConstants.tileWidth, posY * WFConstants.tileHeight);
			LeanTween.move(transform.GetComponent<RectTransform> (), new Vector3 (posX * WFConstants.tileWidth, posY * WFConstants.tileHeight, 0), 0f);
			isPlaying = true;
		} else {
			if (isSelected) {
				transform.GetComponent<RectTransform> ().localPosition = new Vector2 (posX * WFConstants.tileWidth, (posY + WFConstants.boardHeight) * WFConstants.tileHeight);
				setUnselect ();
				LeanTween.move(transform.GetComponent<RectTransform> (), new Vector3 (posX * WFConstants.tileWidth, posY * WFConstants.tileHeight, 0), 0.5f);
			} else {
				//transform.GetComponent<RectTransform> ().localPosition = new Vector2 (posX * WFConstants.tileWidth, posY * WFConstants.tileHeight);
				LeanTween.move(transform.GetComponent<RectTransform> (), new Vector3 (posX * WFConstants.tileWidth, posY * WFConstants.tileHeight, 0), 0.2f);
			}

		}
	}

	public void setData(Tile t){

		isJoker = t.isJoker;

		if ((ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged) && t.isStar && GameController.instance.player.starLetters.letters.IndexOf (t.code) < 0) {	
			// for (int i = 0; i < GameController.instance.player.starLetters.letters.Count; i++) {
			// 	Debug.Log(GameController.instance.player.starLetters.letters[i]);
			// }
			isStar = t.isStar;
		} else {
			isStar = false;
		}
		isMultiplier = t.isMultiplier;
		setText (t.letter);
		point = isJoker ? 0 : (GameController.instance.player.PlayerLanguage == 1 ? WFConstants.trLettersScores[t.letter] : WFConstants.enLettersScores[t.letter]); 
		setPosition (t.i, t.j);

		settings = t;
		setPoint (point);

		if (isStar && (ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged)) {
			//backGround.color = new Color (225f / 255f, 225f / 255f, 145f / 255f, 1);
			backGround.sprite = goldBackImage;
		}else if (isMultiplier) {
			backGround.sprite = multipleerBackImage;
		} else {
			//backGround.color = new Color (1, 1, 1, 1);
			backGround.sprite = letterBackImage;
		}
		//letterText.color = new Color(1, 1, 1, .5f);
	}

	public void setSelected(){
		if (isStar && (ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged)) {
			//backGround.color = new Color (240f / 255f, 112f / 255f, 72f / 255f, 1);
			backGround.sprite = goldSelectedImage;
		} else if (isMultiplier) {
			backGround.sprite = multipleerSelectedImage;
		}else {
			//backGround.color = new Color (1, 0, 0, 1);
			backGround.sprite = selectedImage;
		}
		//letterText.color = new Color(1, 1, 1, 1);
		isSelected = true;
		letterText.color = new Color(0f, 0f, 0f, 1f);
		pointText.color = new Color(0f, 0f, 0f, 1f);
	}

	public void setUnselect(){
		if (isStar && (ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged)) {
			//backGround.color = new Color (225f / 255f, 225f / 255f, 145f / 255f, 1);
			backGround.sprite = goldBackImage;
		}else if (isMultiplier) {
			backGround.sprite = multipleerBackImage;
		} else {
			//backGround.color = new Color (1, 1, 1, 1);
			backGround.sprite = letterBackImage;
		}
		//letterText.color = new Color(1, 1, 1, .5f);
		isSelected = false;
		letterText.color = new Color(0f, 0f, 0f, 204f / 255f);
		pointText.color = new Color(0f, 0f, 0f, 204f / 255f);
	}
	public void destroy(){
		Destroy (gameObject);
		//DestroyImmediate (this);
	}


}
