﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameSparks.Core;

public class DailyRewardPopupController : MonoBehaviour {

	public GameObject[] rewards;
	public GameObject[] buttons;
	public GameObject[] panels;

	public Text day7Reward;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable () {
		SetData();
	}
	public void SetData() {
		GSData dailyReward = new GSData();
		dailyReward = GameController.instance.dailyReward as GSData;
		//GSData dailyReward = GameController.instance.dailyReward as GSData;
		
		int day = (int)dailyReward.GetInt("dailyReward");
		//int day =3;
		GameAnalyticsManager.instance.TrackResourceEventSource("star", (int)dailyReward.GetInt("dailyRewardStar"), "day_"+dailyReward.GetInt("dailyReward").ToString(), "DailyReward");

		for(var i = 0; i < day; i++ ) {
			//rewards[i].GetComponent<Image>().color = new Color(208f / 255f, 11f / 255f, 186f / 255f, 1)
			panels[i].SetActive(true);
		}

		rewards[(day - 1)].GetComponent<Image>().color = new Color(253f / 255f, 76f / 255f, 236f / 255f, 1);

		// active today button
		buttons[(day - 1)].SetActive(true);
		panels[(day-1)].SetActive(false);
		// if day 7 write reward
		if(day == 7) {
			day7Reward.text = ((int)dailyReward.GetInt("dailyRewardStar")).ToString("N0");
		}
		GameController.instance.dailyReward = null;
	}

	public void GetReward(){
		EventManager.TriggerEvent (WFConstants.NotificationType.GotDailyReward, null);
		PopupController.instance.ClosePopup(this.gameObject);
	}
}
