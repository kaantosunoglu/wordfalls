﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PopupController : MonoBehaviour {

	public static PopupController instance;
	public GameObject backGround;
	//public GameObject buyPopup; 
	public GameObject PopupPanel;
	public List<GameObject> PopupsList = new List<GameObject> ();

	private List<GameObject> popups;

	void Awake () {
		popups = new List<GameObject> ();
		if (instance == null) {
			instance = this;
			return;
		}

	}

	// Use this for initialization
	void Start () {
		
	}

	// Update is called once per frame
	void Update () { }

	//Open popup function
	//get name(String) of popup
	public GameObject OpenPopup (string name) {
		backGround.SetActive (true);
		GameObject thisPopup = null;

		thisPopup = PopupsList.Where (p => p.name == name).SingleOrDefault ();

		if (thisPopup == null) {
			thisPopup = (GameObject) Instantiate (Resources.Load ("Prefabs/Popups/" + name.ToString ()));
			thisPopup.name = name;
			thisPopup.transform.SetParent (PopupPanel.transform);
			thisPopup.transform.localPosition = Vector3.zero;
			thisPopup.transform.localScale = Vector3.one;
			thisPopup.GetComponent<RectTransform> ().sizeDelta = Vector3.zero;
		}
		GameAnalyticsManager.instance.popupOpen (name);
		thisPopup.OnWindowLoad ();
		thisPopup.SetActive (true);
		popups.Add (thisPopup);
		return thisPopup;
	}

	public GameObject OpenPopup (string name, Action<bool> completion) {
		backGround.SetActive (true);
		GameObject thisPopup = null;

		thisPopup = PopupsList.Where (p => p.name == name).SingleOrDefault ();

		if (thisPopup == null) {
			thisPopup = (GameObject) Instantiate (Resources.Load ("Prefabs/Popups/" + name.ToString ()));
			thisPopup.name = name;
			thisPopup.transform.SetParent (PopupPanel.transform);
			thisPopup.transform.localPosition = Vector3.zero;
			thisPopup.transform.localScale = Vector3.one;
			thisPopup.GetComponent<RectTransform> ().sizeDelta = Vector3.zero;
		}
		GameAnalyticsManager.instance.popupOpen (name);
		thisPopup.OnWindowLoad ();
		thisPopup.SetActive (true);
		popups.Add (thisPopup);
		return thisPopup;
	}

	public GameObject OpenPopup (string headerText, string infoText, string confirmButtonText, Action<bool> confirm, string cancelButtonText, Action<bool> cancel, string laterButtonText, Action<bool> later) {
		backGround.SetActive (true);
		GameObject thisPopup = null;
		string name = "CommonPopup";

		thisPopup = PopupsList.Where (p => p.name == name).SingleOrDefault ();

		if (thisPopup == null) {
			thisPopup = (GameObject) Instantiate (Resources.Load ("Prefabs/Popups/" + name.ToString ()));
			thisPopup.name = name;
			thisPopup.transform.SetParent (PopupPanel.transform);
			thisPopup.transform.localPosition = Vector3.zero;
			thisPopup.transform.localScale = Vector3.one;
			thisPopup.GetComponent<RectTransform> ().sizeDelta = Vector3.zero;
		}
		GameAnalyticsManager.instance.popupOpen (infoText);
		thisPopup.OnWindowLoad ();
		thisPopup.SetActive (true);
		thisPopup.GetComponent<CommonPopupController> ().SetData (headerText, infoText, confirmButtonText, confirm, cancelButtonText, cancel, laterButtonText, later);

		popups.Add (thisPopup);
		return thisPopup;
	}

	public void ClosePopup (GameObject popup) {
		popup.SetActive (false);
		if (popups.Contains (popup)) {
			popups.Remove (popup);
		}
		backGround.SetActive (false);
	}

	public void ClosePopup (string popup) {

		GameObject thisPopup = null;
		thisPopup = popups.Where (p => p.name == popup).SingleOrDefault ();
		if (thisPopup != null) {
			thisPopup.SetActive (false);
			popups.Remove (thisPopup);
			backGround.SetActive (false);
		}
	}

	public void OpenShopPopup () {
		ClosePopup ("CommonPopup");
		OpenPopup ("ShopPopup");
	}
}