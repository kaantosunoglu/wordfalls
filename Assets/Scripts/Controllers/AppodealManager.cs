﻿using System;
using AppodealAds.Unity.Api;
using AppodealAds.Unity.Common;
using UnityEngine;
using UnityEngine.UI;

// Example script showing how to invoke the Appodeal Ads Unity plugin.
public class AppodealManager : MonoBehaviour, IInterstitialAdListener, IBannerAdListener, INonSkippableVideoAdListener, IRewardedVideoAdListener, IPermissionGrantedListener {

	public static AppodealManager instance;

	public bool isInterstitialReady = false;
	public bool isRewardedReady = false;

#if UNITY_EDITOR && !UNITY_ANDROID && !UNITY_IPHONE
	string appKey = "";
#elif UNITY_ANDROID
	string appKey = "b1dee9386f696abe2706fb61d3ab59a80fbe07656f3528d0";
#elif UNITY_IPHONE
	string appKey = "dcdc9920d8acdc6005cd0bf3b34c7ce1baaecfa878a51c23";
#else
	string appKey = "";
#endif

	private bool testingToggle = false;
	private bool loggingToggle = true;

	void Awake () {
		Appodeal.requestAndroidMPermissions (this);
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);

		
	}

	void Start () {
		//Init();
	}
	public void Init () {

		if (loggingToggle) {
			Appodeal.setLogLevel (Appodeal.LogLevel.Verbose);
		} else {
			Appodeal.setLogLevel (Appodeal.LogLevel.None);
		}
		Appodeal.setTesting (testingToggle);

		//Example for UserSettings usage
		// if(PlayerPrefs.GetInt("ads", -1) == 1) {
		// 	UserSettings settings = new UserSettings();
		// 	settings.setAge(GameController.instance.player.userAge).setGender(GameController.instance.player.userGender).setUserId(GameController.instance.player.playerName+SystemInfo.deviceUniqueIdentifier);
		// } else {
		// 	Appodeal.disableLocationPermissionCheck ();
		// }

		// ** DISABLE AD NETWORK

		//Appodeal.disableNetwork("appnext");
		//Appodeal.disableNetwork("amazon_ads", Appodeal.BANNER);

		
		Appodeal.disableWriteExternalStoragePermissionCheck ();

		Appodeal.setTriggerOnLoadedOnPrecache (Appodeal.INTERSTITIAL, true);

		Appodeal.setSmartBanners (true);
		//Appodeal.setBannerAnimation (false);
		//Appodeal.setTabletBanners (false);
		//Appodeal.setBannerBackground (false);

		Appodeal.setChildDirectedTreatment (false);
		Appodeal.muteVideosIfCallsMuted (true);
		Appodeal.setAutoCache (Appodeal.INTERSTITIAL, false);

		Appodeal.initialize (appKey, Appodeal.INTERSTITIAL | Appodeal.BANNER_VIEW | Appodeal.REWARDED_VIDEO);

		Appodeal.setBannerCallbacks (this);
		Appodeal.setInterstitialCallbacks (this);
		Appodeal.setRewardedVideoCallbacks (this);

		// Appodeal.setCustomRule("newBoolean", true);
		// Appodeal.setCustomRule("newInt", 1234567890);
		// Appodeal.setCustomRule("newDouble", 123.123456789);
		// Appodeal.setCustomRule("newString", "newStringFromSDK");

		showBanner ();
	}


	public void showInterstitial () {
		if (Appodeal.isLoaded (Appodeal.INTERSTITIAL) && !Appodeal.isPrecache (Appodeal.INTERSTITIAL)) {
			hideBanner ();
			Appodeal.show (Appodeal.INTERSTITIAL);
		} else {
			Appodeal.cache (Appodeal.INTERSTITIAL);
		}
	}

	public void showRewardedVideo () {
		hideBanner ();
		Debug.Log ("Reward currency: " + Appodeal.getRewardParameters ().Key + ", amount: " + Appodeal.getRewardParameters ().Value);
		if (Appodeal.canShow (Appodeal.REWARDED_VIDEO)) {
			Appodeal.show (Appodeal.REWARDED_VIDEO);
		}

	}

	public void showBanner () {
		Appodeal.show (Appodeal.BANNER_BOTTOM, "bottom_banner");
	}

	public void showBannerView () {
		Appodeal.showBannerView (Screen.currentResolution.height - Screen.currentResolution.height / 10, Appodeal.BANNER_HORIZONTAL_CENTER, "banner_view");
	}

	public void hideBanner () {
		Appodeal.hide (Appodeal.BANNER);
	}

	public void hideBannerView () {
		Appodeal.hideBannerView ();
	}

	void OnApplicationFocus (bool hasFocus) {
		if (hasFocus) {
			Appodeal.onResume ();
		}
	}

	#region Banner callback handlers

	public void onBannerLoaded (bool isPrecache) { Debug.Log ("Banner loaded, isPrecache:" + isPrecache); }
	public void onBannerFailedToLoad () { Debug.Log ("Banner failed"); }
	public void onBannerShown () { Debug.Log ("Banner opened"); }
	public void onBannerClicked () { Debug.Log ("banner clicked"); }

	#endregion

	#region Interstitial callback handlers

	public void onInterstitialLoaded (bool isPrecache) {
		isInterstitialReady = true;
	}
	public void onInterstitialFailedToLoad () { Debug.Log ("Interstitial failed to load"); }
	public void onInterstitialShown () {
		//showBanner();
	}
	public void onInterstitialClicked () { Debug.Log ("Interstitial clicked"); }
	public void onInterstitialClosed () {
		showBanner();
		isInterstitialReady = false;
		EventManager.TriggerEvent (WFConstants.NotificationType.InterstatialClosed, null);
	}

	#endregion

	#region Non Skippable Video callback handlers
	public void onNonSkippableVideoLoaded () { Debug.Log ("NonSkippable Video loaded"); }
	public void onNonSkippableVideoFailedToLoad () { Debug.Log ("NonSkippable Video failed to load"); }
	public void onNonSkippableVideoShown () { Debug.Log ("NonSkippable Video opened"); }
	public void onNonSkippableVideoClosed (bool isFinished) { Debug.Log ("NonSkippable Video, finished:" + isFinished); }
	public void onNonSkippableVideoFinished () { Debug.Log ("NonSkippable Video finished"); }
	#endregion

	#region Rewarded Video callback handlers
	public void onRewardedVideoLoaded () {
		Debug.Log ("onRewardedVideoLoaded");
		isRewardedReady = true;
	}
	public void onRewardedVideoFailedToLoad () { Debug.Log ("Rewarded Video failed to load"); }
	public void onRewardedVideoShown () { Debug.Log ("Rewarded Video opened"); }
	public void onRewardedVideoClosed (bool isFinished) {
		showBanner ();
		Debug.Log ("REKLAM IZLENDI 1");
		isRewardedReady = false;
		UnityThreadHelper.Dispatcher.Dispatch (() => {
			EventManager.TriggerEvent (WFConstants.NotificationType.RewardedVideoFinish, null);
		});
		//	EventManager.TriggerEvent (WFConstants.NotificationType.RewardedVideoFinish, null);
		//	BoardController.instance.setExtraTimeAfterAd();
	}
	public void onRewardedVideoFinished (int amount, string name) {

	}

	#endregion

	#region Permission Grant callback handlers
	public void writeExternalStorageResponse (int result) {
		if (result == 0) {
			Debug.Log ("WRITE_EXTERNAL_STORAGE permission granted");
		} else {
			Debug.Log ("WRITE_EXTERNAL_STORAGE permission grant refused");
		}
	}
	public void accessCoarseLocationResponse (int result) {
		if (result == 0) {
			Debug.Log ("ACCESS_COARSE_LOCATION permission granted");
		} else {
			Debug.Log ("ACCESS_COARSE_LOCATION permission grant refused");
		}
	}
	#endregion

}