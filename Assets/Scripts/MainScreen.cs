﻿using System.Collections;
using System.Collections.Generic;
using GameSparks.Api.Responses;
using GameSparks.Core;
using UnityEngine;
using UnityEngine.UI;
using UTNotifications;

public class MainScreen : MonoBehaviour {

	public Text counter;
	public Text counter2;
	public Text playerNameText;
	public Text starText;
	public Text pointText;
	public Text timeText;
	public GameObject playPanel;
	public GameObject waitPanel;
	public GameObject howToTextPanel;
	public GameObject clickAnim;
	public Image greenTimer;
	public Button shopButton;
	public Button LeaderBoardButton;
	public Button settingButton;

	private bool isDailyReward = false;
	private GSData dailyReward;
//	private bool isActive = false;
	private bool isChallengeOn = true;
	private string countdownTime;
	private int adscount = 0;
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (!ReachabilityManager.instance.IsReachable)
			return;

		if (isChallengeOn) {
			countdownTime = ExtentionMethods.CountdownToChallengeEnd ();
			if (countdownTime == "stop") {
				isChallengeOn = false;
				playPanel.SetActive (false);
				waitPanel.SetActive (true);
			} else {
				counter.text = countdownTime;
			}
		} else {
			countdownTime = ExtentionMethods.CountdownToChallengeStart ();
			if (countdownTime == "stop") {
				playPanel.SetActive (true);
				waitPanel.SetActive (false);
				GameController.instance.LoadAgain (gameObject);
				isChallengeOn = true;

			} else {
				counter2.text = countdownTime;
			}
		}

	}
	void OnEnable () {
		howToTextPanel.SetActive (false);
			clickAnim.SetActive (false);
		if (GameController.instance.dailyReward != null) {
		//	Debug.Log ("STEP 1");
			dailyReward = new GSData ();
			dailyReward = GameController.instance.dailyReward as GSData;
	
			if (dailyReward.GetInt ("dailyReward") != null) {
				isDailyReward = (int) dailyReward.GetInt ("dailyReward") > 0;

			}
			//isDailyReward = true;
		}
		EventManager.StartListening (WFConstants.NotificationType.BuyASecondReceived, SetPlayerDetails);
		EventManager.StartListening (WFConstants.NotificationType.IAPEnded, SetPlayerDetails);
		EventManager.StartListening (WFConstants.NotificationType.GotDailyReward, SetPlayerDetails);
		EventManager.StartListening (WFConstants.NotificationType.ReachibilityChanged, ReachibilityChanged);
		if (ExtentionMethods.CountdownToChallengeEnd () == "stop") {
			isChallengeOn = false;
			playPanel.SetActive (false);
			waitPanel.SetActive (true);
		}
		playerNameText.text = GameController.instance.player.playerName;
		SetPlayerDetails (null);
		//playerAvatar.setAvatar(GameController.instance.player.facebookID, true);
		//playerAvatar.sprite = null;
		//StartCoroutine(resett());
		AppodealManager.instance.Init ();
		if (isDailyReward) {
			PopupController.instance.OpenPopup ("DailyRewardPopup");
			isDailyReward = false;
			PNManager.instance.CancelRepeatingScheduledNotification ();

			switch ((int) dailyReward.GetInt ("dailyReward")) {
				case 1:
					starText.text = (GameController.instance.player.stars - 25).ToString ();
					break;
				case 2:
					starText.text = (GameController.instance.player.stars - 50).ToString ();
					break;
				case 3:
					starText.text = (GameController.instance.player.stars - 75).ToString ();
					break;
				case 4:
					starText.text = (GameController.instance.player.stars - 100).ToString ();
					break;
				case 5:
					starText.text = (GameController.instance.player.stars - 150).ToString ();
					break;
				case 6:
					starText.text = (GameController.instance.player.stars - 200).ToString ();
					break;
				case 7:
					starText.text = (GameController.instance.player.stars - (int) dailyReward.GetInt ("dailyRewardStar")).ToString ();
					break;
			}
		}

		if (GameController.instance.player.seconds == 90) {
			howToTextPanel.SetActive (true);
			clickAnim.SetActive (true);
		} else {
			if (adscount > 0) {
				AppodealManager.instance.showInterstitial ();
				//adscount = 0;
			} else {
				adscount++;
			}
		}

		shopButton.interactable = ReachabilityManager.instance.IsReachable;
		LeaderBoardButton.interactable = ReachabilityManager.instance.IsReachable;
		settingButton.interactable = ReachabilityManager.instance.IsReachable;

	}

	/// <summary>
	/// This function is called when the behaviour becomes disabled or inactive.
	/// </summary>
	void OnDisable () {
		EventManager.StopListening (WFConstants.NotificationType.BuyASecondReceived, SetPlayerDetails);
		EventManager.StopListening (WFConstants.NotificationType.IAPEnded, SetPlayerDetails);
		EventManager.StopListening (WFConstants.NotificationType.GotDailyReward, SetPlayerDetails);
		EventManager.StopListening (WFConstants.NotificationType.ReachibilityChanged, ReachibilityChanged);
	}

	private void SetPlayerDetails (object o = null) {
		starText.text = (isDailyReward) ? ((int) dailyReward.GetInt ("oldStarCount")).ToString ("N0") : GameController.instance.player.stars.ToString ("N0");
		//starText.text = (isDailyReward) ? "5" : GameController.instance.player.stars.ToString("N0");

		pointText.text = GameController.instance.player.oldScore.ToString ("N0");
		timeText.text = GameController.instance.player.seconds.ToString ("N0");
	//	Debug.Log (ExtentionMethods.TimerAmount (GameController.instance.player.seconds));
		greenTimer.fillAmount = ExtentionMethods.TimerAmount (GameController.instance.player.seconds);
	}

	public void GetUsedStarLetters () {
		GameSparkManager.instance.GetStarLetter (
			delegate (bool response) {
				GameController.instance.StartGamePlay (gameObject);
			},
			delegate (string obj) {

			});
	}

	public void Play () {
		//GetUsedStarLetters ();
		GameController.instance.StartGamePlay (gameObject);
	}

	public void OpenSettings () {
		GameController.instance.OpenSettings (gameObject);

	}

	public void OpenLeaderboard () {
		PopupController.instance.OpenPopup ("LeaderBoardScreen");
	}

	public void OpenShop () {
		PopupController.instance.OpenPopup ("ShopPopup");
	}

	public void ConfirmQuitGame () {
		PopupController.instance.OpenPopup ("confirmQuitHeader", "confirmQuit", "quit",
			delegate (bool b) {
				GameController.instance.QuitGame ();
			},
			"cancel",
			delegate (bool b) {
				PopupController.instance.ClosePopup ("CommonPopup");
			}, "", null);
	}

	private void ReachibilityChanged (object o) {
		//Debug.Log("ReachibilityChanged " + ReachabilityManager.instance.IsReachable);
		//if (!ReachabilityManager.instance.IsReachable) {
			shopButton.interactable = LeaderBoardButton.interactable = settingButton.interactable = ReachabilityManager.instance.IsReachable;
		//}
	}

}