﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using tQsLib;

public class FirstThreeController : MonoBehaviour {

	public Text playerRank;
	public Text playerName;
	public Text playerScore;
	public Text playerTime;

	public void setData(LeaderBoardPlayer player){
		playerName.text = player.playerName;
		playerRank.text = player.playerRank.ToString("N0");
		playerScore.text = player.playerScore.ToString("N0");
		playerTime.text = player.playerTime.ToString("N0");
	}
}
