﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class CommonPopupController : MonoBehaviour {

	public Text infoText;
	public Text headerText;
	public Text cancelText;
	public Text confirmText;
	public Text laterText;
	public GameObject confirmButton;
	public GameObject laterButton;
	public GameObject cancelButton;

	private Action<bool> confirmAction;
	private Action<bool> laterAction;
	private Action<bool> cancelAction;


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetData(string header, string info, string confirmButtonText = "", Action<bool> confirm = null, string cancelButtonText = "", Action<bool> cancel = null, string laterButtonText = "", Action<bool> later = null) {
		
		headerText.text = header == "" ? "" : LanguageManager.getText(header);
		infoText.text = info == "" ? "" : LanguageManager.getText(info);
		
		confirmButton.SetActive(false);
		laterButton.SetActive(false);
		cancelButton.SetActive(false);
		laterAction = later;

		if(cancel != null) {
			cancelButton.SetActive(true);
			cancelText.text = LanguageManager.getText(cancelButtonText);
			cancelAction = cancel;
		}

		if(confirm != null) {
			confirmButton.SetActive(true);
			confirmAction = confirm;
			confirmText.text = LanguageManager.getText(confirmButtonText);
		}

		if(later != null) {
			laterButton.SetActive(true);
			laterText.text = LanguageManager.getText(laterButtonText);
			laterAction = later;
		}
	}

	public void confirmButtonAction() {
		confirmAction(true);
	}

	public void cancelButtonAction() {
		cancelAction(true);
	}

	public void laterButtonAction() {
		laterAction(true);
	}

}
