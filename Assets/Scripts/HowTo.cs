﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowTo : MonoBehaviour {


	public GameObject step1;
	public GameObject step2;
	public GameObject step3;
	public GameObject step4;
	public GameObject step5;
	public GameObject swipe;

	public GameObject nextButton;
	public GameObject click;

	private int step;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}

	/// <summary>
	/// This function is called when the object becomes enabled and active.
	/// </summary>
	void OnEnable () {
		step = 0;
	
		changeStep ();
	}

	public void changeStep () {
		step++;
		Debug.Log (step);
			swipe.SetActive(false);
			click.SetActive(false);
		if (step == 1) {
			step1.SetActive (true);
			click.SetActive(true);
		} else if (step == 2) {
			nextButton.SetActive (false);
			step1.SetActive (false);
			step2.SetActive (true);
				swipe.SetActive(true);
				
		//	HowToBoardController.instance.changeStep ();
		} else if (step == 3) {
			nextButton.SetActive (false);
			step2.SetActive (false);
			step3.SetActive (true);
		//	HowToBoardController.instance.changeStep ();
		} else if (step == 4) {
			nextButton.SetActive (false);
			step3.SetActive (false);
			step4.SetActive (true);
		//	HowToBoardController.instance.changeStep ();
		} else if (step == 5) {
			nextButton.SetActive (false);
			step4.SetActive (false);
			step5.SetActive (true);
		//	HowToBoardController.instance.changeStep ();
		} else if (step == 6) {
			nextButton.SetActive (false);
			step4.SetActive (false);
			step5.SetActive (false);
			PlayerPrefs.SetInt ("howto", 1);
			GameController.instance.GotoMainScreen(gameObject);
		}
	}

	public void skipHowTo() {		
		GameController.instance.GotoMainScreen(gameObject);
	}
}