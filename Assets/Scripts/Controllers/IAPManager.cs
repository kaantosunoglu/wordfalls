﻿#if UNITY_PURCHASING

#if UNITY_ANDROID || UNITY_IPHONE || UNITY_STANDALONE_OSX || UNITY_TVOS
// You must obfuscate your secrets using Window > Unity IAP > Receipt Validation Obfuscator
// before receipt validation will compile in this sample.
//#define RECEIPT_VALIDATION
#endif

//#define DELAY_CONFIRMATION // Returns PurchaseProcessingResult.Pending from ProcessPurchase, then calls ConfirmPendingPurchase after a delay
//#define USE_PAYOUTS // Enables use of PayoutDefinitions to specify what the player should receive when a product is purchased

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Store;
using UnityEngine.UI; // UnityChannel
using UnityEngine.Purchasing.Security;



/// <summary>
/// An example of Unity IAP functionality.
/// To use with your account, configure the product ids (AddProduct).
/// </summary>

public class IAPManager : MonoBehaviour, IStoreListener {
	public static IAPManager instance;
	public IStoreController m_StoreController; // The Unity Purchasing system.
	private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

	// Product identifiers for all products capable of being purchased: 
	// "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
	// counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
	// also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

	// General product identifiers for the consumable, non-consumable, and subscription products.
	// Use these handles in the code to reference which product to purchase. Also use these values 
	// when defining the Product Identifiers on the store. Except, for illustration purposes, the 
	// kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
	// specific mapping to Unity Purchasing's AddProduct, below.
	// public static string kProductIDConsumable =    "consumable";   
	// public static string kProductIDNonConsumable = "nonconsumable";
	// public static string kProductIDSubscription =  "subscription"; 

	private UnityEngine.Purchasing.Product latestPurchasedProduct;

    private CrossPlatformValidator validator;

	void Start () {
		EventManager.StartListening (WFConstants.NotificationType.IAPReceived, IAPReceived);
		// If we haven't set up the Unity Purchasing reference
		if (m_StoreController == null) {
			// Begin to configure our connection to Purchasing
			InitializePurchasing ();
		}
	}

	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	public void InitializePurchasing () {
		// If we have already connected to Purchasing ...
		if (IsInitialized ()) {
			// ... we are done here.
			return;
		}
		// Create a builder, first passing in a suite of Unity provided stores.
		var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

		// Add a product to sell / restore by way of its identifier, associating the general identifier
		// with its store-specific identifiers.
		builder.AddProduct ("STAR_PACK_150", ProductType.Consumable, new IDs () { { "com.thequixoticssolutions.star150", AppleAppStore.Name }, { "com.thequixoticssolutions.star150", GooglePlay.Name },
		});
		builder.AddProduct ("STAR_PACK_450", ProductType.Consumable, new IDs () { { "com.thequixoticssolutions.star450", AppleAppStore.Name }, { "com.thequixoticssolutions.star450", GooglePlay.Name },
		});
		builder.AddProduct ("STAR_PACK_1500", ProductType.Consumable, new IDs () { { "com.thequixoticssolutions.star1500", AppleAppStore.Name }, { "com.thequixoticssolutions.star1500", GooglePlay.Name },
		});
		builder.AddProduct ("STAR_PACK_3000", ProductType.Consumable, new IDs () { { "com.thequixoticssolutions.star3000", AppleAppStore.Name }, { "com.thequixoticssolutions.star3000", GooglePlay.Name },
		});
		builder.AddProduct ("STAR_PACK_5000", ProductType.Consumable, new IDs () { { "com.thequixoticssolutions.star5000", AppleAppStore.Name }, { "com.thequixoticssolutions.star5000", GooglePlay.Name },
		});
		builder.AddProduct ("STAR_PACK_15000", ProductType.Consumable, new IDs () { { "com.thequixoticssolutions.star15000", AppleAppStore.Name }, { "com.thequixoticssolutions.star15000", GooglePlay.Name },
		});

		// Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
		// and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
		UnityPurchasing.Initialize (this, builder);
	}

	private bool IsInitialized () {
		// Only say we are initialized if both the Purchasing references are set.
		return m_StoreController != null && m_StoreExtensionProvider != null;
	}

	public void BuyProductID (string productId) {
		// If Purchasing has been initialized ...
		if (IsInitialized ()) {

			if (latestPurchasedProduct == null) {

				// ... look up the Product reference with the general product identifier and the Purchasing 
				// system's products collection.
				Product product = m_StoreController.products.WithID (productId);
				latestPurchasedProduct = product;
				PlayerPrefs.SetString ("lid", product.definition.id);
				// If the look up found a product for this device's store and that product is ready to be sold ... 
				if (product != null && product.availableToPurchase) {
					// ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
					// asynchronously.
					m_StoreController.InitiatePurchase (product);
				}
				// Otherwise ...
				else {
					// ... report the product look-up failure situation  
					//Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
				}
			} else {
				//Debug.Log ("BuyProductID FAIL. There is another product");
			}
		}
		// Otherwise ...
		else {
			// ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
			// retrying initiailization.
			Debug.Log ("BuyProductID FAIL. Not initialized.");
		}
	}

	// Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
	// Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
	public void RestorePurchases () {
		
		// If Purchasing has not yet been set up ...
		if (!IsInitialized ()) {
			// ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
			Debug.Log ("RestorePurchases FAIL. Not initialized.");
			return;
		}

		// If we are running on an Apple device ... 
		if (Application.platform == RuntimePlatform.IPhonePlayer ||
			Application.platform == RuntimePlatform.OSXPlayer) {
			// ... begin restoring purchases
			

			// Fetch the Apple store-specific subsystem.
			var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions> ();
			// Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
			// the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
			apple.RestoreTransactions ((result) => {
				// The first phase of restoration. If no more responses are received on ProcessPurchase then 
				// no purchases are available to be restored.
				Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
			});
		}
		// Otherwise ...
		else {
			// We are not running on an Apple device. No work is necessary to restore purchases.
			Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
		}
	}

	//  
	// --- IStoreListener
	//

	public void OnInitialized (IStoreController controller, IExtensionProvider extensions) {
		// Purchasing has succeeded initializing. Collect our Purchasing references.
		Debug.Log ("OnInitialized: PASS");

		// Overall Purchasing system, configured with products for this application.
		m_StoreController = controller;
		// Store specific subsystem, for accessing device-specific store features.
		m_StoreExtensionProvider = extensions;
	}

	public void OnInitializeFailed (InitializationFailureReason error) {
		// Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
		Debug.Log ("OnInitializeFailed InitializationFailureReason:" + error);
	}

	// *********************************************	


	public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args) {
		Debug.Log ("PurchaseProcessingResult  1" + args.purchasedProduct.definition.id);
		if (PlayerPrefs.GetString ("lid") == "" && latestPurchasedProduct == null) {
			latestPurchasedProduct = args.purchasedProduct;
		}
		//return PurchaseProcessingResult.Complete;
		// A consumable product has been purchased by this user.
		if (String.Equals (args.purchasedProduct.definition.id, PlayerPrefs.GetString ("lid"), StringComparison.Ordinal) || String.Equals (args.purchasedProduct.definition.id, latestPurchasedProduct.definition.id, StringComparison.Ordinal)) {
			
			var product = m_StoreController.products.WithID (PlayerPrefs.GetString ("lid"));
			string receipt = product.receipt;
			string currency = product.metadata.isoCurrencyCode;
			int amount = decimal.ToInt32 (product.metadata.localizedPrice * 100);

			Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.receipt));

#if UNITY_ANDROID
			Receipt receiptAndroid = JsonUtility.FromJson<Receipt> (receipt);
			PayloadAndroid receiptPayload = JsonUtility.FromJson<PayloadAndroid> (receiptAndroid.Payload);
			GooglePurchaseData data = new GooglePurchaseData (args.purchasedProduct.receipt);

			// GAMESPARK !!!!
			GameSparkManager.instance.BuyStars(data.inAppDataSignature, data.inAppPurchaseData);
			//GameAnalytics.NewBusinessEventGooglePlay(currency, amount, "my_item_type", kProductIDConsumable, "my_cart_type", receiptPayload.json, receiptPayload.signature);
#endif
#if UNITY_IPHONE

			Product p = args.purchasedProduct;
			
			Receipt receiptiOS = JsonUtility.FromJson<Receipt> (receipt);
			string receiptPayload = receiptiOS.Payload;
			GameSparkManager.instance.BuyStars(p.metadata.isoCurrencyCode, receiptPayload);
			//GameAnalytics.NewBusinessEventIOS (currency, amount, "my_item_type", kProductIDConsumable, "my_cart_type", receiptPayload);
#endif

			
		}

		// Or ... an unknown product has been purchased by this user. Fill in additional products here....
		else {
			Debug.Log (string.Format ("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
		}

		// Return a flag indicating whether this product has completely been received, or if the application needs 
		// to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
		// saving purchased products to the cloud, and when that save is delayed. 
		return PurchaseProcessingResult.Pending;
	}


	// *********************************************	
	public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason) {
		// A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
		// this reason with the user to guide their troubleshooting actions.
		//PurchaseProcessingResult.Complete;
		PopupController.instance.OpenPopup("purchaseErrorHeader", "purchaseError", "OK",
			delegate(bool b) {
				PopupController.instance.ClosePopup ("CommonPopup");
			}, 
			"", null, "", null
		);
		Debug.Log (string.Format ("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
	}

	// -- Listening Gamespark IAP
	private void IAPReceived (object o) {
		m_StoreController.ConfirmPendingPurchase (latestPurchasedProduct);
		latestPurchasedProduct = null;
		EventManager.TriggerEvent (WFConstants.NotificationType.IAPSuccess, null);
	}

}
#endif


	public class Receipt {

		public string Store;
		public string TransactionID;
		public string Payload;

		public Receipt () {
			Store = TransactionID = Payload = "";
		}

		public Receipt (string store, string transactionID, string payload) {
			Store = store;
			TransactionID = transactionID;
			Payload = payload;
		}
	}

	public class PayloadAndroid {
		public string json;
		public string signature;

		public PayloadAndroid () {
			json = signature = "";
		}

		public PayloadAndroid (string _json, string _signature) {
			json = _json;
			signature = _signature;
		}
	}

