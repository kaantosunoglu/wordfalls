﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
using GameAnalyticsSDK.Events;

public class GameAnalyticsManager : MonoBehaviour {

	public static GameAnalyticsManager instance;
	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {
		GameAnalytics.Initialize();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void popupOpen(string popup) {
		GameAnalytics.NewDesignEvent("PopupView:" + popup);
	}

	public void screenOpen(string screen) {
		GameAnalytics.NewDesignEvent("ScreenView:" + screen);
	}

	public void TrackResourceEventSource (string type, int amount, string itemType, string itemId) {
		if (amount > 0) {
			GameAnalytics.NewResourceEvent (GAResourceFlowType.Source, type, amount, itemType, itemId);
		}
	}

	public void TrackResourceEventSink (string type, int amount, string itemType, string itemId) {
		if (amount > 0) {
			GameAnalytics.NewResourceEvent (GAResourceFlowType.Sink, type, amount, itemType, itemId);
		}
	}

	public void trackFunnel(string funnel) {
		GameAnalytics.NewDesignEvent(funnel);
	}

	public void startGame() {
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "new");
	}

	public void restartGame(string by) {
		GameAnalytics.NewProgressionEvent(GAProgressionStatus.Start, "restart", by);
	}
}
