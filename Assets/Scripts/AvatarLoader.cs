﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using tQsLib;

public class AvatarLoader : MonoBehaviour
{

    public static AvatarLoader instance;

    private Dictionary<string, Sprite> avatarList;
    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            return;
        }
        Destroy(gameObject);
    }

    // Use this for initialization
    void Start()
    {
        avatarList = new Dictionary<string, Sprite>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    public void setAvatar(string userId, Image userAvatarContainer)
    {
        Debug.Log("STEP 1");
        // if user not logins with Facebook then userId must be null
        // and use default userAvatar Image 
        if (userId == null)
        {
            return;
        }

        // if user logins with Facebook then look for avatar pooled

        // if not get user avatar
        // esle use which in pool
        Sprite sprite = null;
        if (avatarList.ContainsKey(userId))
        {

            sprite = avatarList[userId];
            if (sprite == null)
            {
                return;
            }
            userAvatarContainer.sprite = sprite;
        }
        else
        {
            //sprite = FacebookManager.instance.GetUserAvatar(userId);
            StartCoroutine(FacebookManager.instance.GetUserAvatar(userId, (result) =>
            {
                Debug.Log(result);
                sprite = result;
            if (sprite == null)
            {
                Debug.Log("STEP 1");
                avatarList.Add(userId, null);
                return;
            }
            else
            {
                Debug.Log("STEP 2");
                avatarList.Add(userId, sprite);
            }
                Debug.Log(sprite.ToString());
        userAvatarContainer.sprite = sprite;
        Debug.Log(userAvatarContainer.sprite);
            }));

           
        }
        //Rect rect = new Rect(0, 0, sprite.width, sprite.height);
        //userAvatarContainer.sprite = Sprite.Create(avatarList[userId], rect, new Vector2(0, 0), 1);
    
    }

}
