﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextController : MonoBehaviour {
	public string text;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnEnable(){
		SetText ();
	}

	public void SetText(){
		
		gameObject.GetComponent<Text>().text =  LanguageManager.getText (text);
	}


}
