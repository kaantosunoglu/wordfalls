﻿using UnityEngine;
using System.Collections;
using System;
using GameSparks.Api.Responses;

public static class ExtentionMethods 
{
	public static bool OnWindowLoad(this GameObject target)
	{
		WindowTransition transition = target.GetComponent<WindowTransition> ();
		if(transition != null)
		{
			transition.OnWindowAdded();
			return true;
		}
		return false;
	}

	public static bool OnWindowRemove(this GameObject target)
	{
		WindowTransition transition = target.GetComponent<WindowTransition> ();
		if(transition != null)
		{
			transition.OnWindowRemove();
			return true;
		}
		return false;
	}
		
	public static string CountdownToChallengeEnd() {

		var epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		long now = (long)(System.DateTime.UtcNow - epochStart).TotalMilliseconds;
		long serverTime = now + GameController.instance.player.timeStamp;

		//var countdownTimeSpan = TimeSpan.FromSeconds(serverTime/1000);
			DateTime countdownTimeSpan = UnixTimeStampToDateTime (serverTime);

		if ((countdownTimeSpan.Hour == 0 && countdownTimeSpan.Minute < 4) || (countdownTimeSpan.Hour == 23 && countdownTimeSpan.Minute > 57)) {
			return "stop";
		} 

		int h = 23 - countdownTimeSpan.Hour;
		int m = (59 - countdownTimeSpan.Minute);
		int s = 60 - countdownTimeSpan.Second;
			
		return (h < 10 ? "0"+h.ToString() : h.ToString()) + ":" + (m < 10 ? "0"+m.ToString() : m.ToString()) + ":" + (s<10?"0"+s.ToString():s.ToString());
	}

	public static string CountdownToChallengeStart(){
		var epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		long now = (long)(System.DateTime.UtcNow - epochStart).TotalMilliseconds;
		long serverTime = now + GameController.instance.player.timeStamp;


		DateTime countdownTimeSpan = UnixTimeStampToDateTime (serverTime);

		if ((countdownTimeSpan.Hour == 0 && countdownTimeSpan.Minute > 3) || (countdownTimeSpan.Hour == 23 && countdownTimeSpan.Minute < 58)) {
			return "stop";
		} 
		int m;
		int s;
		if (countdownTimeSpan.Hour == 0 && countdownTimeSpan.Minute == 0) {

			//int h = 23 - countdownTimeSpan.Hour;
			m = 0;
			s = 60 - countdownTimeSpan.Second;
		} else {
			//int h = 23 - countdownTimeSpan.Hour;
			m = (3 - countdownTimeSpan.Minute) + 1;
			s = 60 - countdownTimeSpan.Second;
		}
		return (m<10?"0"+m.ToString():m.ToString())+":"+(s<10?"0"+s.ToString():s.ToString());
	}
	public static void setTimeStamp(this LogEventResponse timeData){

		var epochStart = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);
		long now = (long)(System.DateTime.UtcNow - epochStart).TotalMilliseconds;
		long server = (long)timeData.ScriptData.GetLong ("MS");
		long timestamp = server - now;

		GameController.instance.player.timeStamp = timestamp;
	}

	public static DateTime UnixTimeStampToDateTime(long unixTimeStamp) 
	{
		System.DateTime dtDateTime = new System.DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
		dtDateTime = dtDateTime.AddMilliseconds(unixTimeStamp);
		return dtDateTime;
	}

	public static string checkDate(LogEventResponse timeData){
		string t;

		t = timeData.ScriptData.GetInt ("Year").ToString();
		t += ((timeData.ScriptData.GetInt ("Month")+1) < 10 ? "0" : "") +  (timeData.ScriptData.GetInt ("Month")+1).ToString();
		t += (timeData.ScriptData.GetInt ("Day") < 10 ? "0" : "") + timeData.ScriptData.GetInt ("Day").ToString();

		return t;
	}

	public static string checkDateForMiliseconds(LogEventResponse timeData){
		DateTime today = new DateTime((int)timeData.ScriptData.GetInt ("Year"), (int)timeData.ScriptData.GetInt ("Month")+1, (int)timeData.ScriptData.GetInt ("Day"));
		int time = (int)(today.Subtract(new DateTime(1970, 1, 1))).TotalSeconds;

		return time.ToString ();
	}

	public static string EscapeURL(this string text) {
        return WWW.EscapeURL(text).Replace("+", "%20");
    }

	public static float TimerAmount(int second) {
		return 0.0926f + (((0.911f - 0.0926f) / 120f) * second);
	}
}
