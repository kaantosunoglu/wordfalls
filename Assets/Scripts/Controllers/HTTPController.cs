﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class HTTPController : MonoBehaviour {

	public static HTTPController instance;
	 
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Awake(){
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}



	public void CheckGameVersion(Action<string> completion, Action<String> trouble){
		if (ReachabilityManager.instance.IsReachable) {
			string url = "http://www.thequixotics.co.uk/wordfalls/gamev";
			StartCoroutine (StartRequest (url, completion, trouble));
		} else {
			trouble ("connection error");
			GameController.instance.ShowInternetConnectionAlert ();
		}
	}

	public void CheckWordsVersion(Action<string> completion, Action<String> trouble){
		if (ReachabilityManager.instance.IsReachable) {
			
			string url = "http://www.thequixotics.co.uk/wordfalls/wordsv" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en");
			StartCoroutine (StartRequest (url, completion, trouble));
		} else {
			trouble ("connection error");
			GameController.instance.ShowInternetConnectionAlert ();
		}
	}

	public void GetWordsList(Action<string> completion, Action<String> trouble){
		if (ReachabilityManager.instance.IsReachable) {
			string url = "http://www.thequixotics.co.uk/wordfalls/words" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en");
			StartCoroutine (StartRequest (url, completion, trouble));
		} else {
			trouble ("connection error");
			GameController.instance.ShowInternetConnectionAlert ();
		}
	}

	public void GetBoardsList(string date, Action<string> completion, Action<String> trouble){
		if (ReachabilityManager.instance.IsReachable) {
			string url = "http://www.thequixotics.co.uk/wordfalls/dates/" + date;
			StartCoroutine (StartRequest (url, completion, trouble));
		} else {
			trouble ("connection error");
			GameController.instance.ShowInternetConnectionAlert ();
		}
	}

	public void GetBoard(string boardName, Action<string> completion, Action<string> trouble){
		if (ReachabilityManager.instance.IsReachable) {
			string url = "http://www.thequixotics.co.uk/wordfalls/boards" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + "/" + boardName;
			StartCoroutine (StartRequest (url, completion, trouble));
		} else {
			trouble ("connection error");
			GameController.instance.ShowInternetConnectionAlert ();
		}
	}


	public IEnumerator StartRequest(string requesURL, Action<string> successCallBack, Action<string> failureCallBack)	{
		//LogManager.Msg ("StartRequest");
		WWW www = new WWW(requesURL);
		yield return www;

		if (www.error == null) {
			successCallBack (www.text);
		} else {
			failureCallBack (www.text);
		}    
	}
}
