﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingsController : MonoBehaviour {


	public Dropdown languageList;
	public Toggle ads;
	

	private int oldLanguage;
	private int newLanguage;

	// Use this for initialization
	void Start () {
		languageList.value = oldLanguage = GameController.instance.player.PlayerLanguage -1;
		EventManager.StartListening (WFConstants.NotificationType.NewLangReceived, LoadAgain);
		//ads.isOn = PlayerPrefs.GetInt("ads", -1) == 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SaveChanges(){
		newLanguage = languageList.value;
		//PlayerPrefs.SetInt("ads", ads.isOn ? 1 : 0);
		if (oldLanguage != newLanguage) {
			oldLanguage = newLanguage;
			GameSparkManager.instance.SetLanguage (languageList.value + 1);
		}else{
			GameController.instance.OnBackButtonPressed();
		}
	}

	private void LoadAgain(object o){
		PlayerPrefs.SetInt ("language", newLanguage + 1);
		GameController.instance.player.PlayerLanguage = newLanguage + 1;
		GameController.instance.LoadAgain (gameObject);
	}

	void OnDestroy(){
		EventManager.StopListening (WFConstants.NotificationType.NewLangReceived, LoadAgain);
	}

	public void MailToUs() {
		string email = WFConstants.mailAdress;
		string subject = LanguageManager.getText ("mailSubject").EscapeURL();
		string body = "\n\n";
		body += LanguageManager.getText ("writeYourProblem");
		body += "\n\n\n\n";
		body += "\n\n___________________________________\n\n";
		body += LanguageManager.getText ("dontDelete");
		body += "\n\n";
		body += "User Id:\t\t\t" + GameController.instance.player.playerID + "\n";
		body += "Game Version:\t\t\t" + WFConstants.GAME_VERSION + "\n";
		body += "Version:\t\t\t" + Application.version + "\n";
		body += "Platform:\t\t\t" + Application.platform + "\n";
		body += "Device Model:\t\t" + SystemInfo.deviceModel + "\n";
		body += "OS:\t\t\t\t" + SystemInfo.operatingSystem + "\n";
		body += "Memory Size:\t\t" + SystemInfo.systemMemorySize.ToString("N0") + " MB\n";
		body += "\n___________________________________\n";
		body = body.Trim ().EscapeURL ();
		Application.OpenURL("mailto:" + email + "?subject=" + subject + "&body=" + body);
	}
}
