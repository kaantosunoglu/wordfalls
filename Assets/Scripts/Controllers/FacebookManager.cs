﻿using UnityEngine;
using System.Collections;
using System;
using Facebook;
using Facebook.Unity;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;
using System.IO;

namespace tQsLib
{

    public class FacebookManager : MonoBehaviour
    {

        public static FacebookManager instance;

        private Action<object> success;
        private Action<object> fail;
        public void Destroy()
        {

        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                return;
            }
            Destroy(gameObject);
        }

        public void LoginFacebookAndGetProfile(Action<object> success, Action<object> fail)
        {
            this.success = success;
            this.fail = fail;

            if (FB.IsInitialized)
            {
                FBLogin();
            }
            else
            {
                FB.Init(FBLogin, OnHideUnity);
            }
        }

        public void GetMyFacebookInfo()
        {
                FB.API("/me?fields=id,name,first_name,last_name", HttpMethod.GET, this.HandleMeResult);
        }

        public void LogoutFacebook()
        {
            if (FB.IsInitialized && FB.IsLoggedIn)
            {
                FB.LogOut();
            }
        }

        private void FBLogin()
        {
            // if (FB.IsLoggedIn)
            // {
            //     GetMyFacebookInfo();
            // }
            // else
            // {
                // if(PlayerPrefs.GetInt("ads", -1) == 1){
                //     FB.LogInWithReadPermissions(new List<string>() { "public_profile", "user_friends", "user_birthday", "user_gender" }, HandleLoginResult);
                // }else {
                    FB.LogInWithReadPermissions(new List<string>() { "public_profile", "user_friends" }, HandleLoginResult);
                // }
            // }
        }

        private void HandleLoginResult(IResult result)
        {
            if (result == null)
            {
                fail("Login Failed");
                return;
            }

            if (result.Error != null)
            {
                fail(result.Error.ToString());
                return;
            }

            if (result.Cancelled)
            {
                fail("cancelled");
                return;
            }
            GetMyFacebookInfo();
        }

        private void HandleMeResult(IResult result)
        {
            if (result == null || result.RawResult == null || result.ToString() == "" || result.RawResult.ToString() == "")
            {
                this.fail("Me fail");
                return;
            }

            if (result.Error != null)
            {
                this.fail("Facebook Profile Error : " + result.Error.ToString());
                return;
            }

            this.success(result);
        }

        private void OnHideUnity(bool isGameShown)
        {

        }

        public IEnumerator GetUserAvatar(string userId, System.Action<Sprite> callback)
        {
            if (userId == null)
            {
               yield return null;
            }
            Sprite avatar = null;
            FB.API(userId + "/picture?width=64&height=64&redirect=false", HttpMethod.GET, (IGraphResult graphResult) =>
            {
                if (graphResult.Cancelled)
                {
                    avatar = null;

                }
                else if (!string.IsNullOrEmpty(graphResult.Error))
                {
                   avatar = null;
                }
                else
                {

                    JToken jt = JToken.Parse(graphResult.RawResult.ToString());
                    StartCoroutine(loadAvatar(jt["data"]["url"].ToString(), (result) => {
                        
                        if(result == null)
                        {
                            avatar = null;
                        }
                        else
                        {
                            avatar = result;
                        }
                        callback(avatar);
                    }
                    ));
                }
            });
           // yield return null;
            
            //return avatar;
        }

        IEnumerator loadAvatar(string url, System.Action<Sprite> callback)
        {
            WWW www = new WWW(url);
            yield return www;
            if (String.IsNullOrEmpty(www.error))
            {
            
                Sprite texture = Sprite.Create(www.texture, new Rect(0, 0, www.texture.width, www.texture.height), new Vector2(0, 0));
               // Texture2D texture = new Texture2D(www.texture.width, www.texture.height, TextureFormat.DXT1, false);
                //www.LoadImageIntoTexture(texture);
                callback(texture);
            }
            else
            {
                callback(null);
            }
        }

        /*	public void PostToWall(string fileName, byte[] fileBytes, OKConstants.FBWallPostType postType, Dictionary<string, string> extraData) {
                showOverlay();

                HTTPAPI.Instance.UploadImage(fileName, fileBytes, ProfileViewModel.Instance.MyProfile.Id, ProfileViewModel.Instance.sessionKey, (obj) => {

                    hideOverylay();

                    #if UNITY_WEBGL && !UNITY_EDITOR
                    var desc = String.Empty;
                    var name = String.Empty;

                    switch (postType) {
                    case OKConstants.FBWallPostType.GameWon:
                    desc = String.Format(gameWonTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["gold"]);
                    name = String.Format("{0} Oyunu Kazandı", ProfileViewModel.Instance.MyProfile.Name); 
                    break;
                    case OKConstants.FBWallPostType.PotWon:
                    desc = String.Format(gameWonTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["gold"]);
                    name = String.Format("{0} Okey Atarak Bitti", ProfileViewModel.Instance.MyProfile.Name);
                    break;
                    case OKConstants.FBWallPostType.LeaderboardReward:
                    desc = String.Format(leaderboardRewardTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["rank"], extraData["gem"]);
                    name = String.Format("{0} İlk 3'e Girdi", ProfileViewModel.Instance.MyProfile.Name);
                    break;
                    case OKConstants.FBWallPostType.LevelUp:
                    desc = String.Format(levelupTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["level"]);
                    name = String.Format("{0} Seviye Yükseldi", ProfileViewModel.Instance.MyProfile.Name);
                    break;
                    }

                    Application.ExternalCall("FBShare", name, desc, "https://fb.me/272535769818433", "https://msmok.s3-us-west-2.amazonaws.com/tmpimg/" + fileName + ".jpg");
                    #else

                    if (FB.IsInitialized && FB.IsLoggedIn) {
                        WSAPI.Instance.IsFacebookPause = true;
                        var desc = String.Empty;
                        var name = String.Empty;
                        switch (postType) {
                        case OKConstants.FBWallPostType.GameWon:
                            desc = String.Format(gameWonTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["gold"]);
                            name = String.Format("{0} Oyunu Kazandı", ProfileViewModel.Instance.MyProfile.Name);
                            break;
                        case OKConstants.FBWallPostType.PotWon:
                            desc = String.Format(gameWonTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["gold"]);
                            name = String.Format("{0} Okey Atarak Bitti", ProfileViewModel.Instance.MyProfile.Name);
                            break;
                        case OKConstants.FBWallPostType.LeaderboardReward:
                            desc = String.Format(leaderboardRewardTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["rank"], extraData["gem"]);
                            name = String.Format("{0} İlk 3'e Girdi", ProfileViewModel.Instance.MyProfile.Name);
                            break;
                        case OKConstants.FBWallPostType.LevelUp:
                            desc = String.Format(levelupTemplate, ProfileViewModel.Instance.MyProfile.Name, extraData["level"]);
                            name = String.Format("{0} Seviye Yükseldi", ProfileViewModel.Instance.MyProfile.Name);
                            break;

                        }
                        FB.FeedShare(linkName: name, linkDescription: desc, link: new Uri("https://fb.me/272535769818433"), picture: new Uri("https://msmok.s3-us-west-2.amazonaws.com/tmpimg/" + fileName + ".jpg"), callback: HandleShareResult);
                        Debug.LogWarning("https://msmok.s3-us-west-2.amazonaws.com/tmpimg/" + fileName + ".jpg");
                    }
                    #endif
                }, (str) => Debug.Log(str));
            }*/

        private void HandleShareResult(IShareResult result)
        {
            if (result == null || result.RawResult == null || result.ToString() == "" || result.RawResult.ToString() == "")
            {

                return;
            }

            if (result.Error != null)
            {

                return;
            }
            else
            {

            }
        }

        /*	public IEnumerator ShareScreen(OKConstants.FBWallPostType postType, Dictionary<string, string> extraData) {
                var fileName = String.Format("{0}-{1}-{2}", ProfileViewModel.Instance.MyProfile.Id, DateTime.Now.ToString("dd-MM-yyyy-HH:mm"), postType.ToString());
                var now = Time.time;
                yield return new WaitForEndOfFrame();
                Texture2D screenTexture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
                screenTexture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0);
                screenTexture.Apply();
                #if UNITY_WEBGL && !UNITY_EDITOR
                #else
                var divVal = (Convert.ToDouble(2048) / Convert.ToDouble(1536));
                var width = 960f;
                var height = width / divVal;


                float scaleHeight = (float)540 / (float)Screen.height;
                float scaleWidth = (float)960 / (float)Screen.width;

                float scale = Math.Min(scaleHeight, scaleWidth);

                TextureScale.Bilinear(screenTexture, (int)(Screen.width * scale), (int)(Screen.height * scale));
                #endif
                byte[] dataToSave = screenTexture.EncodeToJPG();

                PostToWall(fileName, dataToSave, postType, extraData);
            }

            public void showOverlay() {
                var sceneActivityIndicator = (GameObject)Instantiate(Resources.Load("SceneActivityIndicator"));
                var canvas = FindObjectOfType(typeof(Canvas)) as Canvas;
                sceneActivityIndicator.gameObject.transform.SetParent(canvas.transform, false);
            }

            public void hideOverylay() {
                var sceneActivityIndicator = FindObjectOfType(typeof(SceneActivityIndicator)) as SceneActivityIndicator;
                if (sceneActivityIndicator == null) {
                    return;
                }
                sceneActivityIndicator.gameObject.SetActive(false);
                sceneActivityIndicator.gameObject.transform.SetParent(null);
                sceneActivityIndicator = null;
            }*/
    }
}
