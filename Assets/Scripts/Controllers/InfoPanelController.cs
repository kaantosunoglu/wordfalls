﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using tQsLib;

public class InfoPanelController : MonoBehaviour {

	public static InfoPanelController instance;

	public Text timeText;
	public Text scoreText;
	public Text starText;

	public Image greenTimer;
//	public Text oldScoreText;
//	public Text playerNameText;
	private UnityAction starListener;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Awake(){
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
		//starListener = new UnityAction(
	}

	void OnEnable ()
	{
//		playerNameText.text = GameController.instance.player.playerName;
		timeText.text = "";
		scoreText.text = "";
		starText.text = GameController.instance.player.stars.ToString("N0");
		SetPlayerDetails();
	//	oldScoreText.text = GameController.instance.player.oldScore.ToString("N0");
		EventManager.StartListening (WFConstants.NotificationType.LetterStarReceived, setStar);
		EventManager.StartListening (WFConstants.NotificationType.ExtraTimeReceived, setStar);
		EventManager.StartListening (WFConstants.NotificationType.MyRankReceived, SetMyRank);
		EventManager.StartListening(WFConstants.NotificationType.BuyASecondReceived, SetPlayerDetails);
		EventManager.StartListening(WFConstants.NotificationType.IAPEnded, SetPlayerDetails);
	}

	void OnDisable ()
	{
		EventManager.StopListening (WFConstants.NotificationType.LetterStarReceived, setStar);
		EventManager.StopListening (WFConstants.NotificationType.ExtraTimeReceived, setStar);
		EventManager.StopListening(WFConstants.NotificationType.BuyASecondReceived, SetPlayerDetails);
		EventManager.StopListening(WFConstants.NotificationType.IAPEnded, SetPlayerDetails);
		EventManager.StopListening (WFConstants.NotificationType.MyRankReceived, SetMyRank);
	}

	private void SetPlayerDetails(object o = null) {
		starText.text = GameController.instance.player.stars.ToString("N0");
		setTimeText(GameController.instance.player.seconds);
		
	}

	public void setTimeText(int time){
		timeText.text = time.ToString("N0");
		greenTimer.fillAmount = ExtentionMethods.TimerAmount(time);
	}

	public void setScore(int newScore){
		scoreText.text = newScore.ToString ("N0");	
	}

	public void setStar(int star){
		starText.text = star.ToString ("N0");
		GameController.instance.player.stars = star;
	}

	public void setStar(object star){
		setStar ((int)star);
	}

	private void SetMyRank(object data){
		GameController.instance.player.SetOldScore(data.ToString());
		PlayerPrefs.SetInt ("oldScore", GameController.instance.player.oldScore);
	//	oldScoreText.text = GameController.instance.player.oldScore.ToString("N0");
	}

	public void SetMyRank(LeaderBoardPlayer data){
		GameController.instance.player.oldScore = data.playerScore;
		PlayerPrefs.SetInt("highScore", data.playerScore);
		PlayerPrefs.SetInt ("oldScore", data.playerScore);
			SaverLoader.instance.SaveData();
	//	oldScoreText.text = data.playerScore.ToString("N0");
	}
}
