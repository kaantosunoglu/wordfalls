﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using tQsLib;
using UnityEngine.UI;

public class LeaderboardController : MonoBehaviour {

	public GameObject content;
	public GameObject row;



	public GameObject[] firstThree;
	

	//private bool isActive = false;


	private List<GameObject> rows;



	// Use this for initialization
	void Start () {	
		
	}
	 
	void OnDisable()
	{
		ClearData();
	}

	public void SetData(LeaderBoardPlayers players, string leaderboardType){
        Debug.Log("SETDATA  1");
		ClearData();
		foreach (GameObject item in firstThree)
		{
			item.SetActive(false);
		}

		if(players == null)
			return;
	//	InfoPanelController.instance.SetMyRank(players.me);
		rows = new List<GameObject>();
		for (int i = 0; i < players.LeaderBoardRanks.Count; i++) {
			if(players.LeaderBoardRanks[i].playerRank > 3)
			{
				GameObject r = Instantiate (row, new Vector2 (0, 0), Quaternion.identity);
				r.transform.SetParent (content.transform, false);
				r.GetComponent<LeaderBoardRow> ().SetData (players.LeaderBoardRanks [i]);
				rows.Add (r);
				// if(players.LeaderBoardRanks[i].playerId == GameController.instance.player.playerID){
				// 	InfoPanelController.instance.SetMyRank(players.LeaderBoardRanks[i]);
				// }
			}
		}
        Debug.Log("SETDATA  2");
        if (leaderboardType == "global") {
            Debug.Log("SETDATA  3");
            for (int i = 0; i < players.FirstThreeRanks.Count; i++) {
				firstThree[i].SetActive(true);
				firstThree[i].GetComponent<FirstThreeController>().setData(players.FirstThreeRanks[i]);
				// if(players.LeaderBoardRanks[i].playerId == GameController.instance.player.playerID){
				// 	InfoPanelController.instance.SetMyRank(players.LeaderBoardRanks[i]);
				// }
			}
		}
		else {
            Debug.Log("SETDATA  4");
            for (int i = 0; i < players.LeaderBoardRanks.Count; i++) {
					if(i==3)
						break;
					if(players.LeaderBoardRanks [i] != null) {
						
						firstThree[i].SetActive(true);
						firstThree[i].GetComponent<FirstThreeController>().setData(players.LeaderBoardRanks [i]);
						if(players.LeaderBoardRanks[i].playerId == GameController.instance.player.playerID){
							InfoPanelController.instance.SetMyRank(players.LeaderBoardRanks[i]);
						}
					
				}
			}
		}
		
		/*GameSparkManager.instance.GetMyRank (
			delegate(string obj) {
				
			},
			delegate(string obj) {
				
			});*/
	}



	public void ClearData(){
		
		foreach (GameObject item in firstThree)
		{
			item.SetActive(false);
		}
		if(rows != null){
		for (int i = 0; i < rows.Count; i++) {
			Destroy (rows [i]);
		}
		rows = null;
		}
	}



	
}
