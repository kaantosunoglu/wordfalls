﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using tQsLib;

public class LeaderBoardRow : MonoBehaviour {

	public Text playerName;
	public Text playerScore;
	public Text playerRank;
	public Text timeText;
	public Image bg;
	public Sprite whiteBack;
	public Sprite defaultBack;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void SetData(LeaderBoardPlayer player){
		playerName.text = player.playerName;
		playerScore.text = player.playerScore.ToString("N0");
		playerRank.text = player.playerRank.ToString ("N0");
		timeText.text = player.playerTime.ToString("N0");
		if(player.playerId == GameController.instance.player.playerID) {
			bg.sprite = whiteBack;
		}else {
			bg.sprite = defaultBack;
		}
		//AvatarLoader.instance.setAvatar(player.playerFacebookId, playerAvatar);
		//playerAvatar.setAvatar(player.playerFacebookId);
	} 
}
