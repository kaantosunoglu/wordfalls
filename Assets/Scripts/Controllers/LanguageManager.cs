﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using tQsLib;

public class LanguageManager {

	public static string getText(string textInstance){
		string language = GameController.instance.player.PlayerLanguage == 0 ? "2" : GameController.instance.player.PlayerLanguage.ToString ();
	
		return GameController.instance.languageText.text [textInstance] [language];
	}
}
