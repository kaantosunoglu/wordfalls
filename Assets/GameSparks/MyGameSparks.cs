#pragma warning disable 612,618
#pragma warning disable 0114
#pragma warning disable 0108

using System;
using System.Collections.Generic;
using GameSparks.Core;
using GameSparks.Api.Requests;
using GameSparks.Api.Responses;

//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!
//THIS FILE IS AUTO GENERATED, DO NOT MODIFY!!

namespace GameSparks.Api.Requests{
		public class LogEventRequest_ADD_SECOND : GSTypedRequest<LogEventRequest_ADD_SECOND, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_ADD_SECOND() : base("LogEventRequest"){
			request.AddString("eventKey", "ADD_SECOND");
		}
		public LogEventRequest_ADD_SECOND Set_SECOND( long value )
		{
			request.AddNumber("SECOND", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_ADD_SECOND : GSTypedRequest<LogChallengeEventRequest_ADD_SECOND, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_ADD_SECOND() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "ADD_SECOND");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_ADD_SECOND SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_ADD_SECOND Set_SECOND( long value )
		{
			request.AddNumber("SECOND", value);
			return this;
		}			
	}
	
	public class LogEventRequest_ADD_STAR : GSTypedRequest<LogEventRequest_ADD_STAR, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_ADD_STAR() : base("LogEventRequest"){
			request.AddString("eventKey", "ADD_STAR");
		}
		public LogEventRequest_ADD_STAR Set_STAR( long value )
		{
			request.AddNumber("STAR", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_ADD_STAR : GSTypedRequest<LogChallengeEventRequest_ADD_STAR, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_ADD_STAR() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "ADD_STAR");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_ADD_STAR SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_ADD_STAR Set_STAR( long value )
		{
			request.AddNumber("STAR", value);
			return this;
		}			
	}
	
	public class LogEventRequest_DropLeadeboard : GSTypedRequest<LogEventRequest_DropLeadeboard, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_DropLeadeboard() : base("LogEventRequest"){
			request.AddString("eventKey", "DropLeadeboard");
		}
		
		public LogEventRequest_DropLeadeboard Set_table( string value )
		{
			request.AddString("table", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_DropLeadeboard : GSTypedRequest<LogChallengeEventRequest_DropLeadeboard, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_DropLeadeboard() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "DropLeadeboard");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_DropLeadeboard SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_DropLeadeboard Set_table( string value )
		{
			request.AddString("table", value);
			return this;
		}
	}
	
	public class LogEventRequest_GET_PLAYERLANG : GSTypedRequest<LogEventRequest_GET_PLAYERLANG, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GET_PLAYERLANG() : base("LogEventRequest"){
			request.AddString("eventKey", "GET_PLAYERLANG");
		}
	}
	
	public class LogChallengeEventRequest_GET_PLAYERLANG : GSTypedRequest<LogChallengeEventRequest_GET_PLAYERLANG, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GET_PLAYERLANG() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GET_PLAYERLANG");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GET_PLAYERLANG SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_GET_STAR_LETTERS : GSTypedRequest<LogEventRequest_GET_STAR_LETTERS, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GET_STAR_LETTERS() : base("LogEventRequest"){
			request.AddString("eventKey", "GET_STAR_LETTERS");
		}
		
		public LogEventRequest_GET_STAR_LETTERS Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_GET_STAR_LETTERS : GSTypedRequest<LogChallengeEventRequest_GET_STAR_LETTERS, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GET_STAR_LETTERS() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GET_STAR_LETTERS");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GET_STAR_LETTERS SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_GET_STAR_LETTERS Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
	}
	
	public class LogEventRequest_GIVE_EXTRA_SECONDS : GSTypedRequest<LogEventRequest_GIVE_EXTRA_SECONDS, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GIVE_EXTRA_SECONDS() : base("LogEventRequest"){
			request.AddString("eventKey", "GIVE_EXTRA_SECONDS");
		}
	}
	
	public class LogChallengeEventRequest_GIVE_EXTRA_SECONDS : GSTypedRequest<LogChallengeEventRequest_GIVE_EXTRA_SECONDS, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GIVE_EXTRA_SECONDS() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GIVE_EXTRA_SECONDS");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GIVE_EXTRA_SECONDS SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_HIGH_SCORE : GSTypedRequest<LogEventRequest_HIGH_SCORE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_HIGH_SCORE() : base("LogEventRequest"){
			request.AddString("eventKey", "HIGH_SCORE");
		}
		public LogEventRequest_HIGH_SCORE Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		
		public LogEventRequest_HIGH_SCORE Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_HIGH_SCORE : GSTypedRequest<LogChallengeEventRequest_HIGH_SCORE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_HIGH_SCORE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "HIGH_SCORE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_HIGH_SCORE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_HIGH_SCORE Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogChallengeEventRequest_HIGH_SCORE Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
	}
	
	public class LogEventRequest_GET_PLAYER_PROFILE : GSTypedRequest<LogEventRequest_GET_PLAYER_PROFILE, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_GET_PLAYER_PROFILE() : base("LogEventRequest"){
			request.AddString("eventKey", "GET_PLAYER_PROFILE");
		}
		
		public LogEventRequest_GET_PLAYER_PROFILE Set_PLAYER_ID( string value )
		{
			request.AddString("PLAYER_ID", value);
			return this;
		}
	}
	
	public class LogChallengeEventRequest_GET_PLAYER_PROFILE : GSTypedRequest<LogChallengeEventRequest_GET_PLAYER_PROFILE, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_GET_PLAYER_PROFILE() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "GET_PLAYER_PROFILE");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_GET_PLAYER_PROFILE SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_GET_PLAYER_PROFILE Set_PLAYER_ID( string value )
		{
			request.AddString("PLAYER_ID", value);
			return this;
		}
	}
	
	public class LogEventRequest_serverTime : GSTypedRequest<LogEventRequest_serverTime, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_serverTime() : base("LogEventRequest"){
			request.AddString("eventKey", "serverTime");
		}
	}
	
	public class LogChallengeEventRequest_serverTime : GSTypedRequest<LogChallengeEventRequest_serverTime, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_serverTime() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "serverTime");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_serverTime SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
	}
	
	public class LogEventRequest_SET_PLAYERLANG : GSTypedRequest<LogEventRequest_SET_PLAYERLANG, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SET_PLAYERLANG() : base("LogEventRequest"){
			request.AddString("eventKey", "SET_PLAYERLANG");
		}
		public LogEventRequest_SET_PLAYERLANG Set_LANG( long value )
		{
			request.AddNumber("LANG", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_SET_PLAYERLANG : GSTypedRequest<LogChallengeEventRequest_SET_PLAYERLANG, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SET_PLAYERLANG() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SET_PLAYERLANG");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SET_PLAYERLANG SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SET_PLAYERLANG Set_LANG( long value )
		{
			request.AddNumber("LANG", value);
			return this;
		}			
	}
	
	public class LogEventRequest_SET_STAR_LETTERS : GSTypedRequest<LogEventRequest_SET_STAR_LETTERS, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SET_STAR_LETTERS() : base("LogEventRequest"){
			request.AddString("eventKey", "SET_STAR_LETTERS");
		}
		
		public LogEventRequest_SET_STAR_LETTERS Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
		public LogEventRequest_SET_STAR_LETTERS Set_LETTERS( GSData value )
		{
			request.AddObject("LETTERS", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_SET_STAR_LETTERS : GSTypedRequest<LogChallengeEventRequest_SET_STAR_LETTERS, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SET_STAR_LETTERS() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SET_STAR_LETTERS");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SET_STAR_LETTERS SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SET_STAR_LETTERS Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
		public LogChallengeEventRequest_SET_STAR_LETTERS Set_LETTERS( GSData value )
		{
			request.AddObject("LETTERS", value);
			return this;
		}
		
	}
	
	public class LogEventRequest_SUBMIT_LEADERBOARD_en : GSTypedRequest<LogEventRequest_SUBMIT_LEADERBOARD_en, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SUBMIT_LEADERBOARD_en() : base("LogEventRequest"){
			request.AddString("eventKey", "SUBMIT_LEADERBOARD_en");
		}
		
		public LogEventRequest_SUBMIT_LEADERBOARD_en Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
		public LogEventRequest_SUBMIT_LEADERBOARD_en Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogEventRequest_SUBMIT_LEADERBOARD_en Set_TIME( long value )
		{
			request.AddNumber("TIME", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_SUBMIT_LEADERBOARD_en : GSTypedRequest<LogChallengeEventRequest_SUBMIT_LEADERBOARD_en, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_en() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SUBMIT_LEADERBOARD_en");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_en SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_en Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_en Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_en Set_TIME( long value )
		{
			request.AddNumber("TIME", value);
			return this;
		}			
	}
	
	public class LogEventRequest_SUBMIT_LEADERBOARD_tr : GSTypedRequest<LogEventRequest_SUBMIT_LEADERBOARD_tr, LogEventResponse>
	{
	
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogEventResponse (response);
		}
		
		public LogEventRequest_SUBMIT_LEADERBOARD_tr() : base("LogEventRequest"){
			request.AddString("eventKey", "SUBMIT_LEADERBOARD_tr");
		}
		public LogEventRequest_SUBMIT_LEADERBOARD_tr Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		
		public LogEventRequest_SUBMIT_LEADERBOARD_tr Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
		public LogEventRequest_SUBMIT_LEADERBOARD_tr Set_TIME( long value )
		{
			request.AddNumber("TIME", value);
			return this;
		}			
	}
	
	public class LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr : GSTypedRequest<LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr, LogChallengeEventResponse>
	{
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr() : base("LogChallengeEventRequest"){
			request.AddString("eventKey", "SUBMIT_LEADERBOARD_tr");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LogChallengeEventResponse (response);
		}
		
		/// <summary>
		/// The challenge ID instance to target
		/// </summary>
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr Set_SCORE( long value )
		{
			request.AddNumber("SCORE", value);
			return this;
		}			
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr Set_DAY( string value )
		{
			request.AddString("DAY", value);
			return this;
		}
		public LogChallengeEventRequest_SUBMIT_LEADERBOARD_tr Set_TIME( long value )
		{
			request.AddNumber("TIME", value);
			return this;
		}			
	}
	
}
	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_ACH_EN : GSTypedRequest<LeaderboardDataRequest_ACH_EN,LeaderboardDataResponse_ACH_EN>
	{
		public LeaderboardDataRequest_ACH_EN() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "ACH_EN");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_ACH_EN (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_ACH_EN SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_ACH_EN : GSTypedRequest<AroundMeLeaderboardRequest_ACH_EN,AroundMeLeaderboardResponse_ACH_EN>
	{
		public AroundMeLeaderboardRequest_ACH_EN() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "ACH_EN");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_ACH_EN (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_EN SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_ACH_EN : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_ACH_EN(GSData data) : base(data){}
	}
	
	public class LeaderboardDataResponse_ACH_EN : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_ACH_EN(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_ACH_EN> Data_ACH_EN{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_EN>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_ACH_EN(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_EN> First_ACH_EN{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_EN>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_ACH_EN(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_EN> Last_ACH_EN{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_EN>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_ACH_EN(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_ACH_EN : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_ACH_EN(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_ACH_EN> Data_ACH_EN{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_EN>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_ACH_EN(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_EN> First_ACH_EN{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_EN>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_ACH_EN(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_EN> Last_ACH_EN{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_EN>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_ACH_EN(data);});}
		}
	}
}	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_ACHIEVEMENT_tr : GSTypedRequest<LeaderboardDataRequest_ACHIEVEMENT_tr,LeaderboardDataResponse_ACHIEVEMENT_tr>
	{
		public LeaderboardDataRequest_ACHIEVEMENT_tr() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "ACHIEVEMENT_tr");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_ACHIEVEMENT_tr (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_ACHIEVEMENT_tr SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_ACHIEVEMENT_tr : GSTypedRequest<AroundMeLeaderboardRequest_ACHIEVEMENT_tr,AroundMeLeaderboardResponse_ACHIEVEMENT_tr>
	{
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "ACHIEVEMENT_tr");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_ACHIEVEMENT_tr (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_ACHIEVEMENT_tr SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_ACHIEVEMENT_tr : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_ACHIEVEMENT_tr(GSData data) : base(data){}
	}
	
	public class LeaderboardDataResponse_ACHIEVEMENT_tr : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_ACHIEVEMENT_tr(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr> Data_ACHIEVEMENT_tr{
			get{return new GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_ACHIEVEMENT_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr> First_ACHIEVEMENT_tr{
			get{return new GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_ACHIEVEMENT_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr> Last_ACHIEVEMENT_tr{
			get{return new GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_ACHIEVEMENT_tr(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_ACHIEVEMENT_tr : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_ACHIEVEMENT_tr(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr> Data_ACHIEVEMENT_tr{
			get{return new GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_ACHIEVEMENT_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr> First_ACHIEVEMENT_tr{
			get{return new GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_ACHIEVEMENT_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr> Last_ACHIEVEMENT_tr{
			get{return new GSEnumerable<_LeaderboardEntry_ACHIEVEMENT_tr>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_ACHIEVEMENT_tr(data);});}
		}
	}
}	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_ACH_TR : GSTypedRequest<LeaderboardDataRequest_ACH_TR,LeaderboardDataResponse_ACH_TR>
	{
		public LeaderboardDataRequest_ACH_TR() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "ACH_TR");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_ACH_TR (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_ACH_TR SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_ACH_TR : GSTypedRequest<AroundMeLeaderboardRequest_ACH_TR,AroundMeLeaderboardResponse_ACH_TR>
	{
		public AroundMeLeaderboardRequest_ACH_TR() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "ACH_TR");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_ACH_TR (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_ACH_TR SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_ACH_TR : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_ACH_TR(GSData data) : base(data){}
	}
	
	public class LeaderboardDataResponse_ACH_TR : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_ACH_TR(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_ACH_TR> Data_ACH_TR{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_TR>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_ACH_TR(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_TR> First_ACH_TR{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_TR>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_ACH_TR(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_TR> Last_ACH_TR{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_TR>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_ACH_TR(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_ACH_TR : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_ACH_TR(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_ACH_TR> Data_ACH_TR{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_TR>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_ACH_TR(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_TR> First_ACH_TR{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_TR>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_ACH_TR(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_ACH_TR> Last_ACH_TR{
			get{return new GSEnumerable<_LeaderboardEntry_ACH_TR>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_ACH_TR(data);});}
		}
	}
}	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_SCOREBOARD_en : GSTypedRequest<LeaderboardDataRequest_SCOREBOARD_en,LeaderboardDataResponse_SCOREBOARD_en>
	{
		public LeaderboardDataRequest_SCOREBOARD_en() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "SCOREBOARD_en");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_SCOREBOARD_en (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_en SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_SCOREBOARD_en : GSTypedRequest<AroundMeLeaderboardRequest_SCOREBOARD_en,AroundMeLeaderboardResponse_SCOREBOARD_en>
	{
		public AroundMeLeaderboardRequest_SCOREBOARD_en() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "SCOREBOARD_en");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_SCOREBOARD_en (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_en SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_SCOREBOARD_en : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_SCOREBOARD_en(GSData data) : base(data){}
		public long? SCORE{
			get{return response.GetNumber("SCORE");}
		}
		public long? TIME{
			get{return response.GetNumber("TIME");}
		}
		public string DAY{
			get{return response.GetString("DAY");}
		}
	}
	
	public class LeaderboardDataResponse_SCOREBOARD_en : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_SCOREBOARD_en(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_en> Data_SCOREBOARD_en{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_en>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SCOREBOARD_en(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_en> First_SCOREBOARD_en{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_en>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SCOREBOARD_en(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_en> Last_SCOREBOARD_en{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_en>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SCOREBOARD_en(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_SCOREBOARD_en : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_SCOREBOARD_en(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_en> Data_SCOREBOARD_en{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_en>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SCOREBOARD_en(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_en> First_SCOREBOARD_en{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_en>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SCOREBOARD_en(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_en> Last_SCOREBOARD_en{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_en>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SCOREBOARD_en(data);});}
		}
	}
}	
	
	
namespace GameSparks.Api.Requests{
	
	public class LeaderboardDataRequest_SCOREBOARD_tr : GSTypedRequest<LeaderboardDataRequest_SCOREBOARD_tr,LeaderboardDataResponse_SCOREBOARD_tr>
	{
		public LeaderboardDataRequest_SCOREBOARD_tr() : base("LeaderboardDataRequest"){
			request.AddString("leaderboardShortCode", "SCOREBOARD_tr");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new LeaderboardDataResponse_SCOREBOARD_tr (response);
		}		
		
		/// <summary>
		/// The challenge instance to get the leaderboard data for
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetChallengeInstanceId( String challengeInstanceId )
		{
			request.AddString("challengeInstanceId", challengeInstanceId);
			return this;
		}
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// The offset into the set of leaderboards returned
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetOffset( long offset )
		{
			request.AddNumber("offset", offset);
			return this;
		}
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public LeaderboardDataRequest_SCOREBOARD_tr SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
		
	}

	public class AroundMeLeaderboardRequest_SCOREBOARD_tr : GSTypedRequest<AroundMeLeaderboardRequest_SCOREBOARD_tr,AroundMeLeaderboardResponse_SCOREBOARD_tr>
	{
		public AroundMeLeaderboardRequest_SCOREBOARD_tr() : base("AroundMeLeaderboardRequest"){
			request.AddString("leaderboardShortCode", "SCOREBOARD_tr");
		}
		
		protected override GSTypedResponse BuildResponse (GSObject response){
			return new AroundMeLeaderboardResponse_SCOREBOARD_tr (response);
		}		
		
		/// <summary>
		/// The number of items to return in a page (default=50)
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetEntryCount( long entryCount )
		{
			request.AddNumber("entryCount", entryCount);
			return this;
		}
		/// <summary>
		/// A friend id or an array of friend ids to use instead of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetFriendIds( List<string> friendIds )
		{
			request.AddStringList("friendIds", friendIds);
			return this;
		}
		/// <summary>
		/// Number of entries to include from head of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetIncludeFirst( long includeFirst )
		{
			request.AddNumber("includeFirst", includeFirst);
			return this;
		}
		/// <summary>
		/// Number of entries to include from tail of the list
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetIncludeLast( long includeLast )
		{
			request.AddNumber("includeLast", includeLast);
			return this;
		}
		
		/// <summary>
		/// If True returns a leaderboard of the player's social friends
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetSocial( bool social )
		{
			request.AddBoolean("social", social);
			return this;
		}
		/// <summary>
		/// The IDs of the teams you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetTeamIds( List<string> teamIds )
		{
			request.AddStringList("teamIds", teamIds);
			return this;
		}
		/// <summary>
		/// The type of team you are interested in
		/// </summary>
		public AroundMeLeaderboardRequest_SCOREBOARD_tr SetTeamTypes( List<string> teamTypes )
		{
			request.AddStringList("teamTypes", teamTypes);
			return this;
		}
	}
}

namespace GameSparks.Api.Responses{
	
	public class _LeaderboardEntry_SCOREBOARD_tr : LeaderboardDataResponse._LeaderboardData{
		public _LeaderboardEntry_SCOREBOARD_tr(GSData data) : base(data){}
		public long? SCORE{
			get{return response.GetNumber("SCORE");}
		}
		public string DAY{
			get{return response.GetString("DAY");}
		}
		public long? TIME{
			get{return response.GetNumber("TIME");}
		}
	}
	
	public class LeaderboardDataResponse_SCOREBOARD_tr : LeaderboardDataResponse
	{
		public LeaderboardDataResponse_SCOREBOARD_tr(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr> Data_SCOREBOARD_tr{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SCOREBOARD_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr> First_SCOREBOARD_tr{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SCOREBOARD_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr> Last_SCOREBOARD_tr{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SCOREBOARD_tr(data);});}
		}
	}
	
	public class AroundMeLeaderboardResponse_SCOREBOARD_tr : AroundMeLeaderboardResponse
	{
		public AroundMeLeaderboardResponse_SCOREBOARD_tr(GSData data) : base(data){}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr> Data_SCOREBOARD_tr{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr>(response.GetObjectList("data"), (data) => { return new _LeaderboardEntry_SCOREBOARD_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr> First_SCOREBOARD_tr{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr>(response.GetObjectList("first"), (data) => { return new _LeaderboardEntry_SCOREBOARD_tr(data);});}
		}
		
		public GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr> Last_SCOREBOARD_tr{
			get{return new GSEnumerable<_LeaderboardEntry_SCOREBOARD_tr>(response.GetObjectList("last"), (data) => { return new _LeaderboardEntry_SCOREBOARD_tr(data);});}
		}
	}
}	

namespace GameSparks.Api.Messages {

		public class ScriptMessage_48HOURS_EN : ScriptMessage {
		
			public new static Action<ScriptMessage_48HOURS_EN> Listener;
	
			public ScriptMessage_48HOURS_EN(GSData data) : base(data){}
	
			private static ScriptMessage_48HOURS_EN Create(GSData data)
			{
				ScriptMessage_48HOURS_EN message = new ScriptMessage_48HOURS_EN (data);
				return message;
			}
	
			static ScriptMessage_48HOURS_EN()
			{
				handlers.Add (".ScriptMessage_48HOURS_EN", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_48HOURS_TR : ScriptMessage {
		
			public new static Action<ScriptMessage_48HOURS_TR> Listener;
	
			public ScriptMessage_48HOURS_TR(GSData data) : base(data){}
	
			private static ScriptMessage_48HOURS_TR Create(GSData data)
			{
				ScriptMessage_48HOURS_TR message = new ScriptMessage_48HOURS_TR (data);
				return message;
			}
	
			static ScriptMessage_48HOURS_TR()
			{
				handlers.Add (".ScriptMessage_48HOURS_TR", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_ACH_MESSAGE_EN : ScriptMessage {
		
			public new static Action<ScriptMessage_ACH_MESSAGE_EN> Listener;
	
			public ScriptMessage_ACH_MESSAGE_EN(GSData data) : base(data){}
	
			private static ScriptMessage_ACH_MESSAGE_EN Create(GSData data)
			{
				ScriptMessage_ACH_MESSAGE_EN message = new ScriptMessage_ACH_MESSAGE_EN (data);
				return message;
			}
	
			static ScriptMessage_ACH_MESSAGE_EN()
			{
				handlers.Add (".ScriptMessage_ACH_MESSAGE_EN", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_ACH_MESSAGE_TR : ScriptMessage {
		
			public new static Action<ScriptMessage_ACH_MESSAGE_TR> Listener;
	
			public ScriptMessage_ACH_MESSAGE_TR(GSData data) : base(data){}
	
			private static ScriptMessage_ACH_MESSAGE_TR Create(GSData data)
			{
				ScriptMessage_ACH_MESSAGE_TR message = new ScriptMessage_ACH_MESSAGE_TR (data);
				return message;
			}
	
			static ScriptMessage_ACH_MESSAGE_TR()
			{
				handlers.Add (".ScriptMessage_ACH_MESSAGE_TR", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_ENDOFDAYMSG_EN : ScriptMessage {
		
			public new static Action<ScriptMessage_ENDOFDAYMSG_EN> Listener;
	
			public ScriptMessage_ENDOFDAYMSG_EN(GSData data) : base(data){}
	
			private static ScriptMessage_ENDOFDAYMSG_EN Create(GSData data)
			{
				ScriptMessage_ENDOFDAYMSG_EN message = new ScriptMessage_ENDOFDAYMSG_EN (data);
				return message;
			}
	
			static ScriptMessage_ENDOFDAYMSG_EN()
			{
				handlers.Add (".ScriptMessage_ENDOFDAYMSG_EN", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_ENDOFDAYMSG_TR : ScriptMessage {
		
			public new static Action<ScriptMessage_ENDOFDAYMSG_TR> Listener;
	
			public ScriptMessage_ENDOFDAYMSG_TR(GSData data) : base(data){}
	
			private static ScriptMessage_ENDOFDAYMSG_TR Create(GSData data)
			{
				ScriptMessage_ENDOFDAYMSG_TR message = new ScriptMessage_ENDOFDAYMSG_TR (data);
				return message;
			}
	
			static ScriptMessage_ENDOFDAYMSG_TR()
			{
				handlers.Add (".ScriptMessage_ENDOFDAYMSG_TR", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_LASTHOUR_EN : ScriptMessage {
		
			public new static Action<ScriptMessage_LASTHOUR_EN> Listener;
	
			public ScriptMessage_LASTHOUR_EN(GSData data) : base(data){}
	
			private static ScriptMessage_LASTHOUR_EN Create(GSData data)
			{
				ScriptMessage_LASTHOUR_EN message = new ScriptMessage_LASTHOUR_EN (data);
				return message;
			}
	
			static ScriptMessage_LASTHOUR_EN()
			{
				handlers.Add (".ScriptMessage_LASTHOUR_EN", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_LASTHOUR_TR : ScriptMessage {
		
			public new static Action<ScriptMessage_LASTHOUR_TR> Listener;
	
			public ScriptMessage_LASTHOUR_TR(GSData data) : base(data){}
	
			private static ScriptMessage_LASTHOUR_TR Create(GSData data)
			{
				ScriptMessage_LASTHOUR_TR message = new ScriptMessage_LASTHOUR_TR (data);
				return message;
			}
	
			static ScriptMessage_LASTHOUR_TR()
			{
				handlers.Add (".ScriptMessage_LASTHOUR_TR", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_NEWDAY_EN : ScriptMessage {
		
			public new static Action<ScriptMessage_NEWDAY_EN> Listener;
	
			public ScriptMessage_NEWDAY_EN(GSData data) : base(data){}
	
			private static ScriptMessage_NEWDAY_EN Create(GSData data)
			{
				ScriptMessage_NEWDAY_EN message = new ScriptMessage_NEWDAY_EN (data);
				return message;
			}
	
			static ScriptMessage_NEWDAY_EN()
			{
				handlers.Add (".ScriptMessage_NEWDAY_EN", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}
		public class ScriptMessage_NEWDAY_TR : ScriptMessage {
		
			public new static Action<ScriptMessage_NEWDAY_TR> Listener;
	
			public ScriptMessage_NEWDAY_TR(GSData data) : base(data){}
	
			private static ScriptMessage_NEWDAY_TR Create(GSData data)
			{
				ScriptMessage_NEWDAY_TR message = new ScriptMessage_NEWDAY_TR (data);
				return message;
			}
	
			static ScriptMessage_NEWDAY_TR()
			{
				handlers.Add (".ScriptMessage_NEWDAY_TR", Create);
	
			}
			
			override public void NotifyListeners()
			{
				if (Listener != null)
				{
					Listener (this);
				}
			}
		}

}
