﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace tQsLib
{
	[System.Serializable]
	public class SaveGameData {

		public string playerName;
		public int oldScore;
		public int seconds;
		public int stars;
		public string date;
		
	}
}

