﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WordPanelController : MonoBehaviour {

	public static WordPanelController instance;
	public Text wordText;
	public Text scoreText;


	private int oldScore;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void Awake(){
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	void OnEnable(){
		wordText.text = "";
		scoreText.text = "";

	}


	public void setWord(List<LetterTile> selectedTiles){
		setPoint (selectedTiles);
		string word = "";
		foreach (LetterTile lt in selectedTiles) {
			word += lt.settings.isJoker ? " " : lt.settings.letter;
		}
		wordText.text = checkForJoker (word);
	}

	public void clear(){
		wordText.text = "";
		scoreText.text = "";

	}

	private void setPoint(List<LetterTile> selectedTiles){
		int point = 0;
		int multi = 1;
		foreach (LetterTile lt in selectedTiles) {
			point += lt.settings.isJoker ? 0 : (GameController.instance.player.PlayerLanguage == 1 ? WFConstants.trLettersScores[lt.settings.letter] : WFConstants.enLettersScores[lt.settings.letter]);
			if (lt.isMultiplier) {
				multi = multi * 2;
			}
		}
		point = point * selectedTiles.Count * multi;
		scoreText.text = point.ToString("N0");

	}

	private string checkForJoker(string word){
		string fakeWord = word;
		if (word.IndexOf (" ") > -1) {
			fakeWord = fakeWord.Remove (word.IndexOf (" "),1);
			fakeWord = fakeWord.Insert (word.IndexOf (" "), "�");
			fakeWord = checkForJoker (fakeWord);
		}
		return fakeWord;
	}

}
