﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameSparks.Api.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using tQsLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class HowToBoardController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

    public static HowToBoardController instance;

    public GameObject letterTile;
    public HowTo howTo;

    // image

    public GameObject mask;

    public int step;

    //data
    private Words wordList;
    private List<Board> boards;
    private List<int[]> boardMap;

    private bool isMouseDown = false;
    private Coroutine drawing;
    private Rect touchRect;
    private Rect touchRect2;
    private Rect touchRect3;
    private Rect touchRect4;
    private Rect touchRect5;
    private Vector2 testHitVector;
    private Dictionary<int, LetterTile[]> boardTiles;
    private Dictionary<int, Transform[]> tilesGameobjects;
    private List<LetterTile> selectedLetterTiles;
    private LetterTile lastSelectedTile;

    //timer
    private float totalTime;
    private float currentTime;
    private bool isTimerActive;

    private int totalScore;
    private int totalJoker;
    private bool firstRun = true;
    private Dictionary<int, int[]> stockBoardDataCoordinates;
    private long pauseTime;
    private long focusTime;

    private LeaderBoardPlayers globalLeaderboardPlayers;
    private LeaderBoardPlayers facebookFriendsLeaderboardPlayers;

    void Awake() {
        if (instance == null) {
            instance = this;
            return;
        }
    }

    // Use this for initialization
    void Start() {
        //startGame ();

    }

    void OnEnable() {
        step = 1;
        GetUsedStarLetters();
    }
    void OnDisable() {

    }

    private void prepareGame() {
        //check date !!!!
        totalScore = 0;

        boards = new List<Board>();
        boardMap = new List<int[]>();
        Board b1;
        Board b2;
        if (GameController.instance.player.PlayerLanguage == 1) {
            b1 = new Board("{\"data\":{\"0\":[[\"V\",0,0,0],[\"K\",1,0,0],[\"İ\",0,0,0],[\"O\",0,0,0],[\"P\",0,0,0]],\"1\":[[\"O\",0,0,0],[\"K\",0,0,0],[\"E\",0,0,0],[\"Y\",0,0,0],[\"A\",0,0,0]],\"2\":[[\"E\",1,0,0],[\"J\",0,0,0],[\"S\",0,1,0],[\"U\",0,0,0],[\"E\",0,0,1]],\"3\":[[\"R\",0,0,0],[\"E\",0,0,0],[\"D\",0,0,1],[\"N\",0,0,0],[\"V\",0,0,0]],\"4\":[[\"H\",0,0,0],[\"N\",0,0,0],[\"E\",0,0,0],[\"R\",0,0,1],[\"E\",0,0,0]]}}");
            b2 = new Board("{\"data\":{\"0\":[[\"A\",0,0,0],[\"L\",0,0,0],[\"V\",0,0,0],[\"R\",0,0,1],[\"E\",0,0,0]],\"1\":[[\"S\",1,0,0],[\"E\",0,0,1],[\"İ\",0,0,0],[\"Y\",0,0,0],[\"T\",0,0,0]],\"2\":[[\"R\",0,0,0],[\"I\",0,0,0],[\"N\",0,0,0],[\"M\",0,0,1],[\"A\",0,0,0]],\"3\":[[\"S\",0,0,0],[\"Z\",0,0,0],[\"Ü\",0,0,0],[\"R\",0,0,1],[\"E\",0,0,0]],\"4\":[[\"T\",0,0,1],[\"A\",0,0,0],[\"H\",0,1,0],[\"A\",0,0,0],[\"P\",0,0,1]]}}");
        } else {
            b1 = new Board("{\"data\":{\"0\":[[\"S\",0,0,0],[\"T\",0,0,0],[\"I\",0,0,0],[\"G\",0,0,0],[\"E\",0,0,0]],\"1\":[[\"K\",0,0,0],[\"E\",1,0,0],[\"Q\",0,1,0],[\"A\",0,0,0],[\"R\",0,0,0]],\"2\":[[\"R\",0,0,0],[\"O\",0,0,0],[\"O\",0,1,0],[\"M\",0,0,0],[\"R\",0,0,1]],\"3\":[[\"J\",0,0,0],[\"C\",0,0,0],[\"L\",0,0,1],[\"E\",0,0,0],[\"O\",0,0,0]],\"4\":[[\"S\",0,0,1],[\"L\",0,0,0],[\"N\",0,0,0],[\"E\",0,0,1],[\"E\",0,0,0]]}}");
            b2 = new Board("{\"data\":{\"0\":[[\"T\",0,0,0],[\"C\",0,0,0],[\"E\",0,1,0],[\"L\",0,0,1],[\"E\",0,0,0]],\"1\":[[\"O\",1,0,0],[\"C\",0,0,1],[\"K\",0,1,0],[\"S\",0,0,0],[\"U\",0,1,0]],\"2\":[[\"I\",0,0,0],[\"C\",0,0,0],[\"P\",0,0,0],[\"R\",0,0,1],[\"U\",0,0,0]],\"3\":[[\"D\",0,0,0],[\"S\",0,0,0],[\"E\",0,0,0],[\"B\",0,0,1],[\"Q\",0,0,0]],\"4\":[[\"I\",0,0,1],[\"O\",0,0,0],[\"A\",0,0,0],[\"O\",0,0,0],[\"V\",0,0,1]]}}");

        }
        boards.Add(b1);
        boards.Add(b2);

        for (int i = 0; i < WFConstants.boardWidth; i++) {
            boardMap.Add(new int[2] { 0, 0 });
        }

        PopupController.instance.ClosePopup("LoadingPopup");
        startGame();
        //Debug.Log ("-----kaan");
    }

    public void startGame() {

        firstRun = false;
        totalJoker = 0;
        selectedLetterTiles = new List<LetterTile>();
        boardTiles = new Dictionary<int, LetterTile[]>();
        tilesGameobjects = new Dictionary<int, Transform[]>();
        for (int i = 0; i < WFConstants.boardWidth; i++) {
            LetterTile[] tiles = new LetterTile[WFConstants.boardHeight];
            Transform[] gos = new Transform[WFConstants.boardHeight];
            for (int j = 0; j < WFConstants.boardHeight; j++) {
                GameObject hede = Instantiate(letterTile, new Vector2(0, 0), Quaternion.identity);
                hede.transform.SetParent(transform, false);

                Tile t = new Tile();
                t.i = i;
                t.j = j;

                Board b = boards[(int)boardMap[i][0]];
                List<ArrayList> arrL = b.board[i.ToString()];
                ArrayList arr = arrL[boardMap[i][1]];
                //ArrayList srr2 = arr [(int)boardMap [i].y];
                t.letter = arr[0].ToString(); //boards [boardMap [i].x.ToString()].board ["b"+i.ToString()] [boardMap [i].y];
                //t.isJoker = forJoker ? System.Convert.ToInt32(arr[1]) == 1 : false;
                t.isJoker = System.Convert.ToInt32(arr[1]) == 1;
                t.isMultiplier = System.Convert.ToInt32(arr[2]) == 1;
                t.isStar = System.Convert.ToInt32(arr[3]) == 1;
                t.code = boardMap[i][0] + "" + i.ToString() + "" + j.ToString();
                boardMap[i][1]++;
                //              Debug.Log (boardMap[i][1]);
                if (boardMap[i][1] == WFConstants.boardHeight) {
                    boardMap[i][0]++;
                    boardMap[i][1] = 0;
                    //Vector2 v2 = new Vector2 (boardMap [i][0]++, 0);
                    //boardMap [i] = v2;
                }
                //t.letter = i.ToString () + j.ToString ();

                hede.transform.GetComponent<LetterTile>().setData(t);
                tiles[j] = hede.transform.GetComponent<LetterTile>();
                gos[j] = hede.transform;
            }
            boardTiles.Add(i, tiles);
            tilesGameobjects.Add(i, gos);
        }
        setTouchArea();
        StartCoroutine(startTimer());
    }

    private IEnumerator startTimer() {

        yield return new WaitForSeconds(1);

        isTimerActive = true;
    }

    private void clearBoard() {
        if (boardTiles.Count > 0) {
            for (int i = 0; i < WFConstants.boardWidth; i++) {
                LetterTile[] tilez = boardTiles[i];
                for (int j = 0; j < WFConstants.boardHeight; j++) {
                    tilez[j].destroy();
                }
            }
            boardTiles.Clear();
        }

    }

    private void SelectLetter(Vector2 pointerPosition) {

        int selectedLetterTilesCount = selectedLetterTiles.Count;

        testHitVector = TestHit(pointerPosition);
        //Debug.Log (testHitVector.x +" || "+ (int)testHitVector.y );
        if ((int)testHitVector.x < 0 || (int)testHitVector.y < 0)
            return;
        if (lastSelectedTile && (testHitVector.x == lastSelectedTile.settings.i && testHitVector.y == lastSelectedTile.settings.j)) {
            return;
        }

        LetterTile lt = (LetterTile)boardTiles[(int)testHitVector.x][(int)testHitVector.y];
        int indexOfNewTile = selectedLetterTiles.FindIndex(d => d == lt);
        if (selectedLetterTiles != null && indexOfNewTile > -1 && indexOfNewTile != selectedLetterTilesCount - 2)
            return;
        if (selectedLetterTilesCount > 15)
            return;

        if ((lt.settings.isJoker && indexOfNewTile != selectedLetterTilesCount - 2) && totalJoker == 2)
            return;

        if (selectedLetterTilesCount > 1 && indexOfNewTile == selectedLetterTilesCount - 2) {
            lastSelectedTile.setUnselect();
            lastSelectedTile = selectedLetterTiles[selectedLetterTilesCount - 2];
            selectedLetterTiles.RemoveAt(selectedLetterTilesCount - 1);
            if (lt.settings.isJoker) {
                totalJoker--;
            }
        } else {
            lastSelectedTile = lt;
            lastSelectedTile.setSelected();
            selectedLetterTiles.Add(lt);
            if (lt.settings.isJoker) {
                totalJoker++;
            }
        }

    }

    private void checkWord() {
        totalJoker = 0;
        if (isTimerActive) {
            if (selectedLetterTiles.Count < 2) {
                //Debug.Log (selectedLetterTiles.Count);
                foreach (var item in selectedLetterTiles) {
                    item.setUnselect();
                }
                //lastSelectedTile = new LetterTile ();
                lastSelectedTile = null;
                selectedLetterTiles.Clear();
                return;
            } else {
                string selectedWord = "";
                //kelimeyi bul
                foreach (LetterTile lt in selectedLetterTiles) {
                    selectedWord += lt.settings.isJoker ? " " : lt.settings.letter;
                }

                if (isWordInThelist(selectedWord)) {
                    Debug.Log("======>>>> checkWord 1");
                    List<string> starLettersList = new List<string>();

                    int point = 0;
                    int star = 0;
                    int multi = 1;

                    foreach (LetterTile lt in selectedLetterTiles) {
                        //point += lt.settings.isJoker ? 0 : (GameController.instance.player.PlayerLanguage == 1 ? WFConstants.trLettersScores[lt.settings.letter] : WFConstants.enLettersScores[lt.settings.letter]);
                        point += lt.point;
                        if (lt.isStar) {
                            Debug.Log("STAR  " + lt.settings.code);
                            starLettersList.Add(lt.settings.code);
                            star++;
                        }
                        if (lt.isMultiplier) {
                            multi = multi * 2;
                        }
                        boardTiles[lt.settings.i][lt.settings.j] = null;

                    }

                    point = point * selectedWord.Length * multi;
                    totalScore += point;

                } else {
                    foreach (var item in selectedLetterTiles) {
                        item.setUnselect();
                    }
                    selectedLetterTiles.Clear();
                }

                reGenareteBoard();
                //lastSelectedTile = new LetterTile ();
                lastSelectedTile = null;
            }
        } else {
            foreach (var item in selectedLetterTiles) {
                item.setUnselect();
            }
            lastSelectedTile = null;
            selectedLetterTiles.Clear();
        }

    }

    private void reGenareteBoard() {
        List<int> columns = findColumnsOfSelectedTiles();

        foreach (int c in columns) {
            int emptyTile = 0;
            for (int n = 0; n < WFConstants.boardHeight; n++) {
                if (boardTiles[c][n] == null) {
                    //Debug.Log ("empty tile " + c + "[]" + n);
                    emptyTile++;
                } else {
                    if (emptyTile > 0) {
                        Tile t = new Tile();
                        t.i = boardTiles[c][n].settings.i;
                        t.j = boardTiles[c][n].settings.j - emptyTile;
                        t.letter = boardTiles[c][n].settings.letter;
                        t.isJoker = boardTiles[c][n].settings.isJoker;
                        t.isStar = boardTiles[c][n].isStar;
                        t.isMultiplier = boardTiles[c][n].settings.isMultiplier;
                        t.code = boardTiles[c][n].settings.code;
                        boardTiles[c][n].setData(t);
                        boardTiles[c][n - emptyTile] = boardTiles[c][n];
                        boardTiles[c][n] = null;
                    }
                }
            }

            for (int n = WFConstants.boardHeight - emptyTile; n < WFConstants.boardHeight; n++) {
                //Debug.Log(c+" --------- " +n);
                boardTiles[c][n] = selectedLetterTiles[0];
                Tile t = new Tile();
                t.i = c;
                t.j = n;

                Board b = boards[(int)boardMap[c][0]];
                List<ArrayList> arrL = b.board[c.ToString()];
                ArrayList arr = arrL[boardMap[c][1]];
                //ArrayList srr2 = arr [(int)boardMap [i].y];
                t.letter = arr[0].ToString(); //boards [boardMap [i].x.ToString()].board ["b"+i.ToString()] [boardMap [i].y];
                t.code = boardMap[c][0].ToString() + "" + c + "" + boardMap[c][1];

                t.isJoker = System.Convert.ToInt32(arr[1]) == 1;
                t.isMultiplier = System.Convert.ToInt32(arr[2]) == 1;
                t.isStar = System.Convert.ToInt32(arr[3]) == 1;
                boardMap[c][1]++;
                Debug.Log(boardMap[c][1]);
                if (boardMap[c][1] == WFConstants.boardHeight) {
                    boardMap[c][0]++;
                    boardMap[c][1] = 0;
                    //Vector2 v2 = new Vector2 (boardMap [i][0]++, 0);
                    //boardMap [i] = v2;
                }

                //t.letter = "N"+n;
                boardTiles[c][n].setData(t);
                selectedLetterTiles.RemoveAt(0);
                //Debug.Log ("selectedLetterTiles.Count " + selectedLetterTiles.Count);
            }

        }

        selectedLetterTiles.Clear();
    }

    private List<int> findColumnsOfSelectedTiles() {
        List<int> columns = new List<int>();

        foreach (LetterTile lt in selectedLetterTiles) {
            if (columns.IndexOf(lt.settings.i) < 0) {
                columns.Add(lt.settings.i);
            }
        }

        return columns;

    }

    private bool isWordInThelist(string word) {
        Debug.Log(word);
        if (step == 2) {
            if (word == "OYUN" || word == "GAME") {
                howTo.changeStep();
                changeStep();
                return true;
            }
        } else if (step == 3) {
            if (word == "HESAP" || word == "SCORE") {
                howTo.changeStep();
                changeStep();
                return true;
            }
        } else if (step == 4) {
            if (word == "JOK R") {
                howTo.changeStep();
                changeStep();
                return true;
            }
        } else if (step == 5) {
            if (word == "EV" || word == "VE" || word == "ON" || word == "NO") {
                howTo.changeStep();
                changeStep();
                return true;
            }
        }
        return false;
    }

    private Vector2 TestHit(Vector2 pointerPosition) {
        int i = (int)pointerPosition.x / WFConstants.tileWidth;
        int j = (int)pointerPosition.y / WFConstants.tileHeight;
        if (i < 0 || i >= WFConstants.boardWidth || j < 0 || j >= WFConstants.boardHeight) return new Vector2(-1, -1);
        if ((touchRect.Contains(pointerPosition) || touchRect2.Contains(pointerPosition) || touchRect3.Contains(pointerPosition) || touchRect4.Contains(pointerPosition) || touchRect5.Contains(pointerPosition)) && isAdjacentTo(i, j)) return new Vector2(i, j);
        return new Vector2(-1, -1);
    }

    public bool isAdjacentTo(int i, int j) {
        if (lastSelectedTile == null)
            return true;
        return !(1 < Mathf.Abs(lastSelectedTile.settings.i - i) || 1 < Mathf.Abs(lastSelectedTile.settings.j - j));
    }

    //MOUSE CONTROLLERS

    public void OnPointerDown(PointerEventData eventData) {
        /*  Vector2 localCursor;
            if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), eventData.position, eventData.pressEventCamera, out localCursor))
                return;

            Debug.Log("LocalCursor:" + localCursor); 
        */
        Debug.Log("KAAN 1 " + isTimerActive + " " + isMouseDown);
        if (isTimerActive && !isMouseDown) {
            Debug.Log("KAAN 2");
            drawing = StartCoroutine(TrackPointer(eventData));
        }
    }

    public void OnPointerUp(PointerEventData eventData) {
        if (!isMouseDown)
            return;

        StopCoroutine(drawing);
        isMouseDown = false;
        Debug.Log("OLASILIK 1");
        checkWord();
    }

    IEnumerator TrackPointer(PointerEventData eventData) {
        if (isTimerActive) {
            isMouseDown = true;
            Vector3 mousePos = Vector3.zero;
            Vector2 localCursor;
            while (Application.isPlaying) {
                /*if (Input.mousePosition != mousePos) {
                mousePos = Input.mousePosition;
            }*/

                if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), eventData.position, eventData.pressEventCamera, out localCursor))
                    Debug.Log("LocalCursor 1:" + localCursor);
                SelectLetter(localCursor);
                yield return 0;
            }
        } else {
            isMouseDown = false;
            StopCoroutine(drawing);
            Debug.Log("OLASILIK 2");
            //checkWord ();
        }
    }

    private void creatStockData() {
        stockBoardDataCoordinates.Clear();
        for (int t = 0; t < WFConstants.boardWidth; t++) {
            int[] a = new int[2];
            a[0] = 0;
            a[1] = 0;
            stockBoardDataCoordinates.Add(t, a);

        }

    }

    private string getDataFromLocal(string fileName) {
        string targetFile = File.ReadAllText(Application.persistentDataPath + "/" + fileName);
        return targetFile;
    }

    public void GetUsedStarLetters() {
        GameSparkManager.instance.GetStarLetter(
            delegate (bool response) {
                //GameController.instance.StartGamePlay (gameObject);
                prepareGame();
            },
            delegate (string obj) {

            });
    }

    public void changeStep() {
        Debug.Log("{}{}{}{}{}{}{}{}{++++++++++ changestep");
        step++;
        setTouchArea();
    }

    private void setTouchArea() {
        touchRect = new Rect(new Vector2(0, 0), new Vector2(0, 0));
        touchRect2 = new Rect(new Vector2(0, 0), new Vector2(0, 0));
        touchRect3 = new Rect(new Vector2(0, 0), new Vector2(0, 0));
        touchRect4 = new Rect(new Vector2(0, 0), new Vector2(0, 0));
        if (step == 1) {
            mask.transform.SetAsLastSibling();

        } else if (step == 2) {
            mask.transform.SetAsLastSibling();
            tilesGameobjects[0][3].SetAsLastSibling();
            tilesGameobjects[1][3].SetAsLastSibling();
            tilesGameobjects[2][3].SetAsLastSibling();
            tilesGameobjects[3][3].SetAsLastSibling();

            touchRect.x = 0 * WFConstants.tileWidth;
            touchRect.y = 3 * WFConstants.tileHeight;
            touchRect.width = (WFConstants.tileHeight * 4) - (WFConstants.BRICK_TOUCH_MARGIN * 4);
            touchRect.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;

        } else if (step == 3) {
            mask.transform.SetAsLastSibling();
            tilesGameobjects[0][4].SetAsLastSibling();
            tilesGameobjects[1][4].SetAsLastSibling();
            tilesGameobjects[2][2].SetAsLastSibling();
            tilesGameobjects[3][1].SetAsLastSibling();
            tilesGameobjects[4][0].SetAsLastSibling();

            touchRect.x = 0 * WFConstants.tileWidth;
            touchRect.y = 3 * WFConstants.tileHeight;
            touchRect.width = (WFConstants.tileHeight * 2) - (WFConstants.BRICK_TOUCH_MARGIN);
            touchRect.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect2.x = 2 * WFConstants.tileWidth;
            touchRect2.y = 2 * WFConstants.tileHeight;
            touchRect2.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN * 1);
            touchRect2.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect3.x = 3 * WFConstants.tileWidth;
            touchRect3.y = 1 * WFConstants.tileHeight;
            touchRect3.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN * 1);
            touchRect3.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect4.x = 4 * WFConstants.tileWidth;
            touchRect4.y = 0 * WFConstants.tileHeight;
            touchRect4.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN * 1);
            touchRect4.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;
        } else if (step == 4) {
            mask.transform.SetAsLastSibling();
            tilesGameobjects[1][0].SetAsLastSibling();
            tilesGameobjects[1][1].SetAsLastSibling();
            tilesGameobjects[2][0].SetAsLastSibling();
            tilesGameobjects[2][1].SetAsLastSibling();
            tilesGameobjects[3][0].SetAsLastSibling();

            touchRect.x = 1 * WFConstants.tileWidth;
            touchRect.y = 0 * WFConstants.tileHeight;
            touchRect.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN);
            touchRect.height = (WFConstants.tileHeight * 1) - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect2.x = 1 * WFConstants.tileWidth;
            touchRect2.y = 1 * WFConstants.tileHeight;
            touchRect2.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN);
            touchRect2.height = (WFConstants.tileHeight * 1) - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect3.x = 2 * WFConstants.tileWidth;
            touchRect3.y = 0 * WFConstants.tileHeight;
            touchRect3.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN);
            touchRect3.height = (WFConstants.tileHeight * 1) - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect4.x = 2 * WFConstants.tileWidth;
            touchRect4.y = 1 * WFConstants.tileHeight;
            touchRect4.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN);
            touchRect4.height = (WFConstants.tileHeight * 1) - WFConstants.BRICK_TOUCH_MARGIN;

            touchRect5.x = 3 * WFConstants.tileWidth;
            touchRect5.y = 0 * WFConstants.tileHeight;
            touchRect5.width = (WFConstants.tileHeight * 1) - (WFConstants.BRICK_TOUCH_MARGIN * 1);
            touchRect5.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;

        } else if (step == 5) {
            mask.transform.SetAsLastSibling();
            tilesGameobjects[3][4].SetAsLastSibling();
            tilesGameobjects[4][2].SetAsLastSibling();

            touchRect.x = 3 * WFConstants.tileWidth;
            touchRect.y = 1 * WFConstants.tileHeight;
            touchRect.width = (WFConstants.tileHeight * 2) - (WFConstants.BRICK_TOUCH_MARGIN);
            touchRect.height = (WFConstants.tileHeight * 1) - WFConstants.BRICK_TOUCH_MARGIN;
        }

        //touchRect = new Rect (new Vector2 (0, 0), new Vector2 (150, 150));
    }
}