﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvasResizer : MonoBehaviour {

	public Camera cam;
	public CanvasScaler scaler;
	void Awake() {

	}

	// Use this for initialization
	void Start () {
	
		if(cam.aspect > 0.5f) {
			scaler.matchWidthOrHeight = 1.0f;
		} else {
			scaler.matchWidthOrHeight = 0f;
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
