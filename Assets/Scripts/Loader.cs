﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameSparks.Api.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using tQsLib;
using UnityEngine;
using UnityEngine.UI;
using UTNotifications;
using UnityEngine.SocialPlatforms;

public class Loader : MonoBehaviour {

	public static Loader instance;
	public BoardsList boardList;
	public GameObject selectLanguagePanel;
	public GameObject loadingPanel;
	public GameObject loginPanel;
	public GameObject guestLoginPanel;
	public InputField nickText;
	public GameObject howTo;
	public Text loadingText;

	private int totalBoard;

	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	// Use this for initialization
	void Start () {

	}

	void OnEnable () {
	//	PlayerPrefs.SetInt("ads", -1);
		EventManager.StartListening (WFConstants.NotificationType.MyRankReceived, SetMyRank);
		EventManager.StartListening (WFConstants.NotificationType.VirtualGoodsReceived, SetVirtualGoods);
		GameController.instance.languageText = new LanguageTexts (loadLanguageTexts ());
		checkGameVersion();
		loadingText.text = PlayerPrefs.GetInt ("language") == 1 ? "YÜKLÜYOR..." : "LOADING...";
	}

	// Update is called once per frame
	void Update () {

	}

	// 1) Check for game version.
	// If different update game app
	// else step over
	private void checkGameVersion () {
		if (!ReachabilityManager.instance.IsReachable) {
			//Debug.Log ("STEP 1");
			noInternetConnection ();
		} else {
			HTTPController.instance.CheckGameVersion (
				delegate (string data) {

					JToken gameVersionData = JToken.Parse (data);
					JObject obj = JObject.Parse (gameVersionData["data"].ToString ());
					int v = (int) obj["version"];
					if (WFConstants.GAME_VERSION == v) {
						getLanguage ();
					} else {
						//yeni versiyonu yuklet 
						PopupController.instance.OpenPopup ("NewVersionPopup");
					}
				},
				delegate (string Error) {
					//Error e gore hareket et
					Debug.Log (Error);
				}
			);
		}

	}

	// 2) Get player language from PlayerPrefs
	// if there is not language than open select language screen then register-login screen 
	// else login
	private void getLanguage () {

#if UNITY_EDITOR

			PlayerPrefs.DeleteAll ();
#endif

		// check player language
		// if not exits ask 
		// else look for credentials 
		if (!ReachabilityManager.instance.IsReachable) {
			noInternetConnection ();
		} else {
			if (PlayerPrefs.GetInt ("language", 0) == 0) {

				selectLanguagePanel.SetActive (true);
				loadingPanel.SetActive(false);
			} else {

				GameController.instance.player.PlayerLanguage = PlayerPrefs.GetInt ("language");
				GameController.instance.player.PlayerBoardLanguage = PlayerPrefs.GetInt ("boardLang", 0);

				selectLanguagePanel.SetActive (false);
				//askAdverd ();
				userCredentials();
			}
		}

	}

	// 	private void askAdverd() {
	// 	if(PlayerPrefs.GetInt("ads", -1) == -1) {
	// 		PopupController.instance.OpenPopup("AdvertPopup");
	// 	}else{
	// 		userCredentials();
	// 	}
	// }

	// login
	private void userCredentials () {
		loadingPanel.SetActive (true);
		if (PlayerPrefs.GetString ("loginas") == "") {
			//Debug.Log ("UserCre 1");
			loadingPanel.SetActive (false);
			loginPanel.SetActive (true);
		} else if (PlayerPrefs.GetString ("loginas") == "fb") {
			//Debug.Log ("UserCre 2");
			//RegisterwithFacebook ();
			RefreshFacebookLogin ();
		} else if (PlayerPrefs.GetString ("loginas") == "guest") {
			//RegisterAsGuest (PlayerPrefs.GetString("nickName"));
			AuthenticaticatinGuestUser ();
		}
	}

	public void OpenGuestLoginPanel () {
		guestLoginPanel.SetActive (true);
		loginPanel.SetActive (false);
	}

	public void LoginWithNickName () {
		if (nickText.text != "") {
			RegisterAsGuest (nickText.text);
		}
	}

	public void BackToLanguageSelect () {
		loadingPanel.SetActive (false);
		PlayerPrefs.SetInt ("language", 0);
		loginPanel.SetActive (false);
		getLanguage ();
	}

	public void BackToLoginSelect () {
		guestLoginPanel.SetActive (false);
		userCredentials ();
		//askAdverd();
	}

	public void RegisterwithFacebook () {
		loginPanel.SetActive (false);
		loadingPanel.SetActive (true);
		LoginManager.instance.RegisterAsFacebook (
			delegate (bool fbResult) {
				if (fbResult) {
					GetPlayerDetails ();
					//checkWordListVersion();
				} else {

				}
			},
			delegate (string error) {
				//sorun olustu

				PlayerPrefs.SetString ("loginas", "");
				userCredentials ();
				//askAdverd();
			}
		);
	}

	public void RefreshFacebookLogin () {
		loginPanel.SetActive (false);
		loadingPanel.SetActive (true);
		LoginManager.instance.RefreshFAcebookLogin (
			delegate (bool fbResult) {
				if (fbResult) {
					GetPlayerDetails ();
					//checkWordListVersion();
				} else {

				}
			},
			delegate (string error) {
				//sorun olustu
				Debug.Log ("SORUN VAR !!! 111");
				PlayerPrefs.SetString ("loginas", "");
				userCredentials ();
				//askAdverd();
			}
		);
	}

	private void checkWordListVersion () {
		//loacalde wordlist var mi?
		int wordsVersion = PlayerPrefs.GetInt ("wordsVersion");
		//localdeki wordlist versiyonu uygun mu?
		HTTPController.instance.CheckWordsVersion (
			delegate (string data) {

				JToken wordData = JToken.Parse (data);
				JObject obj = JObject.Parse (wordData["data"].ToString ());
				int v = (int) obj["version"];
				Debug.Log (wordsVersion + "versiyon" + v);
				if (wordsVersion == v && GameController.instance.player.PlayerLanguage == GameController.instance.player.PlayerBoardLanguage) {
					GetDate ();
				} else {
					//yeni data cek
					HTTPController.instance.GetWordsList (
						delegate (string data2) {

							//datayi dosyaya yaz
							string file = Application.persistentDataPath + "/wordList";
							if (File.Exists (file)) {
								File.Delete (file);
								PlayerPrefs.SetInt ("wordsVersion", 0);
							}

							var sr = File.CreateText (file);
							sr.Write (data2);
							sr.Close ();
							PlayerPrefs.SetInt ("wordsVersion", v);
							GetDate ();
						},
						delegate (string Error) {

						}
					);
				}
			},
			delegate (string Error) {
				//Error e gore hareket et
				checkWordListVersion ();
			}
		);
	}

	private void AuthenticaticatinGuestUser () {
		LoginManager.instance.AuthenticationGuest (
			delegate (bool guestResult) {
				if (guestResult) {
					GetPlayerDetails ();
				} else {
					//sorun olustu
				}
			},
			delegate (string error) {
				//sorun olustu
			}
		);
	}

	private void GetDate () {

		GameSparkManager.instance.getServerTime (
			delegate (LogEventResponse timeData) {
				string t = PlayerPrefs.GetString ("boards", "");
				ExtentionMethods.setTimeStamp (timeData);
				//	ExtentionMethods.CountdownToChallengeEnd();
				if (ExtentionMethods.checkDate (timeData) == t && GameController.instance.player.PlayerLanguage == GameController.instance.player.PlayerBoardLanguage) {
					string targetFile = File.ReadAllText (Application.persistentDataPath + "/boardsList");
					boardList = new BoardsList (targetFile);
					GetPlayerRecords ();
				} else {
					GameController.instance.player.PlayerBoardLanguage = GameController.instance.player.PlayerLanguage;
					PlayerPrefs.SetInt ("boardLang", GameController.instance.player.PlayerBoardLanguage);
					GetBoardsList (ExtentionMethods.checkDate (timeData));
				}
				//GetBoardsList (ExtentionMethods.checkDate(timeData));
			},
			delegate (string error) { });
	}

	private void GetPlayerRecords () {
		//GameSparkManager.instance.GetMyRank ();
		LoadUserGameData ();
	}

	private void SetMyRank (object data) {
		EventManager.StopListening (WFConstants.NotificationType.MyRankReceived, SetMyRank);
		GameController.instance.player.SetOldScore (data.ToString ());
		PlayerPrefs.SetInt ("oldScore", GameController.instance.player.oldScore);
		SaverLoader.instance.SaveData ();
		if (PlayerPrefs.GetInt ("howto", 0) == 0) {
			GameController.instance.GotoHowToScreen (gameObject);
		} else {
			//Destroy (howTo);
			GameController.instance.GotoMainScreen (gameObject);
		}
	}

	private void GetBoardsList (string boardDate) {
		HTTPController.instance.GetBoardsList (boardDate,
			delegate (string data) {
				string file = Application.persistentDataPath + "/boardsList";
				if (File.Exists (file)) {
					File.Delete (file);

				}

				var sr = File.CreateText (file);
				sr.Write (data);
				sr.Close ();
				PlayerPrefs.SetString ("boards", boardDate);
				boardList = new BoardsList (data);
				GetBoards (boardList);
			},
			delegate (string Error) {

			}
		);

	}

	private void GetBoards (BoardsList boardList) {
		totalBoard = boardList.list.Count;
		for (int i = 0; i < boardList.list.Count; i++) {
			loadSingleBoard (boardList.list[i].ToString (), i);
		}
	}

	private void loadSingleBoard (string boardName, int name) {
		HTTPController.instance.GetBoard (boardName,
			delegate (string data) {
				string file = Application.persistentDataPath + "/" + name;
				if (File.Exists (file)) {
					File.Delete (file);
				}

				var sr = File.CreateText (file);
				sr.Write (data);
				sr.Close ();
				totalBoard--;
				//Loading complete
				if (totalBoard == 0) {
					GetPlayerRecords ();
					//GameController.instance.GotoMainScreen(gameObject);
				}
			},
			delegate (string Error) {
				Debug.Log (Error);
			}
		);
	}

	public void SelectLanguage (int lang) {
		selectLanguagePanel.SetActive (false);
		loadingPanel.SetActive(true);
		GameController.instance.player.PlayerLanguage = lang;
		GameController.instance.player.PlayerBoardLanguage = lang;
		PlayerPrefs.SetInt ("language", lang);
		//askAdverd();
		userCredentials();
	}

	private string loadLanguageTexts () {
		TextAsset targetFile = Resources.Load<TextAsset> ("language");
		//Debug.Log ("dil yuklendi.........");
		return targetFile.text;
	}

	private void RegisterAsGuest (string nickName) {
		guestLoginPanel.SetActive (false);
		loginPanel.SetActive (false);
		loadingPanel.SetActive (true);
		LoginManager.instance.RegisterGuest (nickName,
			delegate (bool guestResult) {
				if (guestResult) {
					checkWordListVersion ();
				} else {
					//sorun olustu
				}
			},
			delegate (string error) {
				//sorun olustu
				if (error == "TAKEN") {
					AuthenticaticatinGuestUser ();
				}
			}
		);

	}

	private void GetPlayerDetails () {
		GameSparkManager.instance.GetPlayerDetails (
			delegate (bool result) {
				if (result) {
					checkWordListVersion ();
				} else {
					//sorun oldu
				}
			},
			delegate (string error) {

			});
	}

	public void SetVirtualGoods (object o) {
		EventManager.StartListening (WFConstants.NotificationType.VirtualGoodsReceived, SetVirtualGoods);
		GameController.instance.secondVirtualGoods = new SecondVirtualGoods (o as ListVirtualGoodsResponse);

	}

	private void noInternetConnection () {
				GameController.instance.player.PlayerLanguage = PlayerPrefs.GetInt ("language");
				GameController.instance.player.PlayerBoardLanguage = PlayerPrefs.GetInt ("boardLang", 0);
		LoadUserGameData ();
		if (PlayerPrefs.GetInt ("language", 0) == 0 || GameController.instance.player.seconds < 91) {
			PopupController.instance.OpenPopup ("internet1header", "internet2", "OK",
				delegate (bool b) {
					GameController.instance.QuitGame ();
				},
				//"quit",
				//delegate (bool b) {
				//   GameController.instance.QuitGame ();
				//}, 
				//"",
				//null
				"", null, "", null
			);
		} else {
			PopupController.instance.OpenPopup ("internet1header", "internet1", "OK",
				delegate (bool b) {
					string targetFile = File.ReadAllText (Application.persistentDataPath + "/boardsList");
					boardList = new BoardsList (targetFile);
					PopupController.instance.ClosePopup ("CommonPopup");
					GameController.instance.GotoMainScreen (gameObject);
				},
				"quit",
				delegate (bool b) {
					GameController.instance.QuitGame ();
				},
				"",
				null

			);
		}

	}

	public void AnswerAdverd(int answer) {
		PlayerPrefs.SetInt("ads", answer);
		checkGameVersion();
		PopupController.instance.ClosePopup("AdvertPopup");
	}

	private void LoadUserGameData () {
		SaverLoader.instance.LoadData ();
		if (ReachabilityManager.instance.IsReachable) {
			if (SaverLoader.instance.saveGameData.date == PlayerPrefs.GetString ("serverTime") && SaverLoader.instance.saveGameData.oldScore>0) {
				GameSparkManager.instance.setScore (SaverLoader.instance.saveGameData.oldScore, SaverLoader.instance.saveGameData.seconds,
					delegate (AroundMeLeaderboardResponse response) {
						SaverLoader.instance.SaveScore (0);
						GameSparkManager.instance.GetMyRank ();
						//GameController.instance.GotoMainScreen (gameObject);
					},
					delegate (string Error) {
						Debug.Log (Error);
						GameSparkManager.instance.GetMyRank ();
					});
			} else {
				SaverLoader.instance.SaveScore (0);
				GameSparkManager.instance.GetMyRank ();
			}
		}
	}
}