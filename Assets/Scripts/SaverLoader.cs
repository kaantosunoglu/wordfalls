﻿using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using tQsLib;
using UnityEngine;
public class SaverLoader : MonoBehaviour {

	public SaveGameData saveGameData;
	public static SaverLoader instance;
	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);
	}

	void start() {
		saveGameData = new SaveGameData();
	}
	public void SaveData() {
	//	Debug.Log(GameController.instance.player.oldScore);
		saveGameData.oldScore = GameController.instance.player.oldScore;
		saveGameData.playerName = GameController.instance.player.playerName;
		saveGameData.seconds = GameController.instance.player.seconds;
		saveGameData.stars = GameController.instance.player.stars;
		saveGameData.date = PlayerPrefs.GetString("serverTime");
		string jsonData = JsonUtility.ToJson(saveGameData);
	//	Debug.Log("+++++++  --->>>>>  "+jsonData);
		PlayerPrefs.SetString("SavedData", jsonData);
		PlayerPrefs.Save();

	}

	public void LoadData() {
		string jsonData = PlayerPrefs.GetString("SavedData", "");
	//	Debug.Log("LOAD SAVED DATA --->  "+jsonData);
		if(jsonData != "") {
			JsonUtility.FromJsonOverwrite(jsonData, saveGameData);
			//GameController.instance.player.oldScore = saveGameData.oldScore;
			GameController.instance.player.playerName = saveGameData.playerName;
			
			GameController.instance.player.stars = saveGameData.stars;
			if(GameController.instance.player.seconds == 0) {
				GameController.instance.player.seconds = saveGameData.seconds;
			}

		}
	}

	public void SaveScore(int score) {
		saveGameData.oldScore = score;
		saveGameData.playerName = GameController.instance.player.playerName;
		saveGameData.seconds = GameController.instance.player.seconds;
		saveGameData.stars = GameController.instance.player.stars;
		saveGameData.date = PlayerPrefs.GetString("serverTime");
		string jsonData = JsonUtility.ToJson(saveGameData);
		PlayerPrefs.SetString("SavedData", jsonData);
		PlayerPrefs.Save();
	}
}