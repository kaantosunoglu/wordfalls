﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using tQsLib;
using UnityEngine;
using UnityEngine.EventSystems;

public class GameController : MonoBehaviour {

	public Canvas UICanvas;
	public static GameController instance;
	public List<GameObject> GameScreens = new List<GameObject> ();

	public Player player;

	public LanguageTexts languageText;
	public SecondVirtualGoods secondVirtualGoods;
	public bool wasChangedLang = false;
	public bool firstEnd = true;
	public bool isRefreshingAccessToken = false;
	public bool isReachablityChanged = false;

private GameObject thisScreen;
	[HideInInspector] public object dailyReward;

	[HideInInspector] public GameObject LastScreen = null;

	void Awake () {
player = new Player ();
player.seconds = 0;
		if (instance == null) {
			instance = this;
			return;
		}
		Destroy (gameObject);

	}

	// Use this for initialization
	void Start () {
		Application.targetFrameRate = 60;
		EventManager.StartListening (WFConstants.NotificationType.DailyRewardReceived, DailyReward);
		EventManager.StartListening (WFConstants.NotificationType.ServerError, ServerError);
		EventManager.StartListening (WFConstants.NotificationType.ReachibilityChanged, ReachibilityChanged);

		LastScreen = SpawnUIScreen ("LoaderScreen");
		
		//	player.PlayerBoardLanguage = PlayerPrefs.GetInt("language", 0);
		firstEnd = true;

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Escape) && player.seconds > 90) {
			OnBackButtonPressed ();
		}
	}

	public GameObject SpawnUIScreen (string name) {
		thisScreen = null;

		thisScreen = GameScreens.Where (obj => obj.name == name).SingleOrDefault ();

		if (thisScreen == null) {
			thisScreen = (GameObject) Instantiate (Resources.Load ("Prefabs/UIScreens/" + name.ToString ()));
			thisScreen.name = name;
			thisScreen.transform.SetParent (UICanvas.transform);
			thisScreen.transform.localPosition = Vector3.zero;
			thisScreen.transform.localScale = Vector3.one;
			thisScreen.GetComponent<RectTransform> ().sizeDelta = Vector3.zero;
		}
		//ßthisScreen.Init ();

		thisScreen.OnWindowLoad ();
		thisScreen.SetActive (true);
		LastScreen = thisScreen;
		return thisScreen;
	}

	GameObject GetUIScreen (string name) {
		thisScreen = null;
		thisScreen = GameScreens.Where (obj => obj.name == name).SingleOrDefault ();
		return thisScreen;
	}

	public void OnBackButtonPressed () {
		if (LastScreen.name == "MainScreen") {
			//SpawnUIScreen ("QuitConfirm");
			PopupController.instance.OpenPopup ("confirmQuitHeader", "confirmQuit", "quit",
			delegate (bool b) {
				GameController.instance.QuitGame ();
			}, 
			"cancel",
			delegate(bool b) {
				PopupController.instance.ClosePopup("CommonPopup");
			}, "", null );
		} else if (LastScreen.name == "GameScreen") {
			//LastScreen.OnWindowRemove ();
			LastScreen.SetActive (false);
			firstEnd = true;
			SpawnUIScreen ("MainScreen");
		} else if (LastScreen.name == "SettingsScreen") {
			LastScreen.SetActive (false);
			SpawnUIScreen ("MainScreen");
		}
		/*else if (LastScreen.name == "GamePlay") {
			PauseGame ();
		} else if (LastScreen.name == "Pause") {
			LastScreen.OnWindowRemove ();
			LastScreen = GetUIScreen ("GamePlay");
		} */
	}

	public void LoadAgain (GameObject currentScreen) {
		currentScreen.SetActive (false);
		SpawnUIScreen ("LoaderScreen");

	}

	public void GotoMainScreen (GameObject currentScreen) {
		currentScreen.SetActive (false);
		SpawnUIScreen ("MainScreen");
	}

	public void GotoHowToScreen (GameObject currentScreen) {
		currentScreen.SetActive (false);
		SpawnUIScreen ("HowToScreen");
	}

	public void StartGamePlay (GameObject currentScreen) {
		currentScreen.SetActive (false);
		SpawnUIScreen ("GameScreen");

	}

	public void OpenSettings (GameObject currentScreen) {
		currentScreen.SetActive (false);
		SpawnUIScreen ("SettingsScreen");
	}

	public void ShowInternetConnectionAlert () { }

	public void OpenMarket () {
#if UNITY_ANDROID
		Application.OpenURL ("market://details?id=com.thequixoticssolutions.wordfalls");
#elif UNITY_IPHONE
		Application.OpenURL ("itms-apps://itunes.apple.com/app/com.thequixoticssolutions.wordfalls");
#endif
		PopupController.instance.ClosePopup("CommonPopup");
	}

	public void QuitGame () {
		Application.Quit ();
	}

	private void DailyReward (object obj) {
		dailyReward = obj;
	}

	private void ServerError(object o) {
		PopupController.instance.OpenPopup("", "serverError", "", null, "", null, "", null);
	}

	private void ReachibilityChanged(object obj) {
		
		isReachablityChanged = true;
		if(ReachabilityManager.instance.IsReachable) {
			if(thisScreen.name == "MainScreen") {
				isReachablityChanged = false;
				LoadAgain(thisScreen);
			}
		}else {
			
		}
	}

}