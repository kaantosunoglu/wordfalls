﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UTNotifications;
using System;
using UnityEngine.UI; 

namespace UTNotifications
{
    public class PNManager : MonoBehaviour
    {

        /*	public static PNManager instance;
            private UTNotifications.Manager notificationsManager;
            private bool isInitialized;
            string deviceToken;

            public Text txt;

            // Use this for initialization
            void Start () {

                notificationsManager = UTNotifications.Manager.Instance;
                if (PlayerPrefs.HasKey("isPMInitialized")) {
                    isInitialized = Convert.ToBoolean(PlayerPrefs.GetInt("isPMInitialized"));
                } else {
                    isInitialized = false;
                }

                if (notificationsManager != null && isInitialized) {
                    notificationsManager.Initialize(true, 0, true);
                    notificationsManager.SetBadge(0);
                }
            }

            // Update is called once per frame
            void Update () {

            }

            void Awake() {
                if (instance == null) {
                    instance = this;
                    return;
                }
                Destroy (gameObject);
            }

            void OnApplicationPause( bool pauseStatus )	{
                if (!pauseStatus && isInitialized) {
                    notificationsManager.SetBadge(0);
                }
            }

            public void WakeUp() {
                Debug.Log("------------------    WakeUp 1");
                notificationsManager = UTNotifications.Manager.Instance;
                notificationsManager.OnSendRegistrationId += (providerName, registrationId) => {
                    Debug.Log("------------------    WakeUp 2 " + registrationId);
                    RegisterDeviceToken(providerName, registrationId);
                };
                notificationsManager.OnNotificationClicked += (notification) => {
        //            LaunchManager.Instance.HandleClickedNotification(notification);
                };
                notificationsManager.OnNotificationsReceived += (receivedNotifications) => {
                    Debug.Log("------------------    WakeUp 3");
                    notificationsManager.SetBadge(0);
        //            LaunchManager.Instance.HandleRemoteNotification(receivedNotifications[receivedNotifications.Count - 1]);
                };
                Debug.Log("------------------    WakeUp 4");
            }

            private void RegisterDeviceToken(string providerName, string receivedDeviceToken) {
                Debug.Log("------------------    RegisterDeviceToken");
                //May be "APNS", "FCM", "ADM" or "WNS"
                txt.text = receivedDeviceToken;
                isInitialized = true;
                PlayerPrefs.SetInt("isPMInitialized", 1);
                deviceToken = receivedDeviceToken;
                if (providerName == "APNS") {
                   // Register device token GameSparks
                } else if (providerName == "FCM") {
                    // Register device token GameSparks

                } else if (providerName == "ADM") {
                    ; ;
                } else if (providerName == "WNS") {
                    ; ;
                }
            }
        */

        public static PNManager instance;
		private bool m_notificationsEnabled;
		//public Text txt;

		public void Start()
        {
            //Please see the API Reference for the detailed information: http://universal-tools.github.io/UTNotifications/html/annotated.html

            Manager notificationsManager = Manager.Instance;
            
            notificationsManager.OnSendRegistrationId += SendRegistrationId;
           // notificationsManager.OnNotificationClicked += OnNotificationClicked;
           // notificationsManager.OnNotificationsReceived += OnNotificationsReceived;    //Let's handle incoming notifications (not only push ones)

           
                Initialize();
            
        }

        void Awake()
        {
            if (instance == null)
            {
                instance = this;
                return;
            }
            Destroy(gameObject);
        }

		protected void Initialize()
        {
            //We would like to handle incoming notifications and don't want to increment the push notifications id
            Manager notificationsManager = Manager.Instance;
            bool result = notificationsManager.Initialize(true, 0, false);
            Debug.Log("UTNotifications Initialize: " + result);

            m_notificationsEnabled = notificationsManager.NotificationsEnabled();
            notificationsManager.SetBadge(0);
            
        }

       /// <summary>
        /// A wrapper for the <c>SendRegistrationId(string userId, string providerName, string registrationId)</c> coroutine
        /// </summary>
        protected void SendRegistrationId(string providerName, string registrationId)
        {
			Debug.Log("registrationId : " + registrationId);
			//txt.text = registrationId;
           // string userId = GenerateDeviceUniqueIdentifier();
           // StartCoroutine(SendRegistrationId(userId, providerName, registrationId));
		   //Game Spark refister push
			string deviceOS;
			#if UNITY_IOS 
				deviceOS = "IOS";
			#elif UNITY_ANDROID
				deviceOS = "ANDROID";
			#endif
		   GameSparkManager.instance.PushRegisterRequest(deviceOS, registrationId);
        }

        private void CreateScheduledNotifications() {
            string messageTitle = "WORD FALLS";
            string messageBody = LanguageManager.getText ("dailyReward");
            Manager.Instance.ScheduleNotification((30 * 60 * 60), messageTitle, messageBody, WFConstants.ScheduledNotificationReward, null, "demo_notification_profile", 1);
        }

        /// <summary>
        /// Cancels the previously created repeating scheduled notification.
        /// </summary>
        public void CancelRepeatingScheduledNotification()
        {
            Manager.Instance.CancelNotification(WFConstants.ScheduledNotificationReward);
            CreateScheduledNotifications();
        }

    }
}
