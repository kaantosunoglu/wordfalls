﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Linq;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using GameSparks.Api.Responses;
using GameSparks.Api.Requests;
using GameSparks.Core;
using AppodealAds.Unity.Api;


namespace tQsLib
{
    public class Models
    {
    }

    public class Tile
    {
        public int i { get; set; }
        public int j { get; set; }
        public string letter { get; set; }
        public bool isJoker { get; set; }
        public bool isMultiplier { get; set; }
        public bool isStar { get; set; }
        public string code { get; set; }
    }


    public class Words
    {
        public Dictionary<string, Dictionary<string, JArray>> wordList = new Dictionary<string, Dictionary<string, JArray>>();

        public Words(string data)
        {
            JToken wordData = JToken.Parse(data);
            JObject obj = JObject.Parse(wordData["data"].ToString());

            List<string> keys = obj.Properties().Select(p => p.Name).ToList();
            foreach (string k in keys)
            {
                JObject objW = JObject.Parse(obj[k].ToString());
                Dictionary<string, JArray> dd = new Dictionary<string, JArray>();
                List<string> wKeys = objW.Properties().Select(q => q.Name).ToList();
                foreach (string l in wKeys)
                {
                    JArray h = objW.Value<JArray>(l);

                    dd[l] = h;
                    //wordList[k][l]
                }


                //	Debug.Log (k);
                wordList[k] = dd;
            }
        }
    }

    public class BoardsList
    {

        public JArray list;

        public BoardsList(string data)
        {
            JToken boardsListData = JToken.Parse(data);
            list = boardsListData.Value<JArray>("boards");
        }
    }

    public class Board
    {
        public Dictionary<string, List<ArrayList>> board = new Dictionary<string, List<ArrayList>>();

        public Board(string data)
        {
            JToken boardData = JToken.Parse(data);
            JObject obj = JObject.Parse(boardData["data"].ToString());

            List<string> keys = obj.Properties().Select(p => p.Name).ToList();
            foreach (string k in keys)
            {
                //JObject objB = JObject.Parse (obj[k].ToString());
                JArray bKeys = obj.Value<JArray>(k);
                List<ArrayList> cList = new List<ArrayList>();
                for (int i = 0; i < bKeys.Count; i++)
                {
                    ArrayList ba = new ArrayList();
                    JArray ja = bKeys[i].Value<JArray>();
                    for (int j = 0; j < ja.Count; j++)
                    {
                        ba.Add(ja[j]);
                    }
                    cList.Add(ba);
                }
                board.Add(k, cList);
            }

        }
    }

    public class Player
    {
        public int PlayerLanguage { get; set; }
        public int PlayerBoardLanguage {get; set; }
        public string playerName { get; set; }
        public int oldScore { get; set; }
        public long timeStamp { get; set; }
        public bool newPlayer { get; set; }
        public string playerID { get; set; }
        public int seconds { get; set; }
        public string facebookID {get; set;}
        public int userAge {get; set;}
        public UserSettings.Gender userGender {get; set;}

        public int stars { get; set; }

        public StarLetters starLetters { get; set; }

        public void SetStars(string data)
        {
            JToken s = JToken.Parse(data);
            stars = System.Int32.Parse(s["STAR"].ToString());
            //stars = 0;
        }

        public void SetSeconds(string data)
        {
            JToken s = JToken.Parse(data);
            seconds = System.Int32.Parse(s["TOTAL_SECONDS"].ToString());
            Debug.Log("SECONDS    " + seconds);
        }

        public void SetLetters(string data)
        {
            starLetters = new StarLetters(data);
        }

        public void SetOldScore(string data)
        {


            JToken s = JToken.Parse(data);

            string leaderboardCodes = "SCOREBOARD_" + (GameController.instance.player.PlayerLanguage == 1 ? "tr" : "en") + ".DAY." + PlayerPrefs.GetString("serverTime") + "000";
           

            if (s[leaderboardCodes] != null && s[leaderboardCodes]["SCORE"] != null)
            {
                string highScore = s[leaderboardCodes]["SCORE"].ToString();
                oldScore = System.Int32.Parse(highScore);
                PlayerPrefs.SetInt("highScore", GameController.instance.player.oldScore);
            }
            else
            {
                oldScore = 0;
                PlayerPrefs.SetInt("highScore", 0);
            }

        }

        public void SetFacebookParams(string data)
        {
            JToken s = JToken.Parse(data);
            facebookID =  s["id"].ToString();
            
                //string bd =  s["birthday"].ToString();
                //String sDate = DateTime.Now.ToString();
                //DateTime datevalue = (Convert.ToDateTime(sDate.ToString()));
                //int yy = datevalue.Year;
                //userAge = yy - Int32.Parse(bd.Substring(bd.LastIndexOf("/")+1, bd.Length-1));       
                //if(s["gender"].ToString() == "male") {
                //    userGender = UserSettings.Gender.MALE;
                //} else if (s["gender"].ToString() == "female") {
                //    userGender = UserSettings.Gender.FEMALE;
                //} else {
                //    userGender = UserSettings.Gender.OTHER;
                //}
         
        }

    }


    public class StarLetters
    {
        public string date { get; set; }
        public List<string> letters = new List<string>();

        public StarLetters(string data)
        {

            JToken jt = JToken.Parse(data);
            JToken lettersData = JToken.Parse(jt["starLetters"].ToString());
            Debug.Log("---------->>> " + lettersData.ToString());
            date = lettersData["day"].ToString();
            JArray jl = lettersData.Value<JArray>("letters");

            if (date == PlayerPrefs.GetString("serverTime"))
            {

                for (int i = 0; i < jl.Count; i++)
                {
                    letters.Add(jl[i].ToString());
                }

                //letters = lettersData.Value<JArray> ("letters");
            }
            else
            {
                date = PlayerPrefs.GetString("serverTime");
                letters = new List<string>();
            }
        }

    }

    public class LanguageTexts
    {
        public Dictionary<string, Dictionary<string, string>> text = new Dictionary<string, Dictionary<string, string>>();

        public LanguageTexts(string data)
        {
            JToken languageData = JToken.Parse(data);
            JObject obj = JObject.Parse(languageData["data"].ToString());


            List<string> keys = obj.Properties().Select(p => p.Name).ToList();
            foreach (string k in keys)
            {
                JObject objtext = JObject.Parse(obj[k].ToString());
                Dictionary<string, string> dd = new Dictionary<string, string>();
                List<string> wKeys = objtext.Properties().Select(q => q.Name).ToList();
                foreach (string l in wKeys)
                {
                    //Debug.Log (objtext [l].ToString ());
                    //JObject h = JObject.Parse (objtext [l].ToString ());

                    dd[l] = objtext[l].ToString();
                    // Debug.Log(dd[l]);
                }
                // Debug.Log(k);
                text[k] = dd;

            }
        }
    }

    public class userCredentials
    {
        public string cr1 { get; set; }
        public string cr2 { get; set; }

    }

    public class LeaderBoardPlayers
    {

        public List<LeaderBoardPlayer> LeaderBoardRanks = new List<LeaderBoardPlayer>();
        public List<LeaderBoardPlayer> FirstThreeRanks = new List<LeaderBoardPlayer>();
        public LeaderBoardPlayer me = new LeaderBoardPlayer();

        public LeaderBoardPlayers(LeaderboardDataResponse data)
        {
            if(data != null) {
              foreach (LeaderboardDataResponse._LeaderboardData entry in data.Data)
            {
                LeaderBoardPlayer lbu = new LeaderBoardPlayer();
				lbu.playerId = entry.UserId;
                lbu.playerCountry = entry.Country;
                lbu.playerName = entry.UserName;
                lbu.playerRank = (int)entry.Rank;
                lbu.playerScore = System.Int32.Parse(entry.JSONData["SCORE"].ToString());
                lbu.playerTime = System.Int32.Parse(entry.JSONData["TIME"].ToString());
                
                 JObject externalIds = JObject.Parse(entry.ExternalIds.JSON);
                //Debug.Log(externalIds["FB"].ToString() + " ooooooo    " + externalIds);

                lbu.playerFacebookId = externalIds["FB"] == null ? null : externalIds["FB"].ToString();
                LeaderBoardRanks.Add(lbu);

                if (lbu.playerId == GameController.instance.player.playerID)
				{
					me = lbu;
				}


            }
            

            foreach (LeaderboardDataResponse._LeaderboardData entry in data.First)
            {
                LeaderBoardPlayer lbu = new LeaderBoardPlayer();
                lbu.playerCountry = entry.Country;
                lbu.playerName = entry.UserName;
                lbu.playerRank = (int)entry.Rank;
                lbu.playerScore = System.Int32.Parse(entry.JSONData["SCORE"].ToString());
                lbu.playerTime = System.Int32.Parse(entry.JSONData["TIME"].ToString());

                FirstThreeRanks.Add(lbu);
            }
            }
        }
        public LeaderBoardPlayers(AroundMeLeaderboardResponse data)
        {
            if(data != null) {
            foreach (AroundMeLeaderboardResponse._LeaderboardData entry in data.Data)
            {
                LeaderBoardPlayer lbu = new LeaderBoardPlayer();
				lbu.playerId = entry.UserId;
                lbu.playerCountry = entry.Country;
                lbu.playerName = entry.UserName;
                lbu.playerRank = (int)entry.Rank;
                lbu.playerScore = System.Int32.Parse(entry.JSONData["SCORE"].ToString());
                lbu.playerTime = System.Int32.Parse(entry.JSONData["TIME"].ToString());
                
                 JObject externalIds = JObject.Parse(entry.ExternalIds.JSON);
                //Debug.Log(externalIds["FB"].ToString() + " ooooooo    " + externalIds);

                lbu.playerFacebookId = externalIds["FB"] == null ? null : externalIds["FB"].ToString();
                LeaderBoardRanks.Add(lbu);

                if (lbu.playerId == GameController.instance.player.playerID)
				{
					me = lbu;
				}


            }
            

            foreach (AroundMeLeaderboardResponse._LeaderboardData entry in data.First)
            {
                LeaderBoardPlayer lbu = new LeaderBoardPlayer();
                lbu.playerCountry = entry.Country;
                lbu.playerName = entry.UserName;
                lbu.playerRank = (int)entry.Rank;
                lbu.playerScore = System.Int32.Parse(entry.JSONData["SCORE"].ToString());
                lbu.playerTime = System.Int32.Parse(entry.JSONData["TIME"].ToString());

                FirstThreeRanks.Add(lbu);
            }
            } else {

            }

        }

    }

    public class LeaderBoardPlayer
    {
        public string playerName { get; set; }
        public int playerRank { get; set; }
        public int playerScore { get; set; }
        public int playerTime { get; set; }
        public string playerCountry { get; set; }
        public string playerId { get; set; }
        public string playerFacebookId { get; set; }
    }

    public class letterList
    {
        public List<string> list { get; set; }
    }

    public class Achievements
    {
        public Dictionary<string, int> list = new Dictionary<string, int>();
        public Achievements(string data){

        }
    }

    public class SecondVirtualGoods
    {
        public Dictionary<string, int> secondVirtualGoods = new Dictionary<string, int>();
        public SecondVirtualGoods(ListVirtualGoodsResponse data) {
            GSEnumerable<ListVirtualGoodsResponse._VirtualGood> virtualGoods = data.VirtualGoods;
            foreach (var v in virtualGoods)
            {
                    secondVirtualGoods.Add(v.ShortCode.ToString(), (int)v.CurrencyCosts.GetInt("STAR"));
            }
        }
    }
}