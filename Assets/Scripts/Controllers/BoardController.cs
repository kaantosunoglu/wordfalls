﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using GameSparks.Api.Responses;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using tQsLib;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BoardController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler {

	public static BoardController instance;

	public GameObject letterTile;
	public GameObject againPanel;
	public LeaderboardController lbc;

	// image
	public bool forJoker = true;
	public bool forStar = true;
	public bool forMultiplier = true;

	//data
	private Words wordList;
	private List<Board> boards;
	private List<int[]> boardMap;

	private bool isMouseDown = false;
	private Coroutine drawing;
	private Rect touchRect;
	private Vector2 testHitVector;
	private Dictionary<int, LetterTile[]> boardTiles;
	private List<LetterTile> selectedLetterTiles;
	private LetterTile lastSelectedTile;

	//timer
	private float totalTime;
	private float currentTime;
	private bool isTimerActive;

	private int totalScore;
	private int totalJoker;
	private bool firstRun = true;
	private Dictionary<int, int[]> stockBoardDataCoordinates;
	private long pauseTime;
	private long focusTime;

	private LeaderBoardPlayers globalLeaderboardPlayers;
	private LeaderBoardPlayers facebookFriendsLeaderboardPlayers;

	void Awake () {
		if (instance == null) {
			instance = this;
			return;
		}
	}

	// Use this for initialization
	void Start () {
		//startGame ();
		touchRect = new Rect (new Vector2 (0, 0), new Vector2 (150, 150));
	}

	// Update is called once per frame
	void Update () {

		if (isTimerActive) {
			if (currentTime > 0) {
				currentTime -= Time.deltaTime;
				//InfoPanelController.instance.setTimeText (((int)currentTime).ToString());
				InfoPanelController.instance.setTimeText ((int)(Math.Ceiling (currentTime)));
				//timerImage.fillAmount = currentTime / totalTime;
			} else {
				isTimerActive = false;
				InfoPanelController.instance.setTimeText (0);
				if (isMouseDown) {
					StopCoroutine (drawing);
				}
				Debug.Log ("Olasilik 3");
				checkWord ();
				sendScore ();
				//timerImage.fillAmount = 0;
			}
		} else {
			//timerImage.gameObject.SetActive(false);
		}
	}

	void OnApplicationPause (bool pauseStatus) {
		var epochStart = new System.DateTime (1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc);

		if (pauseStatus) {
			pauseTime = (long) (System.DateTime.UtcNow - epochStart).TotalMilliseconds;
		} else {
			focusTime = (long) (System.DateTime.UtcNow - epochStart).TotalMilliseconds;
			currentTime -= ((focusTime - pauseTime) / 1000);
		}
	}

	void OnEnable () {
		Debug.Log ("***********VISIBLE");
		PopupController.instance.OpenPopup ("LoadingPopup");
		EventManager.StartListening (WFConstants.NotificationType.ExtraTimeReceived, setExtraTime);
		//RestartGame (false);
		GetUsedStarLetters ();
	}
	void OnDisable () {
		Debug.Log ("*************INVISIBLE");
		wordList = null;
		isTimerActive = false;
		clearBoard ();
		againPanel.SetActive (false);
		EventManager.StopListening (WFConstants.NotificationType.ExtraTimeReceived, setExtraTime);
	}

	private void prepareGame () {
		Debug.Log("BoardController 1"); 
		//check date !!!!
		totalScore = 0;
		InfoPanelController.instance.setScore (totalScore);
		if (wordList == null || GameController.instance.player.PlayerLanguage != GameController.instance.player.PlayerBoardLanguage) {
			wordList = new Words (getDataFromLocal ("wordList"));
			GameController.instance.player.PlayerBoardLanguage = GameController.instance.player.PlayerLanguage;
		}
Debug.Log("BoardController 2"); 
		boards = new List<Board> ();
		boardMap = new List<int[]> ();

		int boardListCount = Loader.instance.boardList.list.Count;
		for (int i = 0; i < boardListCount; i++) {
			Debug.Log("BoardController 2"); 
			string boardData = getDataFromLocal (i.ToString ());
			Board b = new Board (boardData);
			boards.Add (b);
		}
Debug.Log("BoardController 3"); 
		for (int i = 0; i < WFConstants.boardWidth; i++) {
			boardMap.Add (new int[2] { 0, 0 });
		}

		PopupController.instance.ClosePopup ("LoadingPopup");
		startGame ();
		//Debug.Log ("-----kaan");
	}

	public void startGame () {
		GameAnalyticsManager.instance.trackFunnel ("Funnel:StartGame");

		firstRun = false;
		totalJoker = 0;
		selectedLetterTiles = new List<LetterTile> ();
		boardTiles = new Dictionary<int, LetterTile[]> ();
		for (int i = 0; i < WFConstants.boardWidth; i++) {
			LetterTile[] tiles = new LetterTile[WFConstants.boardHeight];
			for (int j = 0; j < WFConstants.boardHeight; j++) {
				GameObject hede = Instantiate (letterTile, new Vector2 (0, 0), Quaternion.identity);
				hede.transform.SetParent (transform, false);

				Tile t = new Tile ();
				t.i = i;
				t.j = j;

				Board b = boards[(int) boardMap[i][0]];
				List<ArrayList> arrL = b.board[i.ToString ()];
				ArrayList arr = arrL[boardMap[i][1]];
				//ArrayList srr2 = arr [(int)boardMap [i].y];
				t.letter = arr[0].ToString (); //boards [boardMap [i].x.ToString()].board ["b"+i.ToString()] [boardMap [i].y];
				//t.isJoker = forJoker ? System.Convert.ToInt32(arr[1]) == 1 : false;
				t.isJoker = System.Convert.ToInt32 (arr[1]) == 1;
				t.isMultiplier = forMultiplier ? System.Convert.ToInt32 (arr[2]) == 1 : false;
				t.isStar = forStar ? System.Convert.ToInt32 (arr[3]) == 1 : false;
				t.code = boardMap[i][0] + "" + i.ToString () + "" + j.ToString ();
				boardMap[i][1]++;
				Debug.Log (boardMap[i][1]);
				if (boardMap[i][1] == WFConstants.boardHeight) {
					boardMap[i][0]++;
					boardMap[i][1] = 0;
					//Vector2 v2 = new Vector2 (boardMap [i][0]++, 0);
					//boardMap [i] = v2;
				}
				//t.letter = i.ToString () + j.ToString ();

				hede.transform.GetComponent<LetterTile> ().setData (t);
				tiles[j] = hede.transform.GetComponent<LetterTile> ();
			}
			boardTiles.Add (i, tiles);
		}

		StartCoroutine (startTimer ());

	}

	private IEnumerator startTimer () {
		totalTime = GameController.instance.player.seconds;
		//long updatedAt = gvm.Table.UpdatedAt;
		//TimeSpan currentDateTime = DateTime.Now - new DateTime(1970, 1, 1);
		currentTime = totalTime; // - (float)currentDateTime.TotalSeconds;
		InfoPanelController.instance.setTimeText (((int) currentTime));

		yield return new WaitForSeconds (1);

		isTimerActive = true;
	}

	public void RestartGame (bool willReset) {
		PopupController.instance.OpenPopup ("LoadingPopup");
		if (!firstRun) {
			clearBoard ();
		}
		againPanel.SetActive (false);

		if (!willReset) {
			prepareGame ();
		} else {
			GameController.instance.LoadAgain (gameObject);
		}
	}

	private void clearBoard () {
		if (boardTiles.Count > 0) {
			for (int i = 0; i < WFConstants.boardWidth; i++) {
				LetterTile[] tilez = boardTiles[i];
				for (int j = 0; j < WFConstants.boardHeight; j++) {
					tilez[j].destroy ();
				}
			}
			boardTiles.Clear ();
		}

	}

	private void SelectLetter (Vector2 pointerPosition) {

		int selectedLetterTilesCount = selectedLetterTiles.Count;

		testHitVector = TestHit (pointerPosition);
		//Debug.Log (testHitVector.x +" || "+ (int)testHitVector.y );
		if ((int) testHitVector.x < 0 || (int) testHitVector.y < 0)
			return;
		if (lastSelectedTile && (testHitVector.x == lastSelectedTile.settings.i && testHitVector.y == lastSelectedTile.settings.j)) {
			return;
		}

		LetterTile lt = (LetterTile) boardTiles[(int) testHitVector.x][(int) testHitVector.y];
		int indexOfNewTile = selectedLetterTiles.FindIndex (d => d == lt);
		if (selectedLetterTiles != null && indexOfNewTile > -1 && indexOfNewTile != selectedLetterTilesCount - 2)
			return;
		if (selectedLetterTilesCount > 15)
			return;

		if ((lt.settings.isJoker && indexOfNewTile != selectedLetterTilesCount - 2) && totalJoker == 2)
			return;

		if (selectedLetterTilesCount > 1 && indexOfNewTile == selectedLetterTilesCount - 2) {
			lastSelectedTile.setUnselect ();
			lastSelectedTile = selectedLetterTiles[selectedLetterTilesCount - 2];
			selectedLetterTiles.RemoveAt (selectedLetterTilesCount - 1);
			if (lt.settings.isJoker) {
				totalJoker--;
			}
		} else {
			lastSelectedTile = lt;
			lastSelectedTile.setSelected ();
			selectedLetterTiles.Add (lt);
			if (lt.settings.isJoker) {
				totalJoker++;
			}
		}

		if (selectedLetterTiles.Count > 0) {
			WordPanelController.instance.setWord (selectedLetterTiles);
		} else {
			WordPanelController.instance.clear ();
		}
	}

	private void checkWord () {
		totalJoker = 0;
		WordPanelController.instance.clear ();
		if (isTimerActive) {
			if (selectedLetterTiles.Count < 2) {
				//Debug.Log (selectedLetterTiles.Count);
				foreach (var item in selectedLetterTiles) {
					item.setUnselect ();
				}
				//lastSelectedTile = new LetterTile ();
				lastSelectedTile = null;
				selectedLetterTiles.Clear ();
				return;
			} else {
				string selectedWord = "";
				//kelimeyi bul
				foreach (LetterTile lt in selectedLetterTiles) {
					selectedWord += lt.settings.isJoker ? " " : lt.settings.letter;
				}

				if (isWordInThelist (selectedWord)) {
					List<string> starLettersList = new List<string> ();

					int point = 0;
					int star = 0;
					int multi = 1;

					foreach (LetterTile lt in selectedLetterTiles) {
						//point += lt.settings.isJoker ? 0 : (GameController.instance.player.PlayerLanguage == 1 ? WFConstants.trLettersScores[lt.settings.letter] : WFConstants.enLettersScores[lt.settings.letter]);
						point += lt.point;
						if (lt.isStar) {
							Debug.Log ("STAR  " + lt.settings.code);
							starLettersList.Add (lt.settings.code);
							star++;
						}
						if (lt.isMultiplier) {
							multi = multi * 2;
						}
						boardTiles[lt.settings.i][lt.settings.j] = null;

					}

					point = point * selectedWord.Length * multi;
					totalScore += point;
					InfoPanelController.instance.setScore (totalScore);
					if (star > 0) {
						sendLetterStar (starLettersList);
					}
				} else {
					foreach (var item in selectedLetterTiles) {
						item.setUnselect ();
					}
					selectedLetterTiles.Clear ();
				}

				reGenareteBoard ();
				//lastSelectedTile = new LetterTile ();
				lastSelectedTile = null;
			}
		} else {
			foreach (var item in selectedLetterTiles) {
				item.setUnselect ();
			}
			lastSelectedTile = null;
			selectedLetterTiles.Clear ();
		}

	}

	private void reGenareteBoard () {
		List<int> columns = findColumnsOfSelectedTiles ();

		foreach (int c in columns) {
			int emptyTile = 0;
			for (int n = 0; n < WFConstants.boardHeight; n++) {
				if (boardTiles[c][n] == null) {
					//Debug.Log ("empty tile " + c + "[]" + n);
					emptyTile++;
				} else {
					if (emptyTile > 0) {
						Tile t = new Tile ();
						t.i = boardTiles[c][n].settings.i;
						t.j = boardTiles[c][n].settings.j - emptyTile;
						t.letter = boardTiles[c][n].settings.letter;
						t.isJoker = boardTiles[c][n].settings.isJoker;
						t.isStar = boardTiles[c][n].isStar;
						t.isMultiplier = boardTiles[c][n].settings.isMultiplier;
						t.code = boardTiles[c][n].settings.code;
						boardTiles[c][n].setData (t);
						boardTiles[c][n - emptyTile] = boardTiles[c][n];
						boardTiles[c][n] = null;
					}
				}
			}

			for (int n = WFConstants.boardHeight - emptyTile; n < WFConstants.boardHeight; n++) {
				//Debug.Log(c+" --------- " +n);
				boardTiles[c][n] = selectedLetterTiles[0];
				Tile t = new Tile ();
				t.i = c;
				t.j = n;

				Board b = boards[(int) boardMap[c][0]];
				List<ArrayList> arrL = b.board[c.ToString ()];
				ArrayList arr = arrL[boardMap[c][1]];
				//ArrayList srr2 = arr [(int)boardMap [i].y];
				t.letter = arr[0].ToString (); //boards [boardMap [i].x.ToString()].board ["b"+i.ToString()] [boardMap [i].y];
				t.code = boardMap[c][0].ToString () + "" + c + "" + boardMap[c][1];

				t.isJoker = System.Convert.ToInt32 (arr[1]) == 1;
				t.isMultiplier = System.Convert.ToInt32 (arr[2]) == 1;
				t.isStar = System.Convert.ToInt32 (arr[3]) == 1;
				boardMap[c][1]++;
				Debug.Log (boardMap[c][1]);
				if (boardMap[c][1] == WFConstants.boardHeight) {
					boardMap[c][0]++;
					boardMap[c][1] = 0;
					//Vector2 v2 = new Vector2 (boardMap [i][0]++, 0);
					//boardMap [i] = v2;
				}

				//t.letter = "N"+n;
				boardTiles[c][n].setData (t);
				selectedLetterTiles.RemoveAt (0);
				//Debug.Log ("selectedLetterTiles.Count " + selectedLetterTiles.Count);
			}

		}

		selectedLetterTiles.Clear ();
	}

	private List<int> findColumnsOfSelectedTiles () {
		List<int> columns = new List<int> ();

		foreach (LetterTile lt in selectedLetterTiles) {
			if (columns.IndexOf (lt.settings.i) < 0) {
				columns.Add (lt.settings.i);
			}
		}

		return columns;

	}

	private bool isWordInThelist (string word) {
		string firstChar = word[0].ToString ();
		int wordLength = word.Length;
		string fakeWord = word;

		if (word.IndexOf (" ") > -1) {
			int lettersCount = (GameController.instance.player.PlayerLanguage == 1 ? WFConstants.trLetters.Count : WFConstants.enLetters.Count);
			for (var i = 0; i < lettersCount; i++) {
				fakeWord = fakeWord.Remove (word.IndexOf (" "), 1);
				fakeWord = fakeWord.Insert (word.IndexOf (" "), (GameController.instance.player.PlayerLanguage == 1 ? WFConstants.trLetters[i] : WFConstants.enLetters[i]));
				//				fakeWord [word.IndexOf (" ")] = WFConstants.trLetters [i];
				if (isWordInThelist (fakeWord)) {
					return true;
				}
			}

		} else {

			if (!wordList.wordList.ContainsKey (firstChar)) {
				return false;
			}
			Dictionary<string, JArray> wordListofFirstChar = (Dictionary<string, JArray>) wordList.wordList[firstChar];
			if (!wordListofFirstChar.ContainsKey (wordLength.ToString ())) {
				return false;
			}
			JArray words = wordListofFirstChar[wordLength.ToString ()];
			string[] ws = words.ToObject<string[]> ();
			if (ws.Contains (fakeWord)) {
				return true;
			}

		}
		return false;
	}

	private Vector2 TestHit (Vector2 pointerPosition) {
		int i = (int) pointerPosition.x / WFConstants.tileWidth;
		int j = (int) pointerPosition.y / WFConstants.tileHeight;
		if (i < 0 || i >= WFConstants.boardWidth || j < 0 || j >= WFConstants.boardHeight) return new Vector2 (-1, -1);
		touchRect.x = i * WFConstants.tileWidth + WFConstants.BRICK_TOUCH_MARGIN;
		touchRect.y = j * WFConstants.tileHeight + WFConstants.BRICK_TOUCH_MARGIN;
		touchRect.width = touchRect.height = WFConstants.tileHeight - WFConstants.BRICK_TOUCH_MARGIN;
		if (touchRect.Contains (pointerPosition) && isAdjacentTo (i, j)) return new Vector2 (i, j);
		return new Vector2 (-1, -1);
	}

	public bool isAdjacentTo (int i, int j) {
		if (lastSelectedTile == null)
			return true;
		return !(1 < Mathf.Abs (lastSelectedTile.settings.i - i) || 1 < Mathf.Abs (lastSelectedTile.settings.j - j));
	}

	public void getExtraTime () {
		GameSparkManager.instance.GetExtraTime ();
	}

	public void setExtraTime (object o) {
		againPanel.SetActive (false);
		currentTime = 30F;
		isTimerActive = true;
		GameAnalyticsManager.instance.TrackResourceEventSink ("star", 25, "extra_time", "EndGame");
	}

	public void setExtraTimeAfterAd () {
		againPanel.SetActive (false);
		currentTime = 30F;
		isTimerActive = true;
	}

	//MOUSE CONTROLLERS

	public void OnPointerDown (PointerEventData eventData) {
		/*	Vector2 localCursor;
			if (!RectTransformUtility.ScreenPointToLocalPointInRectangle(GetComponent<RectTransform>(), eventData.position, eventData.pressEventCamera, out localCursor))
				return;

			Debug.Log("LocalCursor:" + localCursor); 
		*/

		if (isTimerActive && !isMouseDown) {
			drawing = StartCoroutine (TrackPointer (eventData));
		}
	}

	public void OnPointerUp (PointerEventData eventData) {
		if (!isMouseDown)
			return;

		StopCoroutine (drawing);
		isMouseDown = false;
		Debug.Log ("OLASILIK 1");
		checkWord ();
	}
	public void GetUsedStarLetters () {
		if(ReachabilityManager.instance.IsReachable && !GameController.instance.isReachablityChanged){
			GameSparkManager.instance.GetStarLetter (
				delegate (bool response) {
					//GameController.instance.StartGamePlay (gameObject);
					prepareGame ();
				},
				delegate (string obj) {

				});
		}else {
			prepareGame ();
		}
	}

	IEnumerator TrackPointer (PointerEventData eventData) {
		if (isTimerActive) {
			isMouseDown = true;
			Vector3 mousePos = Vector3.zero;
			Vector2 localCursor;
			while (Application.isPlaying) {
				/*if (Input.mousePosition != mousePos) {
				mousePos = Input.mousePosition;
			}*/

				if (!RectTransformUtility.ScreenPointToLocalPointInRectangle (GetComponent<RectTransform> (), eventData.position, eventData.pressEventCamera, out localCursor))
					Debug.Log ("LocalCursor 1:" + localCursor);
				SelectLetter (localCursor);
				yield return 0;
			}
		} else {
			isMouseDown = false;
			StopCoroutine (drawing);
			Debug.Log ("OLASILIK 2");
			//checkWord ();
		}
	}

	private void creatStockData () {
		stockBoardDataCoordinates.Clear ();
		for (int t = 0; t < WFConstants.boardWidth; t++) {
			int[] a = new int[2];
			a[0] = 0;
			a[1] = 0;
			stockBoardDataCoordinates.Add (t, a);

		}

	}

	private string getDataFromLocal (string fileName) {
		string targetFile = File.ReadAllText (Application.persistentDataPath + "/" + fileName);
		return targetFile;
	}

	private void sendScore () {
		string endType = GameController.instance.firstEnd ? "FirstEnd" : "LastEnd";
		
		int time = GameController.instance.firstEnd == true ? (int) totalTime : (int) totalTime + 30;
		Debug.Log ("sendScore");
		if(!GameController.instance.isReachablityChanged && ReachabilityManager.instance.IsReachable) {
		GameAnalyticsManager.instance.trackFunnel ("Funnel:" + endType);
		PopupController.instance.OpenPopup ("LoadingPopup");
		GameSparkManager.instance.setScore (totalScore, time,
			delegate (AroundMeLeaderboardResponse response) {
				globalLeaderboardPlayers = new LeaderBoardPlayers (response);
				againPanel.SetActive (true);
				ShowGlobalLeaderboard ();
				PopupController.instance.ClosePopup ("LoadingPopup");
				SaverLoader.instance.SaveScore(totalScore);
			},
			delegate (string Error) {
				globalLeaderboardPlayers = new LeaderBoardPlayers (null as LeaderboardDataResponse);
				Debug.Log (Error);
				againPanel.SetActive (true);
				ShowGlobalLeaderboard ();
				PopupController.instance.ClosePopup ("LoadingPopup");
			});
		} else {
			if(totalScore > GameController.instance.player.oldScore) {
				//GameController.instance.player.oldScore = totalScore;
				SaverLoader.instance.SaveScore(totalScore);
				
			}
			againPanel.SetActive (true);
		}
	}

	public void ShowFacebookFriendsLeaderboard () {
		if (facebookFriendsLeaderboardPlayers == null) {
			GameSparkManager.instance.GetFacebookFriendsLeaderboard (
				delegate (LeaderboardDataResponse response) {
					Debug.Log ("Facebook  ----------------------   1");
					facebookFriendsLeaderboardPlayers = new LeaderBoardPlayers (response);
					lbc.SetData (facebookFriendsLeaderboardPlayers, "fb");
				},
				delegate (string Error) {
					Debug.Log ("Error in fb");
				}
			);
		} else {
			Debug.Log ("Facebook  ----------------------   2");
			lbc.SetData (facebookFriendsLeaderboardPlayers, "fb");
		}
	}

	public void ShowGlobalLeaderboard () {
		lbc.SetData (globalLeaderboardPlayers, "global");
	}

	private void sendLetterStar (List<string> s) {
		GameSparkManager.instance.SetStarLetter (s);
	}

}